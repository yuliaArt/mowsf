/**
 * Created by banghart on 6/2/18.
 */
@isTest(SeeAllData=false)
public with sharing class batchUpdateClientStatus_test {
    public static testMethod void testRunAs() {
        createTestData();
        List<Contact> c = [SELECT FirstName, LastName, Status__c, Funding_Source__c FROM Contact ORDER BY FirstName];
        system.assert(c[0].Status__c == 'Active');
        batchUpdateClientStatus batch = new batchUpdateClientStatus();
        system.debug('about to execute');
        Test.startTest();
        Id batchId = Database.executeBatch(batch);
        system.debug('called execute');
        Test.stopTest();
        List<Meal_Suspension__c> mealSuspensions = [SELECT Client__c FROM Meal_Suspension__c];
        Id suspendedClientId = mealSuspensions[0].Client__c;
        c = [SELECT FirstName, Status__c, Funding_Source__c FROM Contact WHERE Id = :suspendedClientId];
        system.assert(c[0].FirstName == 'A');
        system.assert(c[0].Funding_Source__c == 'Elderly Nutrition Program');
        system.debug('status is: ' + c[0].Status__c);
        //system.assert(c[0].Status__c == 'Suspended no resume date');

    }
    public static Void createTestData() {
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                .get('Client Record').getRecordTypeId();
        List<Route__c> routes = new List<Route__c>();
        routes.add(new Route__c(Name = 'test', Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'test2',Route_Type__c = 'Frozen', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        insert routes;
        List<Contact> c = new List<Contact>();
        c.add(new Contact(FirstName = 'A', LastName = 'B', Diet__c = 'Regular', Temperature__c = 'Chilled',  Status__c = 'Active', RecordTypeId = recordTypeId, Assessment_Cycle_Stages__c = 'NO ASSESSMENTS'));
        c.add(new Contact(FirstName = 'BB', LastName = 'BB', Diet__c = 'Regular', Temperature__c = 'Frozen', Status__c = 'Active', RecordTypeId = recordTypeId, Assessment_Cycle_Stages__c = 'NO ASSESSMENTS'));
        c.add(new Contact(FirstName = 'CC', LastName = 'CC', Diet__c = 'Regular', Temperature__c = 'Hot', Status__c = 'Active', RecordTypeId = recordTypeId, Assessment_Cycle_Stages__c = 'NO ASSESSMENTS'));
        c.add(new Contact(FirstName = 'Dan', LastName = 'Driver', MOWSF_Staff__c = true, Assessment_Cycle_Stages__c = 'NO ASSESSMENTS'));
        insert c;
        c = [SELECT Id, Assessment_Cycle_Switch__r.Assessment_Cycle_Enable__c FROM Contact WHERE MOWSF_Staff__c = false];
        List <Meal_Suspension__c> mealSuspensionsList = new List<Meal_Suspension__c>();
        mealSuspensionsList.add(new Meal_Suspension__c(
                Client__c = c[0].Id,
                Start_Date__c = Date.today().addDays(-2),
                Reason__c = 'Appt',
                Requestor__c = 'Self'
            )
        );
        insert mealSuspensionsList;
        List<Meal_Scheduler__c> mealSchedulers = new List<Meal_Scheduler__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        List<Funding_Source_Information__c> fsis = new List<Funding_Source_Information__c>();
        for (Contact client : c) {
            mealSchedulers.add(new Meal_Scheduler__c(Client__c = client.Id, Mon__c = true, Tue__c = true, Wed__c = true,
                    Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                    Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
            routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = client.Id, Route__c = routes[0].Id));
            routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = client.Id, Route__c = routes[1].Id));
            fsis.add(new Funding_Source_Information__c(Contact__c = client.Id, Start_Date__c = Date.today().addDays(-2),
                    Funding_Source__c = 'Elderly Nutrition Program', MOW_Program__c = 'Senior Meal'));
            system.debug(client.Id + 'is a client');
        }
        // need to reverse effect of trigger that will change Status__c to 'Suspended with End Date' when meal suspension is created.
        for (Contact cupdate :c) {
            cupdate.Status__c = 'Active';
        }
        insert fsis;
        update c;
        c = [SELECT Id FROM Contact WHERE MOWSF_Staff__c = true];
        routes = [SELECT Id FROM Route__c];
        routes[0].Driver_1__c = c[0].Id;
        routes[1].Driver_1__c = c[0].Id;

        insert mealSchedulers;
        insert routeGroupMembers;
        System.debug('number of routes created was ' + routes.size());


    }

}