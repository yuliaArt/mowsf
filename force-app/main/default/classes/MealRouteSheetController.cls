/**
 * Created by dsmith on 4/20/2017.
 - Modified by sunil/Girikon on 5/25/2017. 
 */

public with sharing class MealRouteSheetController {

    private static Integer PAGEBREAK = 9;
    public transient string RouteName{get; set;}
//    public  transient List<Route_Sheet_Row__c> pageRows {get; private set;}
    private ApexPages.StandardController standardController;
    //private transient List<Meal_Scheduler__c> schedules;
//    private transient Map<ID, Integer> client_map = new Map<ID, Integer>();
//    public Map<String,List<Route_Sheet_Row__c>> mapLstRouteSheet{get;set;}
//    public transient Map<String,SummaryWrapper> mapSummary {get;set;}
//    public transient Map<String,Boolean> mapIsSuspensions {get;set;}
//    public transient Map<String,List<Meal_Suspension__c>> MapMeal_Suspension {get;set;}

    
    public MealRouteSheetController (ApexPages.Standardcontroller standardController)
    {
//        Date printDate;
//        String paramDate = '';
//        pageRows = new List<Route_Sheet_Row__c>{};
//        mapLstRouteSheet = new Map<String,List<Route_Sheet_Row__c>>();
//        mapSummary= new Map<String,SummaryWrapper>();
//        mapIsSuspensions = new Map<String,Boolean>();
//        this.standardcontroller = standardController;
//        MapMeal_Suspension = new Map<String,List<Meal_Suspension__c>>();
//        try{
//            paramDate = System.currentPageReference().getParameters().get('printdate');
//            printDate = date.parse(paramDate);
//            //system.debug('Date to print '+paramDate+' which becomes '+printDate.format());
//        }
//        catch(exception e){
//            //printDate = date.today();
//        }

        //schedules = [Select Client__c, Client__r.ID, Client__r.Name, Diet__c, Route__c, Temperature__c, Mon__c, Tue__c, Wed__c, Thu__c, Fri__c, Sat__c, Sun__c from Meal_Scheduler__c Where (Client__r.Active__c=true AND Start_Date__c<=:printDate AND End_Date__c>=:printDate) Order By Route__c];
        //system.debug('Schedules results are '+schedules.size());

//        queryLoadRows(printDate, standardController.getid());
//        system.debug('------------'+Json.Serialize(mapSummary));
    }
    
    public class Temperature{
       //public Map<String,Decimal> mapDWrap{get;set;}
    }
    
    public class SummaryWrapper{
      
       //public List<Route_Sheet_Row__c> ListRoute_Sheet_Row {get; set;}
       //public Map<string,Temperature> MapDiet {get; set;}
    } 
    
//    void queryLoadRows(Date d, id Sheetid){

//        Integer counter = 0;
//        Integer client_counter = 0;
        
//
//        pageRows = [Select id, Address__c, Client__c, Client__r.ID, Cross_Street__c, Date__c, Diet1__c, Diet2__c, Diet3__c, Diet4__c, Driver_Notes__c, First_Name__c, Gender__c, Last_Name__c, Output_Delivery_1__c, Output_Delivery_2__c, Output_Delivery_3__c, Output_Delivery_4__c, Output_Name__c, Output_Schedule_Routes__c, Output_Schedule_Temps__c, Output_Schedule_Monday__c, Output_Schedule_Tuesday__c, Output_Schedule_Wednesday__c, Output_Schedule_Thursday__c, Output_Schedule_Friday__c, Output_Schedule_Saturday__c, Output_Schedule_Sunday__c, Page_Break_After__c, Page_Header__c, Primary_Language__c, Quantity1__c, Quantity2__c, Quantity3__c, Quantity4__c, Route__c, Route_Header__c, Route_Name__c, Sequence__c, Telephone__c, Temperature1__c, Temperature2__c, Temperature3__c, Temperature4__c from Route_Sheet_Row__c Where Date__c=:d Order by Route__c, Sequence__c ];
        //system.debug('Beginning traversing allrows');

//        For(Route_Sheet_Row__c r : pageRows) {
//            client_map.put(r.Client__r.ID,client_counter);
//            client_counter++;
//
//            if(counter==0) {
//                r.Page_Header__c = true;
//            }
//            counter++;
//            //Commented beacuse we add this logic in Routemap
//            // system.debug('Processing one row for '+r.Output_Name__c);
//            //if(counter==PAGEBREAK) {
//               // r.Page_Break_After__c = true;
//               // counter=0;
//            //}
//
//            //Map Pagerows with routes
//            mapIsSuspensions.put(r.Route__c,false);
//             MapMeal_Suspension.put(r.Route__c,new List<Meal_Suspension__c>());
//             if(mapSummary.containskey(r.Route__c)){
//                SummaryWrapper objSummaryWrapper= mapSummary.get(r.Route__c);
//
//                //Add Page break
//                if(math.mod(objSummaryWrapper.ListRoute_Sheet_Row.size(), PAGEBREAK-1)==0) {
//                  r.Page_Break_After__c = true;
//                }
//
//                //Show Page header
//                if(math.mod(objSummaryWrapper.ListRoute_Sheet_Row.size(), PAGEBREAK)==0) {
//                   r.Page_Header__c = true;
//                }
//                objSummaryWrapper.ListRoute_Sheet_Row.add(r);
//
//                mapSummary.put(r.Route__c,objSummaryWrapper);
//             }
//             else
//             {
//
//                SummaryWrapper objSummaryWrapper= new SummaryWrapper();
//                List<Route_Sheet_Row__c> newlist= new  List<Route_Sheet_Row__c>();
//                newlist.add(r);
//
//                objSummaryWrapper.MapDiet= new Map<string,Temperature> ();
//                objSummaryWrapper.ListRoute_Sheet_Row=newlist;
//                mapSummary.put(r.Route__c,objSummaryWrapper);
//             }
//        }

        //now we're going to populate the meal scheduler data into the page_route_rows
//        For(Meal_Scheduler__c ms : schedules){
//            system.debug('Locating rows for client '+ms.Client__r.Name+' with ID '+ms.Client__r.ID);
//            if(client_map.containsKey(ms.Client__r.ID)){        //if yes, this means the client appears on the route sheet. Note, this code does not address a client appearing on multiple routes on a given day which operationally shouldn't happen anyway
//                system.debug('Found a meal schedule row for '+ms.Client__r.Name);
//                Route_Sheet_Row__c rsr = new Route_Sheet_Row__c();
//                rsr = pageRows.get(client_map.get(ms.Client__r.ID));
//                If(rsr.Output_Schedule_Routes__c==null){ //true means this is the first row of the meal schedule block
//                    rsr.Output_Schedule_Routes__c=ms.Route__c+'\r\n';
//                    rsr.Output_Schedule_Temps__c=ms.Temperature__c+' '+ms.Diet__c+'\r\n';
//                    rsr.Output_Schedule_Monday__c=ms.Mon__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Tuesday__c=ms.Tue__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Wednesday__c=ms.Wed__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Thursday__c=ms.Thu__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Friday__c=ms.Fri__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Saturday__c=ms.Sat__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Sunday__c=ms.Sun__c.toPlainString()+'\r\n';
//                }else   //there is already a meal schedule row in there so append
//                {
//                    rsr.Output_Schedule_Routes__c+=ms.Route__c+'\r\n';
//                    rsr.Output_Schedule_Temps__c+=ms.Temperature__c+' '+ms.Diet__c+'\r\n';
//                    rsr.Output_Schedule_Monday__c+=ms.Mon__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Tuesday__c+=ms.Tue__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Wednesday__c+=ms.Wed__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Thursday__c+=ms.Thu__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Friday__c+=ms.Fri__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Saturday__c+=ms.Sat__c.toPlainString()+'\r\n';
//                    rsr.Output_Schedule_Sunday__c+=ms.Sun__c.toPlainString()+'\r\n';
//                }
//            }
//        }
        
        //now cycle backwords to create a page break after each time a route changes
//        Integer lc = pageRows.size();
//        system.debug('Page rows size '+lc.format());
//        ID oldroute;
//        ID newroute;
//        List<String> ObjRoutrId =new List<String>();
//        //system.debug('Beginning traversing allrows backwords');
//        For(Integer t = lc-1;t>=0;t--){
//            newroute = pagerows[t].Route__c;
//
//            //system.debug('Comparing at slot '+t.format()+' oldroute '+oldroute+' newroute '+newroute);
//            if(oldroute==null||oldroute!=newroute) {    //we're at the record where we need to break pages since a new route is after
//                //system.debug('Setting page break to true on reverse');
//                pagerows[t].Page_Break_After__c = true;
//
//                //Calling calculation funtion and bind calculated data to routes map
//                SummaryWrapper objSummaryWrapper= mapSummary.get(newroute);
//                objSummaryWrapper.MapDiet=calculateDiet(objSummaryWrapper.ListRoute_Sheet_Row);
//                mapSummary.put(pagerows[t].Route__c,objSummaryWrapper);
//
//                ObjRoutrId.add(newroute);
//            }
//            oldroute=newroute;
//        }

//        getSuspension(d,ObjRoutrId);
//        system.debug('And we are all done!');
//    }

    //Get suspended clients list for the day on routes
  // public void getSuspension(Date printDate,List<String> RouteId) {
       
  //     List<Meal_Suspension__c> objMeal_Suspension =new List<Meal_Suspension__c>([Select Client__c,Client__r.Name,Reason__c, Start_Date__c, End_Date__c, Long_Hold__c,Client__r.Primary_Route__c from Meal_Suspension__c Where  Client__r.Active__c = true and Client__r.Primary_Route__c IN :RouteId and Start_Date__c<=:printDate AND End_Date__c>=:printDate]); //Where (Start_Date__c<='+PrintDate+') AND (End_Date__c>='+PrintDate+')
  //     for(Meal_Suspension__c newMeal_Suspension : objMeal_Suspension)
  //     {
  //          //Map suspended cliets to routes
  //          if(MapMeal_Suspension.containskey(newMeal_Suspension.Client__r.Primary_Route__c)){
  //              List<Meal_Suspension__c> lstMealSup = MapMeal_Suspension.get(newMeal_Suspension.Client__r.Primary_Route__c);
  //              lstMealSup.add(newMeal_Suspension);
  //              MapMeal_Suspension.put(newMeal_Suspension.Client__r.Primary_Route__c,lstMealSup);
  //
  //          }
  //          else{
  //              List<Meal_Suspension__c> lstMealSup = new List<Meal_Suspension__c>();
  //              lstMealSup.add(newMeal_Suspension);
  //              MapMeal_Suspension.put(newMeal_Suspension.Client__r.Primary_Route__c,lstMealSup);
  //          }
  //         mapIsSuspensions.put(newMeal_Suspension.Client__r.Primary_Route__c,true);
  //     }
  // }
    
   
    //Calculate total diets for a route
//    public Map<String,Temperature> calculateDiet( List<Route_Sheet_Row__c>  pageRows) {
//        //List<Route_Sheet_Row__c> pageRows = [Select id, Address__c, Client__c, Client__r.ID, Cross_Street__c, Date__c, Diet1__c, Diet2__c, Diet3__c, Diet4__c, Driver_Notes__c, First_Name__c, Gender__c, Last_Name__c, Output_Delivery_1__c, Output_Delivery_2__c, Output_Delivery_3__c, Output_Delivery_4__c, Output_Name__c, Output_Schedule_Routes__c, Output_Schedule_Temps__c, Output_Schedule_Monday__c, Output_Schedule_Tuesday__c, Output_Schedule_Wednesday__c, Output_Schedule_Thursday__c, Output_Schedule_Friday__c, Output_Schedule_Saturday__c, Output_Schedule_Sunday__c, Page_Break_After__c, Page_Header__c, Primary_Language__c, Quantity1__c, Quantity2__c, Quantity3__c, Quantity4__c, Route__c, Route_Header__c, Route_Name__c, Sequence__c, Telephone__c, Temperature1__c, Temperature2__c, Temperature3__c, Temperature4__c from Route_Sheet_Row__c Where Date__c=:PrintDate and Route__c=:RouteID Order by Route__c, Sequence__c ];
//        RouteName=pageRows[0].Route_Name__c;
//        Map<String,Temperature> mapTemp= new Map<String,Temperature>();
//
//        for(Route_Sheet_Row__c singleRSRow:pageRows){
//             if(singleRSRow.Temperature1__c != null && String.isNotBlank(singleRSRow.Temperature1__c))
//             {
//                 If(singleRSRow.Diet1__c != null && String.isNotBlank(singleRSRow.Diet1__c)){
//
//                     if(mapTemp.containskey(singleRSRow.Temperature1__c)){
//                         Temperature temp = mapTemp.get(singleRSRow.Temperature1__c) ;
//                         if(temp.mapDWrap.containskey(singleRSRow.Diet1__c)){
//                            Decimal iValue = temp.mapDWrap.get(singleRSRow.Diet1__c);
//                            iValue= iValue + singleRSRow.Quantity1__c;
//                            temp.mapDWrap.put(singleRSRow.Diet1__c,iValue);
//                         }
//                         else{
//
//                            temp.mapDWrap.put(singleRSRow.Diet1__c,singleRSRow.Quantity1__c);
//                         }
//
//                         mapTemp.put(singleRSRow.Temperature1__c,temp);
//                     }
//                     else{
//                         Temperature temp = new Temperature();
//                         Map<String,Decimal> mapDWrap = new Map<String,Decimal>();
//                         mapDWrap.put(singleRSRow.Diet1__c,singleRSRow.Quantity1__c);
//                         temp.mapDWrap = mapDWrap;
//
//                         mapTemp.put(singleRSRow.Temperature1__c,temp);
//                     }
//
//                 }
//             }
//             if(singleRSRow.Temperature2__c != null && String.isNotBlank(singleRSRow.Temperature2__c))
//             {
//                 If(singleRSRow.Diet2__c != null && String.isNotBlank(singleRSRow.Diet2__c)){
//
//                     if(mapTemp.containskey(singleRSRow.Temperature2__c)){
//                         Temperature temp = mapTemp.get(singleRSRow.Temperature2__c) ;
//                         if(temp.mapDWrap.containskey(singleRSRow.Diet2__c)){
//                            Decimal iValue = temp.mapDWrap.get(singleRSRow.Diet2__c);
//                            iValue= iValue + singleRSRow.Quantity2__c;
//
//                            temp.mapDWrap.put(singleRSRow.Diet2__c,iValue);
//                         }
//                         else{
//
//                            temp.mapDWrap.put(singleRSRow.Diet2__c,singleRSRow.Quantity2__c);
//                         }
//
//                         mapTemp.put(singleRSRow.Temperature2__c,temp);
//                     }
//                     else{
//                         Temperature temp = new Temperature();
//                         Map<String,Decimal> mapDWrap = new Map<String,Decimal>();
//                         mapDWrap.put(singleRSRow.Diet2__c,singleRSRow.Quantity2__c);
//                         temp.mapDWrap = mapDWrap;
//
//                         mapTemp.put(singleRSRow.Temperature2__c,temp);
//                     }
//
//                 }
//             }
//             if(singleRSRow.Temperature3__c != null && String.isNotBlank(singleRSRow.Temperature3__c))
//             {
//                 If(singleRSRow.Diet3__c != null && String.isNotBlank(singleRSRow.Diet3__c)){
//
//                     if(mapTemp.containskey(singleRSRow.Temperature3__c)){
//                         Temperature temp = mapTemp.get(singleRSRow.Temperature3__c) ;
//                         if(temp.mapDWrap.containskey(singleRSRow.Diet3__c)){
//                            Decimal iValue = temp.mapDWrap.get(singleRSRow.Diet3__c);
//                            iValue= iValue + singleRSRow.Quantity3__c;
//
//                            temp.mapDWrap.put(singleRSRow.Diet3__c,iValue);
//                         }
//                         else{
//
//                            temp.mapDWrap.put(singleRSRow.Diet3__c,singleRSRow.Quantity3__c);
//                         }
//                         mapTemp.put(singleRSRow.Temperature3__c,temp);
//                     }
//                     else{
//                         Temperature temp = new Temperature();
//                         Map<String,Decimal> mapDWrap = new Map<String,Decimal>();
//                         mapDWrap.put(singleRSRow.Diet3__c,singleRSRow.Quantity3__c);
//                         temp.mapDWrap = mapDWrap;
//                         mapTemp.put(singleRSRow.Temperature3__c,temp);
//                     }
//
//                 }
//             }
//             if(singleRSRow.Temperature4__c != null && String.isNotBlank(singleRSRow.Temperature4__c))
//             {
//                If(singleRSRow.Diet4__c != null && String.isNotBlank(singleRSRow.Diet4__c)){
//
//                     if(mapTemp.containskey(singleRSRow.Temperature4__c)){
//                         Temperature temp = mapTemp.get(singleRSRow.Temperature4__c) ;
//                         if(temp.mapDWrap.containskey(singleRSRow.Diet4__c)){
//                            Decimal iValue = temp.mapDWrap.get(singleRSRow.Diet4__c);
//                            iValue= iValue + singleRSRow.Quantity4__c;
//
//                            temp.mapDWrap.put(singleRSRow.Diet4__c,iValue);
//                         }
//                         else{
//
//                            temp.mapDWrap.put(singleRSRow.Diet4__c,singleRSRow.Quantity4__c);
//                         }
//                         mapTemp.put(singleRSRow.Temperature4__c,temp);
//                     }
//                     else{
//                         Temperature temp = new Temperature();
//                         Map<String,Decimal> mapDWrap = new Map<String,Decimal>();
//                         mapDWrap.put(singleRSRow.Diet4__c,singleRSRow.Quantity4__c);
//                         temp.mapDWrap = mapDWrap;
//                         mapTemp.put(singleRSRow.Temperature4__c,temp);
//                     }
//
//                 }
//             }
//        }
//        return mapTemp;
//    }
}