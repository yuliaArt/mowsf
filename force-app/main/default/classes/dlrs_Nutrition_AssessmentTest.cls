/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Nutrition_AssessmentTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Nutrition_AssessmentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Nutrition_Assessment__c());
    }
}