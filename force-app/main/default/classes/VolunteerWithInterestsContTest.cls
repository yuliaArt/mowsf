@isTest
public class VolunteerWithInterestsContTest {

    public List<GW_Volunteers__Volunteer_Hours__c> displayVolunteers {get;set;}
    public GW_Volunteers__Volunteer_Job__c filterProgram { get; set; }    
    public GW_Volunteers__Volunteer_Hours__c filterVolunteer {get;set;}

    @isTest static void testfilter(){
 
         Account testAccount = new Account();
         testAccount.Name = 'Test Account';
         insert testAccount;
         
         Contact testContact = new Contact();
         testContact.FirstName = 'TestFirstName';
         testContact.LastName  = 'TestLastName';
         testContact.AccountId = testAccount.Id;
         insert testContact;
         
         Campaign testProgram = new Campaign();
         testProgram.Name = 'Test Program' ;
         testProgram.RecordTypeId = [Select id from RecordType where sObjectType = 'Campaign' and developerName = 'Volunteers_Campaign'].id;
         testProgram.Status = 'In Progress'; 
         testProgram.isActive = TRUE;
         insert testProgram;
         
         GW_Volunteers__Volunteer_Job__c testJob = new GW_Volunteers__Volunteer_Job__c();
         testJob.Name = 'Test Job';
         testJob.GW_Volunteers__Campaign__c = testProgram.Id;
         insert testJob;
         
         Volunteer_Interest__c testInterest = new Volunteer_Interest__c();
         testInterest.Volunteer__c = testContact.Id;
         testInterest.Program__c = testProgram.Id;
         testInterest.Volunteer_Job__c = null;
         system.debug(testInterest);
         insert testInterest;
         
         DateTime dt = DateTime.now();
         GW_Volunteers__Volunteer_Shift__c testShift = new GW_Volunteers__Volunteer_Shift__c();
         testShift.GW_Volunteers__Volunteer_Job__c = testJob.Id;
         testShift.GW_Volunteers__Start_Date_Time__c = dt;
         testShift.GW_Volunteers__Duration__c = 2;
         insert testShift;

         GW_Volunteers__Volunteer_Hours__c testHours = new GW_Volunteers__Volunteer_Hours__c();
         testHours.GW_Volunteers__Contact__c = testContact.Id;
         testHours.GW_Volunteers__Volunteer_Job__c = testJob.Id;
         testHours.GW_Volunteers__Status__c = 'Confirmed';
         testHours.GW_Volunteers__Number_of_Volunteers__c = 1;
         testHours.GW_Volunteers__Start_Date__c = dt.date();
         insert testHours;
 
         GW_Volunteers__Volunteer_Job__c t_filterProgram = new GW_Volunteers__Volunteer_Job__c();
         t_filterProgram.GW_Volunteers__Campaign__c = testProgram.Id;
        
         GW_Volunteers__Volunteer_Hours__c t_filterVolunteer = new GW_Volunteers__Volunteer_Hours__c();
         t_filterVolunteer.GW_Volunteers__Volunteer_Job__c = testJob.Id;
         t_filterVolunteer.GW_Volunteers__Start_Date__c = testHours.GW_Volunteers__Start_Date__c;
         t_filterVolunteer.GW_Volunteers__Status__c = testHours.GW_Volunteers__Status__c;
                
         PageReference pageRef = Page.VolunteersWithInterestsReport;
         
         ApexPages.currentPage().getParameters().put('GW_Volunteers__Volunteer_Job__c',t_filterProgram.Id);
         ApexPages.currentPage().getParameters().put('GW_Volunteers__Volunteer_Hours__c',t_filterVolunteer.Id);         
 
         Test.setCurrentPage(pageRef);
         
         VolunteerWithInterestsCont c = new VolunteerWithInterestsCont();

         c.filterProgram = t_filterProgram;
         c.filterVolunteer = t_filterVolunteer;

         c.filter();
         System.assertNotEquals(0, c.displayVolunteers.size()); 
                    
        } //filter
    
    } // class