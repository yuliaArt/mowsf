@isTest
public class MOWSF_VolunteerVehicleInfo_Test {
   
  	 static testMethod void testSaveAll(){
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
		
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
         
        VolunteerVehicleInfo__c VolVehicleInfoTest=new VolunteerVehicleInfo__c();

        MOWSF_VolunteerVehicleInfoController mowsfVvi = new MOWSF_VolunteerVehicleInfoController(new ApexPages.StandardController(contactObj));
        VolVehicleInfoTest.Contact__c=contactObj.Id;
        VolVehicleInfoTest.Vehicle_Available__c=true;
        VolVehicleInfoTest.Insurance_Company__c='Test';
        VolVehicleInfoTest.Insurance_Policy_Nbr__c='Test';
        VolVehicleInfoTest.Insurance_Expiration_Date__c=Date.parse('11/11/2017');
        VolVehicleInfoTest.Vehicle_Plate_Number__c='Test';
        VolVehicleInfoTest.Drivers_License_Number__c='Test';
        VolVehicleInfoTest.Driver_s_License_Expiration_Date__c=Date.parse('11/11/2017');
         
        mowsfVvi.VolVehicleInfo=VolVehicleInfoTest;
        mowsfVvi.saveVolunteerVehicleInfo();
        //Test that Records are Inserted/Updated
        system.assertequals('Thank you for updating your vehicle information.',mowsfVvi.saveMessage);	
   	}
    
    static testMethod void runTest() {
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
		
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
         
        MOWSF_VolunteerVehicleInfoController c = new MOWSF_VolunteerVehicleInfoController(new ApexPages.StandardController(contactObj));
        c.test_Only();
      }    
    
}