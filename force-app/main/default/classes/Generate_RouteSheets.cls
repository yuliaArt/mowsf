/**
 * Created by dsmith on 4/24/2017.
 * Modified by Rick Banghart 1/6/2018, 9/8/2018
 */

public with sharing class Generate_RouteSheets {
    public Date generate_date;
    public Date holiday_date {get;set;}
    public Boolean combine_holiday {get;set;}
    // the following are mostly summary variables
    Decimal ChillLS = 0;
    Decimal ChillDB = 0;
    Decimal ChillMS = 0;
    Decimal ChillRG = 0;
    public Decimal ChillLS_alt = 0;
    public Decimal ChillDB_alt = 0;
    public Decimal ChillMS_alt = 0;
    public Decimal ChillRG_alt = 0;
    Decimal HotLS = 0;
    Decimal HotDB = 0;
    Decimal HotMS = 0;
    Decimal HotRG = 0;
    Decimal FrozenRG = 0;
    Decimal FrozenMS = 0;
    Decimal FrozenLS = 0;
    Decimal FrozenDB = 0;
    Decimal FrozenRG_alt = 0;
    Decimal FrozenMS_alt = 0;
    Decimal FrozenLS_alt = 0;
    Decimal FrozenDB_alt = 0;
    Decimal HotToChillClients = 0;
    String dayOfWeek {get;set;}
    Map<Id, List<Meal_Suspension__c>> mealSuspensionMap;
    Map<Id, Set<Id>> routeGroupMemberMap;
    List<Meal_Suspension__c> mealSuspensionList;
    Map<Id, Integer> clientCountMap = new Map<Id, Integer>();

    public Generate_RouteSheets(Date d) {
        generate_date = d;
        system.debug('Generate_RouteSheets constructor');
        mealSuspensionList = getMeal_suspensions(generate_date);
        routeGroupMemberMap = getRouteGroupMemberMap();
        String dateString = d.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        dayOfWeek = deliveryDate.format('EEE');
    }
    public String create_grid(Delivery__c del) {
        String gridString = '';
        gridString += '<table><tr><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td></tr>';
        gridString += '<tr><td style="text-align:center">' + del.Meal_Scheduler__r.Sun_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Mon_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Tue_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Wed_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Thu_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Fri_Quantity__c.toPlainString() + '</td>';
        gridString += '<td style="text-align:center">' + del.Meal_Scheduler__r.Sat_Quantity__c.toPlainString() + '</td>';
        gridString += '</tr></table>';
        return gridString;
    }
    public List<Route_Group_Deliveries> do_generation(String selectedRoute){
        Map<Id, Route__c> routes = new Map<Id, Route__c>{};
        Map<Id, Id> deliveryClientMap = new Map<Id, Id>();
        List<Route_Group_Deliveries> routeGroupDeliveriesList = new List<Route_Group_Deliveries>{};
        List<Delivery__c> deliveries = new List<Delivery__c>{};
        List<Delivery__c> specialDeliveries = new List<Delivery__c>{};
        Integer i = 0;
        Id routeGroupId = NULL;
        List<Route_Sheet_Delivery_Row> return_deliveries = new List<Route_Sheet_Delivery_Row>{};
        Route_Group_Deliveries routeGroupDels = new Route_Group_Deliveries();
        routeGroupDels.dayOfWeek = dayOfWeek;
        i = 0;
        //System.debug('selected route is: ' + selectedRoute + '*********');
        if (selectedRoute == 'all') {
            system.debug('about to get all deliveries for all routes for ' + generate_date);
            deliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.LastName, Client__r.FirstName,
                            Client__r.Cross_Street__c, Client__r.Phone, Client__r.Gender__c, Client__r.Primary_Language__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c, Client__r.Apartment__c,
                            Hot__c, Chill__c, Frozen__c,Chill_Alt__c,Frozen_Alt__c,Special__c,
                            Meal_Scheduler__r.Mon__c, Meal_Scheduler__r.Tue__c,
                            Meal_Scheduler__r.Wed__c,Meal_Scheduler__r.Thu__c,Meal_Scheduler__r.Fri__c,
                            Meal_Scheduler__r.Sat__c, Meal_Scheduler__r.Sun__c,
                            Meal_Scheduler__r.Mon_Quantity__c, Meal_Scheduler__r.Tue_Quantity__c,
                            Meal_Scheduler__r.Wed_Quantity__c, Meal_Scheduler__r.Thu_Quantity__c,
                            Meal_Scheduler__r.Fri_Quantity__c, Meal_Scheduler__r.Sat_Quantity__c,
                            Meal_Scheduler__r.Sun_Quantity__c, holiday__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = false
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
            specialDeliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.Cross_Street__c, Client__r.Phone, Client__r.LastName,
                            Client__r.FirstName, Client__r.MailingStreet, Client__r.Apartment__c, Special__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c,
                            List_Items__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = true
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
        } else if (selectedRoute == 'daily' || selectedRoute == 'frozen') {
            deliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.LastName, Client__r.FirstName,
                            Client__r.Cross_Street__c, Client__r.Phone, Client__r.Gender__c, Client__r.Primary_Language__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c, Client__r.Apartment__c,
                            Hot__c, Chill__c, Frozen__c,Chill_Alt__c,Frozen_Alt__c,Special__c,
                            Meal_Scheduler__r.Mon__c, Meal_Scheduler__r.Tue__c,
                            Meal_Scheduler__r.Wed__c,Meal_Scheduler__r.Thu__c,Meal_Scheduler__r.Fri__c,
                            Meal_Scheduler__r.Sat__c, Meal_Scheduler__r.Sun__c,
                            Meal_Scheduler__r.Mon_Quantity__c, Meal_Scheduler__r.Tue_Quantity__c,
                            Meal_Scheduler__r.Wed_Quantity__c, Meal_Scheduler__r.Thu_Quantity__c,
                            Meal_Scheduler__r.Fri_Quantity__c, Meal_Scheduler__r.Sat_Quantity__c,
                            Meal_Scheduler__r.Sun_Quantity__c, holiday__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = false
                    AND Route_Group_Del__r.Route_Type__c = :selectedRoute
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
            specialDeliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.Cross_Street__c, Client__r.Phone, Client__r.LastName,
                            Client__r.FirstName, Client__r.MailingStreet, Client__r.Apartment__c, Special__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c,
                            List_Items__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = true
                    AND Route_Group_Del__r.Route_Type__c = :selectedRoute
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];

        } else if (selectedRoute == 'hdg') {
            system.debug('handling hdg here');
            deliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.LastName, Client__r.FirstName,
                            Client__r.Cross_Street__c, Client__r.Phone, Client__r.Gender__c, Client__r.Primary_Language__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c, Client__r.Apartment__c,
                            Hot__c, Chill__c, Frozen__c,Chill_Alt__c,Frozen_Alt__c,Special__c,
                            Meal_Scheduler__r.Mon__c, Meal_Scheduler__r.Tue__c,
                            Meal_Scheduler__r.Wed__c,Meal_Scheduler__r.Thu__c,Meal_Scheduler__r.Fri__c,
                            Meal_Scheduler__r.Sat__c, Meal_Scheduler__r.Sun__c,
                            Meal_Scheduler__r.Mon_Quantity__c, Meal_Scheduler__r.Tue_Quantity__c,
                            Meal_Scheduler__r.Wed_Quantity__c, Meal_Scheduler__r.Thu_Quantity__c,
                            Meal_Scheduler__r.Fri_Quantity__c, Meal_Scheduler__r.Sat_Quantity__c,
                            Meal_Scheduler__r.Sun_Quantity__c, holiday__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = false
                        AND Route_Group_Del__r.Route_Type__c = 'Home Delivered Groceries'
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
            specialDeliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.Cross_Street__c, Client__r.Phone, Client__r.LastName,
                            Client__r.FirstName, Client__r.MailingStreet, Client__r.Apartment__c, Special__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c,
                            List_Items__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = true
                        AND Route_Group_Del__r.Route_Type__c = 'Home Delivered Groceries'
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
        } else {
            system.debug('about to get deliveries for selected route');
            deliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.LastName, Client__r.FirstName,
                            Client__r.Cross_Street__c, Client__r.Phone, Client__r.Gender__c, Client__r.Primary_Language__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c, Client__r.Apartment__c,
                            Hot__c, Chill__c, Frozen__c,Chill_Alt__c,Frozen_Alt__c, Special__c,
                            Meal_Scheduler__r.Mon__c, Meal_Scheduler__r.Tue__c,
                            Meal_Scheduler__r.Wed__c,Meal_Scheduler__r.Thu__c,Meal_Scheduler__r.Fri__c,
                            Meal_Scheduler__r.Sat__c, Meal_Scheduler__r.Sun__c,
                            Meal_Scheduler__r.Mon_Quantity__c, Meal_Scheduler__r.Tue_Quantity__c,
                            Meal_Scheduler__r.Wed_Quantity__c, Meal_Scheduler__r.Thu_Quantity__c,
                            Meal_Scheduler__r.Fri_Quantity__c, Meal_Scheduler__r.Sat_Quantity__c,
                            Meal_Scheduler__r.Sun_Quantity__c, holiday__c

                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = false
                    AND Route_Group_del__c = :selectedRoute
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];
            specialDeliveries = [
                    SELECT Id, Date__c, Client__r.Name, Client__r.Cross_Street__c, Client__r.Phone, Client__r.LastName,
                            Client__r.FirstName, Client__r.MailingStreet,Special__c,
                            Client__r.Diet__c, Client__r.Temperature__c, Sequence__c, Route_Group_del__r.Name,
                            Address__c, Route_Group_del__c, Route__r.LoadTime__c, Client__r.Delivery_Notes__c,
                            Client__r.Funding_Source__c, Quantity__c, Client__c, Client__r.Apartment__c,
                            List_Items__c
                    FROM Delivery__c
                    WHERE Date__c = :generate_date and Special__c = true
                    AND Route_Group_del__c = :selectedRoute
                    ORDER BY Route_Group_del__r.Name, Sequence__c
            ];

        }
        Map<Id, List<Delivery__c> > specialDeliveryMapIds = new Map<Id,List<Delivery__c>>();
        Map<Id, List<Delivery__c> > specialDeliveryRouteIdMap = new Map<Id, List<Delivery__c> >();
        For (Delivery__c sd : specialDeliveries) {
            // builds map <client_id, list<special_delivery>
            if (specialDeliveryMapIds.containsKey(sd.Client__c) == true) {
                specialDeliveryMapIds.get(sd.Client__c).add(sd);
            } else {
                List<Delivery__c> tList = new List<Delivery__c>();
                tList.add(sd);
                specialDeliveryMapIds.put(sd.Client__c, tList);
            }
        }
        For (Delivery__c sd : specialDeliveries) {
            //builds map <route_id,List<special_deliveries>>
            if (specialDeliveryRouteIdMap.containsKey(sd.Route_Group_Del__c) == true) {
                //system.debug('adding to existing list ' + sd.Route_Group_Del__r.Name + ' ' + sd.Client__r.Name);
                specialDeliveryRouteIdMap.get(sd.Route_Group_Del__c).add(sd);
            } else {
                List<Delivery__c> tlist = new List<Delivery__c>();
                //system.debug('creating list to add ' + sd.Route_Group_Del__r.Name + ' ' + sd.Client__r.Name);
                tlist.add(sd);
                specialDeliveryRouteIdMap.put(sd.Route_Group_Del__c, tlist);
            }
        }
        For (Delivery__c d: deliveries) {
            //system.debug('putting client in client map ' + d.Client__r.Name);
            deliveryClientMap.put(d.Client__c,d.Route_Group_Del__c);
        }
        System.debug('number of deliveries is: ' + deliveries.size());
        For(Delivery__c d : deliveries){
            //System.debug(d.Route_Group_Del__r.Name + ' is the route group name');
            Boolean setDateString = false;
            if (d.Holiday__c != null && setDateString == false) {
                routeGroupDels.combineHoliday = true;
                routeGroupDels.holiday_date = d.Holiday__c;
                routeGroupDels.AltDateString = d.Holiday__c.format();
                setDateString = true;
            }
            if (routeGroupId != d.Route_Group_del__c) {
                // will be here for first delivery, as routeGroupId is null initially
                if (return_deliveries.size() > 0) {
                    // we have some deliveries for this route group and
                    // we are about to start a new route group
                    // The first route sheet has a header, so need to identify
                    // first or subsequent route group
                    if (i == 0) {
                        routeGroupDels.isNotFirst = false;
                        system.debug('set isNotFirst false');
                    } else {
                        routeGroupDels.isNotFirst = true;
                        system.debug('set isNotFirst true');
                    }
                    // Here is where we finish a route group before starting a new one
                    //
                    for (Meal_Suspension__c mstemp : mealSuspensionList) {
                        if (routeGroupMemberMap.containsKey(mstemp.Client__c)) {
                            for (Id route : routeGroupMemberMap.get(mstemp.Client__c)) {
                                if (route == routeGroupId) {
                                    routeGroupDels.suspensionList.add(mstemp);
                                }
                            }
                        }
                    }
                    routeGroupDels.deliveryList = return_deliveries;
                    routeGroupDels.SumChillLs = ChillLS;
                    routeGroupDels.SumChillDb = ChillDB;
                    routeGroupDels.SumChillMs = ChillMS;
                    routeGroupDels.SumChillRg = ChillRG;
                    routeGroupDels.SumChillLs_alt = ChillLS_alt;
                    routeGroupDels.SumChillDb_alt = ChillDB_alt;
                    routeGroupDels.SumChillMs_alt = ChillMS_alt;
                    routeGroupDels.SumChillRg_alt = ChillRG_alt;
                    routeGroupDels.SumHotRg = HotRG;
                    routeGroupDels.SumHotLs = HotLS;
                    routeGroupDels.SumHotDb = HotDB;
                    routeGroupDels.SumHotMs = HotMS;
                    routeGroupDels.SumFrozenRg = FrozenRG;
                    routeGroupDels.SumFrozenLs = FrozenLS;
                    routeGroupDels.SumFrozenDb = FrozenDB;
                    routeGroupDels.SumFrozenMs = FrozenMS;
                    routeGroupDels.SumFrozenRg_alt = FrozenRG_alt;
                    routeGroupDels.SumFrozenLs_alt = FrozenLS_alt;
                    routeGroupDels.SumFrozenDb_alt = FrozenDB_alt;
                    routeGroupDels.SumFrozenMs_alt = FrozenMS_alt;
                    routeGroupDels.HotToChillClients = HotToChillClients;
                    routeGroupDels.ClientCount = clientCountMap.size();
                    routeGroupDels.generateSummary();
                    routeGroupDeliveriesList.add(routeGroupDels);
                    return_deliveries = new List<Route_Sheet_Delivery_Row>{};
                    i = 1;
                }
                // This starts a new route sheet (for a single route)
                routeGroupDels = new Route_Group_Deliveries();
                routeGroupDels.dayOfWeek = dayOfWeek;
                if (dayOfWeek == 'Wed') {
                    routeGroupDels.AltDateString = generate_date.addDays(2).format();
                }
                if (dayOfWeek == 'Fri') {
                    routeGroupDels.AltDateString = 'Weeekend';
                }
                HotRG = 0;
                HotLS = 0;
                HotDB = 0;
                HotMS = 0;
                ChillRG = 0;
                ChillMS = 0;
                ChillLS = 0;
                ChillDB = 0;
                ChillRG_alt = 0;
                ChillMS_alt = 0;
                ChillLS_alt = 0;
                ChillDB_alt = 0;
                FrozenRG = 0;
                FrozenLS = 0;
                FrozenDB = 0;
                FrozenMS = 0;
                FrozenRG_alt = 0;
                FrozenLS_alt = 0;
                FrozenDB_alt = 0;
                FrozenMS_alt = 0;
                HotToChillClients = 0;
                clientCountMap.clear();
                //system.debug('**** about to check on special delivery client not on route **** ');
                //system.debug('does route map contain sd? ' + specialDeliveryRouteIdMap.containsKey(d.Route_Group_Del__c));
                //system.debug('does client map contain client? ' + deliveryClientMap.containsKey(d.Client__c));
                if (specialDeliveryRouteIdMap.containsKey(d.Route_Group_Del__c) == true) {
                    for (Delivery__c sd : specialDeliveryRouteIdMap.get(d.Route_Group_Del__c)) {
                        if (deliveryClientMap.containsKey(sd.Client__c) == false) {
                            routeGroupDels.generateOffRouteSpecialDelivery(sd);
                        } else {
                            if (deliveryClientMap.get(sd.Client__c) == routeGroupId) {
                                routeGroupDels.generateOffRouteSpecialDelivery(sd);
                            }
                        }
                    }
                }
                routeGroupDels.DateString = d.Date__c.format();
                routeGroupDels.RouteGroupString = d.Route_Group_del__r.Name;
                routeGroupDels.LoadTimeString = d.Route__r.LoadTime__c;
                routeGroupId = d.Route_Group_del__c;
            }

            Route_Sheet_Delivery_Row newRow = new Route_Sheet_Delivery_Row();
            String DietString = '';
            String MealTypeString = '';
            String SequenceString = '';
            String CrossStreetString = '';
            if (d.Client__r.Diet__c == 'Mechanically Softened') {
                DietString = 'MS';
            } else if (d.Client__r.Diet__c == 'Low Sodium') {
                DietString = 'LS';
            } else if (d.Client__r.Diet__c == 'Regular') {
                DietString = 'RG';
            } else if (d.Client__r.Diet__c == 'Diabetic') {
                DietString = 'DB';
            } else {
                DietString = d.Client__r.Diet__c;
            }
            if (d.Sequence__c == null) {
                SequenceString = '';
            } else {
                SequenceString = String.valueOf(d.Sequence__c) + '. ';
            }
            if (d.Client__r.Cross_Street__c != null) {
                CrossStreetString = '<strong>Major Intersection: </strong>' + d.Client__r.Cross_Street__c;
            }
            newRow.ClientString = d.Client__r.LastName + ', ' + d.Client__r.FirstName;
            newRow.AddressString = d.Address__c;
            if (d.Client__r.Apartment__c > '') {
                newRow.AddressString += ' # ' + d.Client__r.Apartment__c;
            }
            newRow.CrossStreetString = CrossStreetString;
            newRow.PhoneString = d.Client__r.Phone;
            newRow.DietString = DietString;
            newRow.MealTypeString = d.Client__r.Temperature__c;
            newRow.SequenceString = SequenceString;
            newRow.RouteString = d.Route_Group_del__r.Name;
            newRow.GenderString = d.Client__r.Gender__c;
            newRow.LanguageString = d.Client__r.Primary_Language__c;
            //system.debug('the diet should be ' + d.Client__r.Name);
            newRow.DateString = d.Date__c.format();
            if (d.Client__r.Funding_Source__c == 'MOWSF' || d.Client__r.Funding_Source__c == 'Elderly Nutrition Program') {
                newRow.FundingSourceString = '';
            } else if (d.Client__r.Funding_Source__c == 'DAAS Emergency'){
                newRow.FundingSourceString = 'DAAS';
            } else {
                newRow.FundingSourceString = d.Client__r.Funding_Source__c;
            }
            String dnote = d.Client__r.Delivery_Notes__c;
            if (dnote != null) {
                dnote = dnote;
            } else {
                dnote = '';
            }
            if (specialDeliveryMapIds.containsKey(d.Client__c) == true) {
                List<Delivery__c> csd = specialDeliveryMapIds.remove(d.Client__c);
                newRow.SpecialDeliveryString = '';
                for (Delivery__c sd : csd) {
                    if (sd.Route_Group_Del__c == routeGroupId) {
                        newRow.SpecialDeliveryString += '*** Special Delivery: ' + sd.List_Items__c + ' *** <br/>';
                    }
                }
            } else {
                newRow.SpecialDeliveryString = '';
            }
            newRow.DeliveryNotesString = dnote;
            if (d.Hot__c > 0 && d.Chill_Alt__c > 0) {
                newRow.Quantity = 'H-' + d.Hot__c.toPlainString() + ',C-' + d.Chill_Alt__c.toPlainString();
            } else {
                if (d.Hot__c > 0) {
                    newRow.Quantity = d.Hot__c.toPlainString();
                } else if (d.Chill__c > 0) {
                    newRow.Quantity = (d.Chill__c + d.Chill_Alt__c).toPlainString();
                } else {
                    newRow.Quantity = (d.Frozen__c + d.Frozen_Alt__c).toPlainString();
                }
            }
            newRow.Grid = create_grid(d);
            if (d.Special__c == false) {
                updateSummary(d);
            }
            return_deliveries.add(newRow);
            clientCountMap.put(d.Client__c, 1);
        }

        if (return_deliveries.size() > 0) {
            if (i == 0) {
                routeGroupDels.isNotFirst = false;
                system.debug('set isNotFirst false');
            } else {
                routeGroupDels.isNotFirst = true;
                system.debug('set isNotFirst true');
            }
            for (Meal_Suspension__c mstemp : mealSuspensionList) {
                if (routeGroupMemberMap.containsKey(mstemp.Client__c)) {
                    for (Id route : routeGroupMemberMap.get(mstemp.Client__c)) {
                        if (route == routeGroupId) {
                            routeGroupDels.suspensionList.add(mstemp);
                        }
                    }
                }
            }
            routeGroupDels.deliveryList = return_deliveries;
            routeGroupDels.SumChillLs = ChillLS;
            routeGroupDels.SumChillDb = ChillDB;
            routeGroupDels.SumChillMs = ChillMS;
            routeGroupDels.SumChillRg = ChillRG;
            routeGroupDels.SumChillLs_alt = ChillLS_alt;
            routeGroupDels.SumChillDb_alt = ChillDB_alt;
            routeGroupDels.SumChillMs_alt = ChillMS_alt;
            routeGroupDels.SumChillRg_alt = ChillRG_alt;
            routeGroupDels.SumHotRg = HotRG;
            routeGroupDels.SumHotLs = HotLS;
            routeGroupDels.SumHotDb = HotDB;
            routeGroupDels.SumHotMs = HotMS;
            routeGroupDels.SumFrozenRg = FrozenRG;
            routeGroupDels.SumFrozenLs = FrozenLS;
            routeGroupDels.SumFrozenDb = FrozenDB;
            routeGroupDels.SumFrozenMs = FrozenMS;
            routeGroupDels.SumFrozenLs_alt = FrozenLS_alt;
            routeGroupDels.SumFrozenDb_alt = FrozenDB_alt;
            routeGroupDels.SumFrozenMs_alt = FrozenMS_alt;
            routeGroupDels.SumFrozenRg_alt = FrozenRG_alt;
            routeGroupDels.ClientCount = clientCountMap.size();
            routeGroupDels.generateSummary();
            routeGroupDeliveriesList.add(routeGroupDels);
            return_deliveries = new List<Route_Sheet_Delivery_Row>{};
        }
        return routeGroupDeliveriesList;
    }
    public List<Meal_Suspension__c> getMeal_suspensions(Date genDate) {
        List<Meal_Suspension__c> suspensionList = [SELECT Client__c, End_Date__c, Start_Date__c,
                Client__r.FirstName, Client__r.LastName, Reason__c
        FROM Meal_Suspension__c
        WHERE (Start_Date__c <= :genDate) AND
        ((End_Date__c > :genDate) OR (End_Date__c = null))
        ORDER BY Client__r.LastName, Client__r.FirstName];
        //Map<Id, List<Meal_Suspension__c>> suspensionMap = new Map<Id, List<Meal_Suspension__c>>();
        //for (Meal_Suspension__c ms : suspensionList) {
        //    system.debug(ms.Client__c);
        //    if (suspensionMap.containsKey(ms.Client__c)) {
        //        suspensionMap.get(ms.Client__c).add(ms);
        //    } else {
        //        List<Meal_Suspension__c> msl = new List<Meal_Suspension__c>();
        //        msl.add(ms);
        //        suspensionMap.put(ms.Client__c,msl);
        //    }
        //}
        return suspensionList;
    }
    public Map<Id, Set<Id> > getRouteGroupMemberMap() {
        List<Route_Group_Member__c> rgmList = [SELECT
            RG_Member_Contact__c,
            Route__c
            FROM Route_Group_Member__c
            WHERE RG_Member_Contact__r.Diet__c != NULL AND RG_Member_Contact__r.Temperature__c != NULL
            AND RG_Member_Contact__r.Status__c != 'Inactive'
            ORDER BY Sequence__c];
        Map<Id, Set<Id>> rgmMap = new Map<Id, Set<Id>>();
        for (Route_Group_Member__c rgm : rgmList) {
            if (rgmMap.containsKey(rgm.RG_Member_Contact__c) == false) {
                Set<Id> tset = new Set<Id>();
                tset.add(rgm.Route__c);
                rgmMap.put(rgm.RG_Member_Contact__c,tset);
            } else {
                rgmMap.get(rgm.RG_Member_Contact__c).add(rgm.Route__c);
            }
        }
        return rgmMap;
    }
    public Void updateSummary(Delivery__c d) {
        Decimal dayQuantity = 0;
        Integer packCount = 0;
        //system.debug('chill rg alt entering ' + ChillRG_alt);
        if (dayOfWeek == 'Mon') {
            dayQuantity = d.Meal_Scheduler__r.Mon_Quantity__c;
        } else if (dayOfWeek == 'Tue') {
            dayQuantity = d.Meal_Scheduler__r.Tue_Quantity__c;
        } else if (dayOfWeek == 'Wed') {
            dayQuantity = d.Meal_Scheduler__r.Wed_Quantity__c;
        } else if (dayOfWeek == 'Thu') {
            dayQuantity = d.Meal_Scheduler__r.Thu_Quantity__c;
        } else if (dayOfWeek == 'Fri') {
            dayQuantity = d.Meal_Scheduler__r.Fri_Quantity__c;
        } else if (dayOfWeek == 'Sat') {
            dayQuantity = d.Meal_Scheduler__r.Sat_Quantity__c;
        }
        if (dayQuantity < 9 && dayQuantity > 0) {
            packCount = 1;
        }
        if (dayQuantity > 9) {
            packCount = 2;
        }
        if (d.Client__r.Temperature__c == 'Frozen') { // frozen uses packcount
            if (d.Client__r.Diet__c == 'Regular') {
                FrozenRG += packCount;
            } else if (d.Client__r.Diet__c == 'Low Sodium') {
                FrozenLS += packCount;
            } else if (d.Client__r.Diet__c == 'Diabetic') {
                FrozenDB += packCount;
            } else if (d.Client__r.Diet__c == 'Mechanically Softened') {
                FrozenMS += packCount;
            } else {
                system.debug('no update frozen ' + d.Client__r.Diet__c);

            }
        } else { // Hot and Chill get treated the same but hot clients receive chill on Friday
            if (d.Client__r.Temperature__c == 'Hot') {
                if (d.Chill_Alt__c > 0) {
                    HotToChillClients += 1;
                }
            }
            if (d.Client__r.Diet__c == 'Regular') {
                HotRG += d.Hot__c / 2;
                ChillRG += d.Chill__c / 2;
                ChillRG_alt += d.Chill_Alt__c/2;
            } else if (d.Client__r.Diet__c == 'Low Sodium') {
                HotLS += d.Hot__c / 2;
                ChillLS += d.Chill__c / 2;
                ChillLS_alt += d.Chill_Alt__c/2;
            } else if (d.Client__r.Diet__c == 'Diabetic') {
                HotDB += d.Hot__c / 2;
                ChillDB += d.Chill__c / 2;
                ChillDB_alt += d.Chill_Alt__c/2;
            } else if (d.Client__r.Diet__c == 'Mechanically Softened') {
                HotMS += d.Hot__c / 2;
                ChillMS += d.Chill__c / 2;
                ChillMS_alt += d.Chill_Alt__c/2;
            } else {
                system.debug('no update Hot and Chill ' +  d.Client__r.Diet__c);

            }
        }
        //system.debug('chill rg alt leaving ' + ChillRG_alt);

    }
}