/**
 * Created by banghart on 3/13/18.
 */

public with sharing class RouteManagerController {
    public Boolean selected {get;set;}
    public String oldRouteName {get;set;}
    public String newRouteName {get;set;}
    public String pageMessage {get;set;}
    public List<cRoute> Routes = new List<cRoute>();
    public ApexPages.StandardSetController setCon {
        get {
            oldRouteName = '';
            newRouteName = '';
            pageMessage = '';
            return setCon;
        }
        set;
    }
    public PageReference cloneSelected() {
        system.debug('processing selected');
        system.debug(oldRouteName + ', ' + newRouteName);
        List<Route__c> selectedRoutes = new List<Route__c>();
        for (cRoute r: getRoutes()) {
            if (r.selected == true) {
                selectedRoutes.add(r.Route);
            }
        }
        if (selectedRoutes.size() > 1 || selectedRoutes.size() == 0) {
            pageMessage = 'You must select one Route to clone. ';
            return null;
        }
        if (oldRouteName == '' || newRouteName == '') {
            if (pageMessage > '') {
                pageMessage += ' Please enter a new name for the cloned route and a name for the new route.';
            } else {
                pageMessage = 'Please enter a new name for the cloned route and a name for the new route.';
            }
            return null;
        }
        if (selectedRoutes.size() == 1) {
            for (Route__c r : selectedRoutes) {
                r.Name = oldRouteName;
                Route__c newRoute = new Route__c();
                newRoute = r.clone();
                newRoute.Name = newRouteName;
                insert newRoute;
                List<Route_Group_Member__c> routeGroupMembers = [SELECT RG_Member_Contact__c, Sequence__c FROM Route_Group_Member__c WHERE Route__c = :r.Id];
                List<Route_Group_Member__c> newRoute_group_members = new List<Route_Group_Member__c>();
                for (Route_Group_Member__c rgm: routeGroupMembers) {
                    Route_Group_Member__c newRoute_group_member = new Route_Group_Member__c();
                    newRoute_group_member.Sequence__c = rgm.Sequence__c;
                    newRoute_group_member.Route__c = newRoute.Id;
                    newRoute_group_member.RG_Member_Contact__c = rgm.RG_Member_Contact__c;
                    newRoute_group_members.add(newRoute_group_member);
                }
                if (newRoute_group_members.size() > 0) {
                    insert newRoute_group_members;
                    pageMessage = 'Route was cloned with ' + newRoute_group_members.size() + ' members.';
                } else {
                    pageMessage = 'Route was cloned, but has no Clients';
                }
                system.debug('retrieved member count of: ' + routeGroupMembers.size());
            }
        }
        system.debug('found ' + selectedRoutes.size() + ' routes selected ');
        Routes.clear();
        return null;
    }
    public PageReference resequenceSelected() {
        List<Route__c> selectedRoutes = new List<Route__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        for (cRoute r: getRoutes()) {
            if (r.selected == true) {
                selectedRoutes.add(r.Route);
            }
        }
        if (selectedRoutes.size() == 0) {
            pageMessage = 'No Route was selected to re-sequence.';
        }
        for (Route__c r: selectedRoutes) {
            routeGroupMembers = [SELECT Id, Sequence__c FROM Route_Group_Member__c WHERE Route__c = :r.Id ORDER BY Sequence__c];
            Integer offset = 5;
            Integer seq = 0;
            for (Route_Group_Member__c rgm: routeGroupMembers) {
                rgm.Sequence__c = seq;
                seq += offset;
            }
            Update routeGroupMembers;
            pageMessage = 'The selected Route was re-sequenced.';
        }
        return null;
    }
    public List<Contact> getClients() {
        List<Contact> clients = [SELECT Id, Name, MailingStreet FROM Contact WHERE RecordType.Name = 'Client Record'
                 AND Status__c = 'Active' ORDER BY lastname LIMIT 500];
        return clients;
    }
    public List<cRoute> getRoutes() {
        if (Routes.size() == 0) {
            for (Route__c r : [SELECT Id, Name, Mon__c, Tue__c, Wed__c, Thu__c, Fri__c, Sat__c, Load_Time__c, Route_Type__c, Is_Active__c FROM Route__c ORDER BY Name]) {
                Routes.add(new cRoute(r));
            }
        }
        return Routes;
    }
    public class cRoute {
        public Route__c Route {get;set;}
        public Boolean selected {get;set;}
        public cRoute (Route__c r) {
            Route = r;
            selected = false;
        }
    }
}