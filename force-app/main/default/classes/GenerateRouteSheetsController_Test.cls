/**
 * Created by banghart on 3/2/18.
 */


@IsTest(SeeAllData=false)
public with sharing class GenerateRouteSheetsController_Test {

    public static testMethod void testRunAs() {
        // Instantiate a new controller with all parameters in the page
        GenerateRouteSheetsController RSheetsController = new GenerateRouteSheetsController();
        RSheetsController.delivery_date = date.today();
        List<SelectOption> RouteOptions = RSheetsController.getRouteOptions();
        RSheetsController.generateRouteSheets();
        system.debug('in the generate route sheets test ');
        //System.assertEquals(controller.login(),null);
    }
}