/**
 * Created by banghart on 6/2/18.
 */

global class batchUpdateClientStatus implements Database.Batchable<sObject>{
    global List<Meal_Suspension__c> suspensions;
    global List<Funding_Source_Information__c> fsis;
    global List<Control_Settings__c> controls;
    global Map<Id, List<Meal_Suspension__c> > suspensionMap = new Map<Id, List<Meal_Suspension__c>>();
    global Map<Id, List<Funding_Source_Information__c>> fsimap = new Map<Id, List<Funding_Source_Information__c>>();
    global Date dateToday = Date.today();
    global batchUpdateClientStatus() {
        Date updateDate = Date.today();
        controls = [SELECT Assessment_Cycle_Enable__c FROM Control_Settings__c];
        for (Control_Settings__c control :controls) {
            control.Assessment_Cycle_Enable__c = false;
        }
        update controls;
        suspensions = [SELECT Id, Client__c, Start_Date__c, End_Date__c FROM Meal_Suspension__c WHERE Start_Date__c <= :updateDate
            AND (End_Date__c = null OR End_Date__c > :updateDate)];
        for (Meal_Suspension__c msusp :suspensions) {
            if (suspensionMap.containsKey(msusp.Client__c)) {
                suspensionMap.get(msusp.Client__c).add(msusp);
            } else {
                List<Meal_Suspension__c> suspList = new List<Meal_Suspension__c>();
                suspList.add(msusp);
                suspensionMap.put(msusp.Client__c, suspList);
            }
        }
        system.debug('ms retrieved: ' + suspensions.size());
        fsis = [SELECT Id, Contact__c, Funding_Source__c, Start_Date__c, End_Date__c FROM Funding_Source_Information__c WHERE (Contact__r.Stop_Date__c = null OR
            Contact__r.Stop_Date__c > :dateToday) ];
        for (Funding_Source_Information__c fsi : fsis) {
            if (fsimap.containsKey(fsi.Contact__c)) {
                fsimap.get(fsi.Contact__c).add(fsi);
            } else {
                List<Funding_Source_Information__c> fsilist = new List<Funding_Source_Information__c>();
                fsilist.add(fsi);
                fsimap.put(fsi.Contact__c,fsilist);
            }
        }

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {

        return Database.getQueryLocator([SELECT Id, Status__c, FirstName, LastName FROM Contact WHERE Status__c != 'Inactive' AND RecordType.Name = 'Client Record' AND (Stop_Date__c = null OR Stop_Date__c > :dateToday ) ] );
    }
    global void execute(Database.BatchableContext BC, List<Contact> clients){
        system.debug('begin execute portion of batch update client status');
        for (Contact c :clients) {
            if (suspensionMap.containsKey(c.Id) == true) {
                for (Meal_Suspension__c ms :suspensionMap.get(c.Id)) {
                    if (ms.End_Date__c == null) {
                        c.Status__c = 'Suspended no resume date';
                    } else {
                        c.Status__c = 'Suspended with resume date';
                    }
                }
            } else {
                c.Status__c = 'Active';
            }
            if (fsimap.containsKey(c.Id) == true) {
                List<Funding_Source_Information__c> fsilist = fsimap.get(c.Id);
                for (Funding_Source_Information__c fsi :fsilist) {
                    if (fsi.Start_Date__c <= dateToday && (fsi.End_Date__c == null || fsi.End_Date__c > dateToday)) {
                        c.Funding_Source__c = fsi.Funding_Source__c;
                    }

                }
            }
        }
        update clients;
    }
    global void finish(Database.BatchableContext BC){
        system.debug('******  batch update of client status is finished ****** ');
        for (Control_Settings__c control :controls) {
            control.Assessment_Cycle_Enable__c = true;
        }
        update controls;
        //update clients;
    }


}