/**
 * Created by banghart on 11/12/17.
 */
@IsTest(SeeAllData=false)
public with sharing class Generate_Deliveries_Test {
    public static testMethod void testRunAs() {
        Date genDate = Date.today();
        String dateString = genDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        if (dayOfWeek == 'Sun' || dayOfWeek == 'Sat') {
            genDate = genDate.addDays(3);
        }
        createTestData(genDate);
        List<Date> dateList = new List<Date>();
        dateList.add(genDate);
        Generate_Deliveries gd = new Generate_Deliveries();
        gd.do_generation(dateList);
        gd.numDays = 7;
        List<Delivery__c> deleteDeliveries = [SELECT Id From Delivery__c WHERE Date__c = :genDate];
        delete deleteDeliveries;
        List<Date> genList = gd.prepare_date_list(genDate,7);
        gd.do_generation(genList);
        gd.do_generation(genList); // tests for refresh
        deleteDeliveries = [SELECT Id From Delivery__c WHERE Date__c = :genList[0]];
        delete deleteDeliveries;
        gd.holiday_date = genDate + 7;
        gd.combine_holiday = true;
        gd.do_generation(genList);
    }
    public static Void createTestData(Date genDate) {
        List<Route__c> routes = new List<Route__c>();
        routes.add(new Route__c(Name = 'test', Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'test2',Route_Type__c = 'Frozen', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        insert routes;
        List<Contact> c = new List<Contact>();
        c.add(new Contact(FirstName = 'A', LastName = 'B', Diet__c = 'Regular', Temperature__c = 'Chilled',  Status__c = 'Active'));
        c.add(new Contact(FirstName = 'AA', LastName = 'BB', Diet__c = 'Regular', Temperature__c = 'Frozen', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'CC', LastName = 'CC', Diet__c = 'Regular', Temperature__c = 'Hot', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'Dan', LastName = 'Driver', MOWSF_Staff__c = true));
        insert c;
        c = [SELECT Id FROM Contact WHERE MOWSF_Staff__c = false];
        List<Meal_Scheduler__c> mealSchedulers = new List<Meal_Scheduler__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        for (Contact client : c) {
            mealSchedulers.add(new Meal_Scheduler__c(Client__c = client.Id, Mon__c = true, Tue__c = true, Wed__c = true,
                    Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                    Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
            routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = client.Id, Route__c = routes[0].Id));
            routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = client.Id, Route__c = routes[1].Id));
            system.debug(client.Id + 'is a client');
        }
        c = [SELECT Id FROM Contact WHERE MOWSF_Staff__c = true];
        routes = [SELECT Id FROM Route__c];
        routes[0].Driver_1__c = c[0].Id;
        routes[1].Driver_1__c = c[0].Id;
        insert mealSchedulers;
        insert routeGroupMembers;
        List<Delivery__c> specialDeliveries = new List<Delivery__c>();
        specialDeliveries.add(new Delivery__c( Quantity__c = 1, Date__c = genDate ,Special__c = true, Client__c = c[0].Id, Route_Group_Del__c = routes[0].Id));
        insert specialDeliveries;
        System.debug('number of routes created was ' + routes.size());


    }

}