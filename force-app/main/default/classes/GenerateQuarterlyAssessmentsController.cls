public with sharing class GenerateQuarterlyAssessmentsController {

    public ApexPages.StandardController standardController;
    public String pageMessageString {get; set;}

    public GenerateQuarterlyAssessmentsController(ApexPages.StandardController standardController)
    {
        this.standardController = standardController;
        pageMessageString = 'Click "Generate" and wait for this message to be updated';

    }

    public PageReference generateAssessments()
    {
        pageMessageString = 'Click "Generate" and wait for this message to be updated';
        Id recordID = standardController.getId();

        List<Quarterly_Assessment__c> existingQas = [SELECT Id, Client__c FROM Quarterly_Assessment__c WHERE Period__c = :recordID];
        Map<Id, String> qasMap = new Map<Id, String>();
        for (Quarterly_Assessment__c qa : existingQas) {
            qasMap.put(qa.Client__c,'exists');
        }
        List<Quarterly_Assessment__c> qas = new List<Quarterly_Assessment__c>();
        system.debug('found ' + qasMap.size() + ' existing quarterly assessments');
		List<Contact> cs = [Select Id, OwnerID,  FirstName, LastName, MailingStreet, Suspended__c,Social_Worker__c
            FROM Contact
            WHERE Status__c != 'Inactive' AND
            Funding_Source__c = 'Elderly Nutrition Program' AND
            Age__c >= 60 AND
            RecordType.Name = 'Client Record'];
        system.debug('found ' + cs.size() + ' clients');
		for (Contact c : cs) {
            system.debug('from the map -> ' + qasMap.get(c.Id));
            if (c.Social_Worker__c != NULL && qasMap.get(c.Id) != 'exists') {
                Quarterly_Assessment__c qa = new Quarterly_Assessment__c(Client__c = c.Id, OwnerID = c.Social_Worker__c, Period__c = recordID, Suspended__c = c.Suspended__c, Address__c = c.MailingStreet, Client_Last_Name__c = c.LastName, Client_First_Name__c = c.FirstName);
                qas.add(qa);
            }  else {
                if (qasMap.get(c.Id) != 'exists') {
                    Quarterly_Assessment__c qa = new Quarterly_Assessment__c(Client__c = c.Id, Period__c = recordID, Suspended__c = c.Suspended__c, Address__c = c.MailingStreet, Client_Last_Name__c = c.LastName, Client_First_Name__c = c.FirstName);
                    qas.add(qa);
                }
            }
		}
        system.debug('created ' + qas.size() + ' qas ');
        if(!qas.isEmpty())
        {
            insert qas;
        }
        pageMessageString = 'Generated ' + qas.size() + ' quarterly assessments';
        return null;
    }

    public PageReference deleteAssessments()
    {
        Id recordID = standardController.getId();
        List<Quarterly_Assessment__c> qasToDeleteList = [select ID from quarterly_assessment__c Where Period__c = :recordID];

        Integer deletedInteger = qasToDeleteList.size();
        delete qasToDeleteList;
        //delete [select ID from quarterly_assessment__c Where Period__c = :recordID];
        pageMessageString = 'deleted ' + deletedInteger + ' quarterly assessments';
        return null;
    }

}