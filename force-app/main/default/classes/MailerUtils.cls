public class MailerUtils {
	public static Messaging.SingleEmailMessage constructEmail(string subject, string message, string[] recipients) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//		String[] toAddress = new String[] {recipient};
		mail.setToAddresses(recipients);
		mail.setSubject(subject);
		mail.setUseSignature(false);
		mail.setHtmlBody(message);
		mail.setSenderDisplayName('Salesforce System');
		mail.setSaveAsActivity(false);

		return mail;
		}
}