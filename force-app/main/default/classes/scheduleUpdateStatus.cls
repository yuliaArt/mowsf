/**
 * Created by banghart on 6/3/18.
 */

global class scheduleUpdateStatus implements Schedulable {
    global void execute (SchedulableContext sc) {
        batchUpdateClientStatus b = new batchUpdateClientStatus();
        Database.executeBatch(b);
    }

}