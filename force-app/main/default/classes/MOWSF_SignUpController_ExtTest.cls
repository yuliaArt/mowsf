@isTest
private class MOWSF_SignUpController_ExtTest {
    
   
    static testMethod void testPrograms() {
        MOWSF_SignUpController_Ext mowsfExt = new MOWSF_SignUpController_Ext();
        List<MOWSF_SignUpController_Ext.CampaignWrapper> campList = mowsfExt.getPrograms();
        
        List<Campaign> listOfCampaigns = [SELECT Id,Name,Description FROM Campaign WHERE Display_on_Web_Interest_Form__c = TRUE];
        System.assertEquals(listOfCampaigns.size(), campList.size());
    }
    
    static testMethod void testGetSelected(){
        
        MOWSF_SignUpController_Ext mowsfExt = new MOWSF_SignUpController_Ext();
        List<MOWSF_SignUpController_Ext.CampaignWrapper> campList = mowsfExt.getPrograms();
        
        for(MOWSF_SignUpController_Ext.CampaignWrapper ca: campList){
            ca.selected = true;
        }
        System.assertEquals(null,mowsfExt.getSelected());
        System.assertEquals(mowsfExt.selectedCampaigns.size(), campList.size());
    }
    
    static testMethod void testSaveContact()
    {
       MOWSF_SignUpController_Ext mowsfExt = new MOWSF_SignUpController_Ext();
       Test.startTest();
       mowsfExt.saveContact();
       System.assertEquals(false, mowsfExt.insertContact());
       System.assertEquals('Thank you for your interest in volunteering with MOWSF. Check your inbox for further instructions to attend an orientation session.',mowsfExt.insertVolunteerInterests());     
    }
    
     static testMethod void runTest() {
        MOWSF_SignUpController_Ext c = new MOWSF_SignUpController_Ext();
        c.test_Only();
      }
    
}