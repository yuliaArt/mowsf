//written by David Smith
// modified by Rick Banghart
//To-Do
public class Generate_Deliveries {
    public Integer numDays {get;set;}
    public Delivery__c tempDelivery;
    public Date holiday_date {get;set;}
    public Boolean combine_holiday {get;set;}
    //constructors
    public Generate_Deliveries() {
        system.debug('in the Generate_Deliveries constructor');
    }
    public List<Date> prepare_date_list(Date startDate, Integer numToMake) {
        List<Date> returnDateList = new List<Date>();
        for (Integer i = 0; i < numToMake; i++) {
            returnDateList.add(startDate);
            startDate = startDate.addDays(1);
        }
        return returnDateList;
    }
    public Integer do_generation(List<Date> genDates) {
        Date todayDate = Date.today();
        system.debug('generating deliveries, combine holiday is ' + combine_holiday);
        system.debug('generating deliveries, holiday date is ' + holiday_date);
        //updateClientStatus();
        String existDelSOQLCondition = ' WHERE ';
        Boolean firstDate = true;
        Boolean pastDate = false;
        String yearString = '';
        String monthString = '';
        String dayString = '';
        String formattedDateString;
        Map<String, Integer> typeQuantities = new Map<String, Integer>();

        for (Date gendate : genDates) {
            if (todayDate.daysBetween(gendate) < 0) {
                // do nothing if gendate is in the past
                return 0;
            }
            yearString = String.valueOf(gendate.year());
            monthString = '0' + String.valueOf(gendate.month());
            monthString = monthString.right(2);
            dayString = '0' + String.valueOf(gendate.day());
            dayString = dayString.right(2);
            formattedDateString = yearString + '-' + monthString + '-' + dayString;
            if (firstDate == true) {
                existDelSOQLCondition += 'Date__c = ' + formattedDateString;
            } else {
                existDelSOQLCondition += ' OR Date__c = ' + formattedDateString;
            }
            firstDate = false;
        }
        // *** Retrieve meal schedulers
        List<Meal_Scheduler__c> msList = [SELECT Mon__c, Tue__c, Wed__c, Thu__c, Fri__c, Sat__c, Sun__c,
                Mon_Quantity__c, Tue_Quantity__c, Wed_Quantity__c, Thu_Quantity__c, Fri_Quantity__c,
                Sat_Quantity__c, Sun_Quantity__c, Client__r.Temperature__c, Client__c
                FROM Meal_Scheduler__c
                WHERE (Client__r.Status__c != 'Inactive') AND Client__r.Diet__c != NULL ];
        Map <Id, Meal_Scheduler__c> msMap = new Map<Id, Meal_Scheduler__c>();
        for (Meal_Scheduler__c ms : msList) {
            msMap.put(ms.Client__c, ms);
            System.debug('retrieved meal scheduler for: ' + ms.Client__c);
        }
        // *** Retrieve route groups
        List<Route__c> rgList = [SELECT Id, Mon__c, Tue__c, Wed__c, Thu__c, Fri__c, Sat__c, Driver_1__c, Load_Time_txt__c FROM Route__c];
        Map<Id, Route__c> rgMap = new Map<Id, Route__c>();
        for (Route__c rg : rgList) {
            rgMap.put(rg.Id, rg);
        }
        // Retrieve existing deliveries in the date range
        String existDelQry = 'SELECT Id, Client__c, Date__c,' +
                'Address__c, Zip__c, Diet__c, Temperature__c, Quantity__c, Sequence__c,' +
                'Route_Group_Del__c, Side_Bag__c, Special__c, Driver_1__c, Load_Time__c FROM Delivery__c ' + existDelSOQLCondition;
        List<Delivery__c> existingDeliveries = Database.query(existDelQry);
        System.debug('existing delivery count: ' + existingDeliveries.size());
        Map<String, List<Delivery__c>> existingDeliveryMap = new Map<String, List<Delivery__c>>();
        List<Delivery__c> deliveriesToDelete = new List<Delivery__c>();
        for (Delivery__c d : existingDeliveries) {
            if (d.Special__c == true) {
                System.debug('****** Found a special delivery ********');
            }
            if (msMap.containsKey(d.Client__c) && d.Special__c == false) {
                if (checkDelivery(msmap.get(d.Client__c), rgMap.get(d.Route_Group_del__c),d.Date__c) == true) {
                    // We need this delivery but will need to update it.
                    if(existingDeliveryMap.containsKey(d.Client__c + d.Date__c.format())) {
                        existingDeliveryMap.get(d.Client__c + d.Date__c.format()).add(d);
                    } else {
                        List<Delivery__c> newList = new List<Delivery__c>();
                        newList.add(d);
                        existingDeliveryMap.put(d.Client__c + d.Date__c.format(), newList);
                    }
                } else {
                    // no longer need this delivery, don't add this to list to check
                    // Add it to the List to delete
                    system.debug('remove');
                    deliveriesToDelete.add(d);
                }
            } else {
                // here if Special__c is true
                if(existingDeliveryMap.containsKey(d.Client__c + d.Date__c.format())) {
                    existingDeliveryMap.get(d.Client__c + d.Date__c.format()).add(d);
                } else {
                    List<Delivery__c> newList = new List<Delivery__c>();
                    newList.add(d);
                    existingDeliveryMap.put(d.Client__c + d.Date__c.format(), newList);
                }

            }
        }

        List<Meal_Suspension__c> suspensionList = [SELECT Client__c, End_Date__c, Start_Date__c
        FROM Meal_Suspension__c
        WHERE (Start_Date__c <= :genDates[genDates.size() - 1]) AND
        ((End_Date__c > :genDates[0]) OR (End_Date__c = null))];
        Map<Id, List<Meal_Suspension__c>> suspensionMap = new Map<Id, List<Meal_Suspension__c>>();
        for (Meal_Suspension__c ms : suspensionList) {
            if (suspensionMap.containsKey(ms.Client__c)) {
                suspensionMap.get(ms.Client__c).add(ms);
            } else {
                List<Meal_Suspension__c> msl = new List<Meal_Suspension__c>();
                msl.add(ms);
                suspensionMap.put(ms.Client__c,msl);
            }
        }
        List<Route_Group_Member__c> rgMembers = [SELECT RG_Member_Contact__r.MailingAddress,
                RG_Member_Contact__r.FirstName,
                RG_Member_Contact__c,
                RG_Member_Contact__r.LastName,
                RG_Member_Contact__r.Diet__c,
                RG_Member_Contact__r.Primary_Route__c,
                RG_Member_Contact__r.Temperature__c,
                Sequence__c,
                Route__c,
                Route__r.Driver_1__c
        FROM Route_Group_Member__c
        WHERE RG_Member_Contact__r.Diet__c != NULL AND RG_Member_Contact__r.Temperature__c != NULL
        AND RG_Member_Contact__r.Status__c != 'Inactive'
        ORDER BY Sequence__c
        ];
        Set<Delivery__c> deliveriesToUpsert = new Set<Delivery__c>();
        // use a set to avoid duplicating deliveries.
        //
        // We now have existing deliveries, Route Group Members (Clients), Meal Schedulers, Suspensions
        // And are ready to loop
        Set<Delivery__c> tempDeliveriesSet = new Set<Delivery__c>();
        System.debug('about to loop through rg members (size): ' + rgMembers.size());
        Boolean foundMeal = false;
        for (Date gendate : genDates) {
            for (Route_Group_Member__c m : rgMembers) {
                //system.debug(m);
                // decide if this Client gets a delivery on this date
                if ( checkDelivery(msMap.get(m.RG_Member_Contact__c), rgMap.get(m.Route__c),gendate) == true &&
                        checkSuspension(suspensionMap.get(m.RG_Member_Contact__c), gendate) == false) {
                    // create new if no existing delivery
                    if (existingDeliveryMap.containsKey(m.RG_Member_Contact__c + gendate.format())) {
                        foundMeal = false;
                        for (Delivery__c tempDelivery : existingDeliveryMap.get(m.RG_Member_Contact__c + gendate.format())) {
                            if (tempDelivery.Special__c == true) {
                                System.debug('***** Found special is Map **** ');
                            } else {
                                foundMeal = true;
                            }
                            Delivery__c preDelivery = tempDelivery.clone(true, true, true, true);
                            if (tempDelivery.Special__c == false) {
                                // refreshDelivery looks at meal scheduler, special deliveries
                                // have nothing to do with a meal scheduler
                                //system.debug('before refresh ' + tempDelivery.Side_Bag__c);
                                refreshDelivery(m, gendate, tempDelivery);
                                //system.debug('after refresh temp is ' + tempDelivery.Side_Bag__c + ' while previous was ' + preDelivery.Side_Bag__c);
                                typeQuantities = checkQuantity(msMap.get(m.RG_Member_Contact__c), gendate);
                                if (combine_holiday == true) {
                                    system.debug('***** combine holiday is true ******');
                                    if (checkSuspension(suspensionMap.get(m.RG_Member_Contact__c), holiday_date) == false) {
                                        system.debug('***** client is not suspended ****** ');
                                        typeQuantities = addHoliday(msMap.get(m.RG_Member_Contact__c), holiday_date, typeQuantities);
                                        tempDelivery.Holiday__c = holiday_date;
                                    }
                                }
                                tempDelivery.Quantity__c = typeQuantities.get('Hot') + typeQuantities.get('Chill') +
                                        typeQuantities.get('Frozen') + typeQuantities.get('Chill_alt') + typeQuantities.get('Frozen_alt');
                                tempDelivery.Hot__c = typeQuantities.get('Hot');
                                tempDelivery.Chill__c = typeQuantities.get('Chill');
                                tempDelivery.Chill_Alt__c = typeQuantities.get('Chill_alt');
                                tempDelivery.Frozen__c = typeQuantities.get('Frozen');
                                tempDelivery.Side_Bag__c = typeQuantities.get('Sidebag');
                            } else {
                                system.debug('driver for special is: ' + tempDelivery.Driver_1__c);
                                tempDelivery.Driver_1__c = m.Route__r.Driver_1__c;
                                system.debug('no refresh special delivery');
                                system.debug('driver for special is: ' + tempDelivery.Driver_1__c);
                            }
                            if (tempDelivery == preDelivery) {
                            } else {
                                //system.debug('== says they are not equal');
                                deliveriesToUpsert.add(tempDelivery);
                            }
                        }
                        if (foundMeal == false) {
                            Delivery__c del = createDelivery(m, genDate, existingDeliveryMap, msMap.get(m.RG_Member_Contact__c).Id);
                            typeQuantities = checkQuantity(msMap.get(m.RG_Member_Contact__c), gendate);
                            if (combine_holiday == true) {
                                if (checkSuspension(suspensionMap.get(m.RG_Member_Contact__c), holiday_date) == false) {
                                    typeQuantities = addHoliday(msMap.get(m.RG_Member_Contact__c), holiday_date, typeQuantities);
                                    del.Holiday__c = holiday_date;
                                }
                            }
                            del.Quantity__c = typeQuantities.get('Hot') + typeQuantities.get('Chill') + typeQuantities.get('Frozen');
                            del.Hot__c = typeQuantities.get('Hot');
                            del.Chill__c = typeQuantities.get('Chill');
                            del.Chill_Alt__c = typeQuantities.get('Chill_alt');
                            del.Frozen__c = typeQuantities.get('Frozen');
                            del.Frozen_Alt__c = typeQuantities.get('Frozen_alt');
                            del.Side_Bag__c = typeQuantities.get('Sidebag');
                            deliveriesToUpsert.add(del);

                        }
                    } else {
                        system.debug('delivery does not exist, create');
                        Delivery__c del = createDelivery(m, genDate, existingDeliveryMap, msMap.get(m.RG_Member_Contact__c).Id);
                        typeQuantities = checkQuantity(msMap.get(m.RG_Member_Contact__c), gendate);
                        if (combine_holiday == true) {
                            if (checkSuspension(suspensionMap.get(m.RG_Member_Contact__c), holiday_date) == false) {
                                typeQuantities = addHoliday(msMap.get(m.RG_Member_Contact__c), holiday_date, typeQuantities);
                                del.Holiday__c = holiday_date;
                            }
                        }
                        del.Quantity__c = typeQuantities.get('Hot') + typeQuantities.get('Chill') + typeQuantities.get('Frozen');
                        del.Hot__c = typeQuantities.get('Hot');
                        del.Chill__c = typeQuantities.get('Chill');
                        del.Chill_Alt__c = typeQuantities.get('Chill_alt');
                        del.Frozen__c = typeQuantities.get('Frozen');
                        del.Frozen_Alt__c = typeQuantities.get('Frozen_alt');
                        del.Side_Bag__c = typeQuantities.get('Sidebag');
                        deliveriesToUpsert.add(del);
                    }
                } else {
                    // system.debug('no delivery for this rgm');
                }
            }
        }
        system.debug(deliveriesToUpsert);
        List<Delivery__c> deliveriesToUpsertList = new List<Delivery__c>(deliveriesToUpsert);
        upsert deliveriesToUpsertList;
        delete deliveriesToDelete;
        system.debug('above is number deliveries to add');
        system.debug('number to delete: ' + deliveriesToDelete.size());
        return deliveriesToUpsert.size();
    }
    public Integer refreshDelivery (Route_Group_Member__c m, Date genDate, Delivery__c delivery) {
        //system.debug('refreshing a delivery here');
        Integer dummy = 1;
        String address = '';
        String postalCode = '';
        Integer sideBagInteger = 0;
        if (m.RG_Member_Contact__r.Temperature__c == 'Frozen') {
            sideBagInteger = 1;
            //system.debug('sidebag 1');
        } else {
            //system.debug('not frozen');
        }
        if (m.RG_Member_Contact__r.MailingAddress == NULL) {
            address = '';
            postalCode = '';
        } else {
            address = m.RG_Member_Contact__r.MailingAddress.getStreet();
            postalCode = m.RG_Member_Contact__r.MailingAddress.getPostalCode();
        }
        delivery.Client__c = m.RG_Member_Contact__c;
        delivery.Date__c = genDate;
        delivery.Address__c = address;
        delivery.Side_Bag__c = sideBagInteger;
        delivery.Zip__c = postalCode;
        delivery.Diet__c = m.RG_Member_Contact__r.Diet__c;
        delivery.Temperature__c = m.RG_Member_Contact__r.Temperature__c;
        delivery.Route_Group_del__c = m.Route__c;
        delivery.Driver_1__c = m.Route__r.Driver_1__c;
        delivery.Sequence__c = m.Sequence__c;
        delivery.Route__c = m.RG_Member_Contact__r.Primary_Route__c;
        //system.debug(delivery.Side_Bag__c + ' is sidebag');
        return dummy;
    }
    public Delivery__c createDelivery(Route_Group_Member__c m, Date genDate, Map<String, List<Delivery__c>> exDels, Id ms) {
        // exDels Map string key is Client_id + Date__c.format()
        // **** check if there is already a delivery for the client and date
        String address = '';
        String postalCode = '';
        if (m.RG_Member_Contact__r.MailingAddress == NULL) {
            address = '';
            postalCode = '';
        } else {
            address = m.RG_Member_Contact__r.MailingAddress.getStreet();
            postalCode = m.RG_Member_Contact__r.MailingAddress.getPostalCode();
        }
        Delivery__c del = new Delivery__c(
                Client__c = m.RG_Member_Contact__c,
                Meal_Scheduler__c = ms,
                Date__c = genDate,
                Delivery_Status__c = 'Pending',
                Address__c = address,
                Zip__c = postalCode,
                Diet__c = m.RG_Member_Contact__r.Diet__c,
                Temperature__c = m.RG_Member_Contact__r.Temperature__c,
                Route_Group_del__c = m.Route__c,
                Driver_1__c = m.Route__r.Driver_1__c,
                Sequence__c = m.Sequence__c,
                Special__c = false,
                Route__c = m.RG_Member_Contact__r.Primary_Route__c
        );
        return del;
    }
    public Boolean checkSuspension (List<Meal_Suspension__c> suspList, Date checkDate) {
        Boolean checkResult = false;
        if (suspList != null) {
            for (Meal_Suspension__c susp : suspList) {
                system.debug('checking ' + susp.End_Date__c + ' against ' + checkDate);
                if (checkDate >= susp.Start_Date__c && ((susp.End_Date__c == null) || susp.End_Date__c > checkDate)) {
                    checkResult = true;
                }
                system.debug('result is ' + checkResult);
            }
        }
        return checkResult;
    }
    public Map<String, Integer> addHoliday (Meal_Scheduler__c sched, Date checkDate, Map<String, Integer> typeQuantity) {
        String dateString = checkDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        system.debug('adding ' + dayOfWeek + ', ' + sched.Client__r.Temperature__c);
        if (sched.Client__r.Temperature__c == 'Frozen') {
            system.debug('checking frozen quantity for ' + dayOfWeek);
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Frozen_alt', sched.Mon_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Frozen_alt', sched.Tue_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Frozen_alt', sched.Wed_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Frozen_alt', sched.Thu_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                typeQuantity.put('Frozen_alt', sched.Fri_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Sat') {
                typeQuantity.put('Frozen_alt', sched.Sat_Quantity__c.intValue());
                system.debug('set frozen to ' + sched.Sat_Quantity__c);
                return typeQuantity;
            }
        } else if (sched.Client__r.Temperature__c == 'Hot') {
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Chill_alt', sched.Mon_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Chill_alt', typeQuantity.get('Chill') + sched.Tue_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Chill_alt', typeQuantity.get('Chill') + sched.Wed_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Chill_alt',typeQuantity.get('Chill') + sched.Thu_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                if (sched.Sat_Quantity__c > 0 || sched.Sat__c == true || sched.Sun_Quantity__c > 0 || sched.Sun__c == true) {
                    typeQuantity.put('Chill_alt', sched.Sat_Quantity__c.intValue() + sched.Sun_Quantity__c.intValue());
                }
            }
            return typeQuantity;
        } else if (sched.Client__r.Temperature__c == 'Chilled') {
            // system.debug('doing chill');
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Chill_alt', sched.Mon_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Chill_alt', sched.Tue_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Chill_alt', sched.Wed_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Chill_alt', sched.Thu_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                system.debug('the day is friday');
                typeQuantity.put('Chill_alt', sched.Sat_Quantity__c.intValue() + sched.Sun_Quantity__c.intValue());
                system.debug(typeQuantity.get('Chill') + ' chill quantity');
                return typeQuantity;
            }
        }
        return typeQuantity;
    }
    public Map<String, Integer> checkQuantity (Meal_Scheduler__c sched, Date checkDate) {
        Map<String, Integer> typeQuantity = new Map<String, Integer>();
        typeQuantity.put('Hot', 0);
        typeQuantity.put('Chill', 0);
        typeQuantity.put('Chill_alt', 0);
        typeQuantity.put('Frozen', 0);
        typeQuantity.put('Frozen_alt', 0);
        typeQuantity.put('Sidebag', 0);
        String dateString = checkDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        //system.debug('checking quantity ' + dayOfWeek + ', ' + sched.Client__r.Temperature__c);
        // lots of idiosyncratic logic here
        if (sched.Client__r.Temperature__c == 'Frozen') {
            typeQuantity.put('Hot',0);
            typeQuantity.put('Chill',0);
            //system.debug('checking frozen quantity for ' + dayOfWeek);
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Frozen', sched.Mon_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Mon_Quantity__c.intValue()));
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Frozen', sched.Tue_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Tue_Quantity__c.intValue()));
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Frozen', sched.Wed_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Wed_Quantity__c.intValue()));
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Frozen', sched.Thu_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Thu_Quantity__c.intValue()));
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                typeQuantity.put('Frozen', sched.Fri_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Fri_Quantity__c.intValue()));
                return typeQuantity;
            }
            if (dayOfWeek == 'Sat') {
                typeQuantity.put('Frozen', sched.Sat_Quantity__c.intValue());
                typeQuantity.put('Sidebag',sideBagCount(sched.Sat_Quantity__c.intValue()));
                system.debug('set frozen to ' + sched.Sat_Quantity__c);
                return typeQuantity;
            }
        } else if (sched.Client__r.Temperature__c == 'Hot') {
            typeQuantity.put('Frozen', 0);
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Hot', sched.Mon_Quantity__c.intValue());
                typeQuantity.put('Chill',0);
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Hot', sched.Tue_Quantity__c.intValue());
                typeQuantity.put('Chill',0);
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Hot', sched.Wed_Quantity__c.intValue());
                typeQuantity.put('Chill',0);
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Hot', sched.Thu_Quantity__c.intValue());
                typeQuantity.put('Chill',0);
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                system.debug('doing friday');
                typeQuantity.put('Hot', sched.Fri_Quantity__c.intValue());
                if (sched.Sat_Quantity__c > 0 || sched.Sat__c == true || sched.Sun_Quantity__c > 0 || sched.Sun__c == true) {
                    typeQuantity.put('Chill_alt', sched.Sat_Quantity__c.intValue() + sched.Sun_Quantity__c.intValue());
                    system.debug('populated Chill alt  ******************** ');
                }  else {
                    typeQuantity.put('Chill', 0);
                }
            }
            return typeQuantity;
        } else if (sched.Client__r.Temperature__c == 'Chilled') {
            //system.debug('doing chill');
            typeQuantity.put('Frozen', 0);
            typeQuantity.put('Hot', 0);
            if (dayOfWeek == 'Mon') {
                typeQuantity.put('Chill', sched.Mon_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Tue') {
                typeQuantity.put('Chill', sched.Tue_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Wed') {
                typeQuantity.put('Chill', sched.Wed_Quantity__c.intValue());
                typeQuantity.put('Chill_alt', sched.Fri_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Thu') {
                typeQuantity.put('Chill', sched.Thu_Quantity__c.intValue());
                return typeQuantity;
            }
            if (dayOfWeek == 'Fri') {
                //system.debug('the day is friday');
                typeQuantity.put('Chill', sched.Sat_Quantity__c.intValue() + sched.Sun_Quantity__c.intValue());
                system.debug(typeQuantity.get('Chill') + ' chill quantity');
                return typeQuantity;
            }
        }
        return typeQuantity;
    }
    public Integer sideBagCount (Integer msFrozenCount) {
        Integer sbc = 0;
        if (msFrozenCount < 6) {
            sbc = 0;
            return sbc;
        }
        if (msFrozenCount > 5 && msFrozenCount < 9) {
            sbc = 1;
            return sbc;
        }
        if (msFrozenCount > 9) {
            sbc = 2;
        }
        return sbc;
    }
    public Boolean checkDelivery(Meal_Scheduler__c ms, Route__c rg,Date checkDate) {
        // determines if a delivery should be created for this Client on this Date
        // check for Meal Suspension before checking here
        Boolean checkResult = false;
        if (ms == null) {
            return checkResult;
        }
        String dateString = checkDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        //system.debug('check delivery function in Generate_Deliveries.cls');
        //system.debug('date is ' + checkDate + ' ms is ' + ms);
        //system.debug('route group is ' + rg);
        if (dayOfWeek == 'Mon' && ms.Mon__c == true && ms.Mon_Quantity__c > 0 && rg.Mon__c == true) {
            checkResult = true;
            return checkResult;
        }
        if (dayOfWeek == 'Tue' && ms.Tue__c == true && ms.Tue_Quantity__c > 0 && rg.Tue__c == true) {
            checkResult = true;
            return checkResult;
        }
        if (dayOfWeek == 'Wed' && ms.Wed__c == true && ms.Wed_Quantity__c > 0 && rg.Wed__c == true) {
            checkResult = true;
            return checkResult;
        }
        if (dayOfWeek == 'Thu' && ms.Thu__c == true && ms.Thu_Quantity__c > 0 && rg.Thu__c == true) {
            checkResult = true;
            return checkResult;
        }

        if (dayOfWeek == 'Fri' && ms.Fri__c == true && ms.Fri_Quantity__c > 0 && rg.Fri__c == true) {
            try {
                if (ms.Client__r.Temperature__c == 'Chilled' && ms.Sat_Quantity__c == 0 && ms.Sun_Quantity__c == 0) {
                    checkResult = false;
                } else {
                    checkResult = true;
                }
            } catch (ListException e){
                system.debug('Error:' + e);

            }

            return checkResult;
        }
        if (dayOfWeek == 'Sat' && ms.Sat__c == true && ms.Sat_Quantity__c > 0 && rg.Sat__c == true) {
            checkResult = true;
            return checkResult;
        }
        if (dayOfWeek == 'Sun' && ms.Sun__c == true && ms.Sun_Quantity__c > 0) {
            // we don't schedule delivery on sunday
            checkResult = false;
            return checkResult;
        }
        return checkResult;
    }

}