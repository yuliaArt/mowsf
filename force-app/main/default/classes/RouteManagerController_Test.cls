/**
 * Created by banghart on 3/15/18.
 */
@isTest(SeeAllData=true)
public with sharing class RouteManagerController_Test {
    public static testMethod void testRunAs() {
        RouteManagerController routeManagerController = new RouteManagerController();
        List<RouteManagerController.cRoute> routes = routeManagerController.getRoutes();
        List<Contact> contacts = routeManagerController.getClients();
        routes[0].selected = true;
        routeManagerController.resequenceSelected();
        routeManagerController.cloneSelected();
    }

}