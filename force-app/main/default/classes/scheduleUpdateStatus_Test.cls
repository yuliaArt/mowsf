/**
 * Created by banghart on 6/4/18.
 */
@isTest
public with sharing class scheduleUpdateStatus_Test {
    public static testMethod void testRunAs() {
        scheduleUpdateStatus b = new scheduleUpdateStatus();
        String sch = '0 0 1 * * ?';
        System.schedule('my test', sch, b);
    }
}