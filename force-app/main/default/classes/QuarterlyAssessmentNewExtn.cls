/**
 * Created by banghart on 8/22/17.
 */

public with sharing class QuarterlyAssessmentNewExtn {
    public Quarterly_Assessment__c qa {get;set;}
    public String mymessage {get;set;}
    public Contact client {get;set;}
    public Quarterly_Assessment_Period__c Qap {get;set;}
    public QuarterlyAssessmentNewExtn(ApexPages.StandardController controller) {
        mymessage = 'here we are';
        qa = (Quarterly_Assessment__c)controller.getRecord();
        fetchUserDtls();
        //qa.Primary_Route__c = client.Primary_Route__c;
        fetchQuarterlyPeriod();
        qa.Period__c = Qap.Id;
    }
    public void fetchUserDtls() {
        Id cid;
        if(qa.Id!=null){
            cid = [select Client__c from Quarterly_Assessment__c where Id =:qa.Id limit 1].Client__c;
        } else {
            cid = qa.Client__c;
            system.debug('below is cid');
            system.debug(cid);
            system.debug('above is cid');
        }
        client = [SELECT Primary_Route__c, Primary_Route__r.Name FROM Contact WHERE Id = :cid];
    }
    public void fetchQuarterlyPeriod() {
        Qap = [SELECT Id, Name, Start_Date__c, End_Date__c, Active__c
        FROM Quarterly_Assessment_Period__c
        WHERE Active__c = true AND
        Start_Date__c < TODAY AND
        End_Date__c > TODAY
        LIMIT 1];
    }

}