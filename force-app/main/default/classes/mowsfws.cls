/**
 * Created by banghart on 1/25/18.
 */

@RestResource(urlMapping='/mowsfws/*')
global with sharing class mowsfws {
    @HttpDelete

    global static void doDelete() {
        system.debug('handle delete here');
    }

    @HttpGet
    global static List<SObject> doGet() {
        RestRequest req = RestContext.request;
        Map<String,String> reqParams = req.params;
        String action = reqParams.get('action');
        if (action == 'getDeliveries') {
            Integer yearInt = Integer.valueOf(reqParams.get('year'));
            Integer monthInt = Integer.valueOf(reqParams.get('month'));
            Integer dayInt = Integer.valueOf(reqParams.get('day'));
            String driverId = reqParams.get('driver');
            String routeId = reqParams.get('route');
            Date delDate = Date.newInstance(yearInt,monthInt,dayInt);
            return getDeliveries(delDate, routeId);
        } else if (action == 'getroutes') {
            Integer yearInt = Integer.valueOf(reqParams.get('year'));
            Integer monthInt = Integer.valueOf(reqParams.get('month'));
            Integer dayInt = Integer.valueOf(reqParams.get('day'));
            Date delDate = Date.newInstance(yearInt,monthInt,dayInt);
            system.debug('action is getroutes');
            return getRoutes(delDate);
        } else if (action == 'getdrivers') {
            system.debug('action is getdrivers');
            return getDrivers();
        }

        system.debug(req.params);
        system.debug(reqParams.get('q'));
        RestResponse res = RestContext.response;
        List<Delivery__c> dummy = new List<Delivery__c>();
        return dummy;
    }

    @HttpPost
    global static Void doPost() {
        system.debug('handle POST here');
        MAP<string, string> hdrs = RestContext.request.headers;
        String headers = '';
        for (string key : hdrs.keySet() ) {
            headers += key + ' : ' + hdrs.get(key) + '\n';
        }
        system.debug(headers);
        String jsonString = RestContext.request.requestBody.toString();
        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        system.debug('the user name value is: ' + m.get('username'));
        system.debug(RestContext.request.requestBody.toString());
        Map<String,String> reqParams = RestContext.request.params;
        String respToBlob = '';
        if (m.containsKey('action')) {
            system.debug('we have an action parameter');
            system.debug(getObjectType(m.get('action')));
            if (m.get('action') == 'updatedeliverystatus') {
                system.debug('we have action = updatedeliverystatus');
                system.debug(getObjectType(m.get('deliveryid')));
                String IdString = String.valueOf(m.get('deliveryid'));
                Id deliveryId = Id.valueOf(IdString);
                List<Delivery__c> del = [SELECT Id, Delivery_Status__c, Client__c FROM Delivery__c WHERE Id = :deliveryId];
                Boolean doUpdateDelivery = false;
                Boolean doUpdateClient = false;
                if (del.size() > 0) {
                    if (m.containsKey('status')) {
                        del[0].Delivery_Status__c = String.valueOf(m.get('status'));
                        doUpdateDelivery = true;
                    }
                    if (m.containsKey('affect')) {
                        String responseString = String.valueOf(m.get('affect'));
                        String tgMeal = '';
                        if (responseString == '10' || responseString == '11' || responseString == '12' || responseString == '13') {
                            // responses for Thursday survey
                            system.debug('handling thanksgiving here');
                            doUpdateClient = true;
                            Contact client = [SELECT Id, Thanksgiving_Meal__c FROM Contact WHERE Id = :del[0].Client__c];
                            if (responseString == '10') {
                                tgMeal = 'Yes';
                            } else if (responseString == '11') {
                                tgMeal = 'No';
                            } else if (responseString == '12') {
                                tgMeal = 'Yes with a guest';
                            } else {
                                tgMeal = 'Unable to answer';
                            }
                            client.Thanksgiving_Meal__c = tgMeal;
                            update client;
                        } else {
                            del[0].Affect__c = String.valueOf(m.get('affect'));
                            doUpdateDelivery = true;
                        }




                    }
                    if (m.containsKey('drivernote')) {
                        del[0].Driver_Note__c = String.valueOf(m.get('drivernote'));
                        doUpdateDelivery = true;
                    }
                    if (doUpdateDelivery == true) {
                        update del;
                    }
                    respToBlob = '{"response":"success"}';
                } else {
                    respToBlob = '{"response":"No Such Delivery ID"}';
                }
            } else if (m.get('action') == 'resequence') {
                system.debug('we have action = resequence');
                system.debug(getObjectType(m.get('deliveryid')));
                system.debug(getObjectType(m.get('sequence')));
                String IdString = String.valueOf(m.get('deliveryid'));
                Id deliveryId = Id.valueOf(IdString);
                Integer newSequence = Integer.valueOf(m.get('sequence'));
                List<Delivery__c> del = [SELECT Id, Delivery_Status__c, Route_Group_Del__c, Client__c FROM Delivery__c WHERE Id = :deliveryId];
                Id RouteId = del[0].Route_Group_Del__c;
                Id ClientId = del[0].Client__c;
                Route_Group_Member__c movedRGM;
                List<Route_Group_Member__c> rgms = [SELECT Id, Sequence__c, RG_Member_Contact__c FROM Route_Group_Member__c WHERE Route__c = :RouteId ORDER BY Sequence__c];
                Boolean foundClient = false;
                system.debug('list of member is this long: ' + rgms.size());
                Integer rgNum;
                for (Route_Group_Member__c rgm : rgms) {
                    if (rgm.RG_Member_Contact__c == ClientId) {
                        rgNum = rgms.indexOf(rgm);
                        system.debug('Found and removed route group member');
                    }
                    if (foundClient == true) {
                        break;
                    }
                }
                system.debug('found rgm at index of: ' + rgNum);
                system.debug('new sequence is ' + newSequence);
                movedRGM = rgms.remove(rgNum);
                rgms.add(newSequence,movedRGM);
                Integer counter = 1;
                for (Route_Group_Member__c rgm : rgms) {
                    rgm.Sequence__c = counter;
                    counter ++;
                }
                update rgms;
            }
        }
        Set<String> paramKeys = new Set<String>();
        paramKeys = reqParams.keySet();
        List<String> keyList  = new List<String>(paramKeys);
        for (String kString : keyList) {
            system.debug(kString + ' ' + reqParams.get(kString)  );
        }
        Map<String,String> respMap = new Map<String, String>();

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(respToBlob);
        //return RestContext.response.responseBody.toString();
    }
    private static List<Route__c> getRoutes(Date dateParam) {
        String dateString = dateParam.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        String fieldName = dayOfWeek + '__c';
        String qryString = 'SELECT Id, Name FROM Route__c WHERE ' + fieldName + ' = true ORDER BY Name';
        //List<Route__c> routes =[SELECT Id, Name
        //FROM Route__c
        //WHERE fieldName == true
        //ORDER BY Name];
        system.debug(qryString);
        List<Route__c> routes = Database.query(qryString);
        return routes;
    }
    private static List<Delivery__c> getDeliveries(Date dateParam, Id route_id) {
        List<Delivery__c> deliveries =[SELECT Id,Date__c,Client__r.Name,Client__r.Cross_Street__c,Client__r.Phone,Client__r.Diet__c,Client__r.Temperature__c,
                Route_Group_del__r.Name,Address__c,Route_Group_del__c,Route__r.LoadTime__c,Client__r.Client_Gerry_Notes__c,Client__r.Funding_Source__c,
                Quantity__c,Client__c,Meal_Scheduler__r.Mon__c,Meal_Scheduler__r.Tue__c,Meal_Scheduler__r.Wed__c,Meal_Scheduler__r.Thu__c,
                Meal_Scheduler__r.Fri__c,Meal_Scheduler__r.Sat__c,Meal_Scheduler__r.Sun__c, Sequence__c, Driver_1__c, List_Items__c,
                Client__r.Apartment__c, Route_Group_Del__r.Load_Time_txt__c
        FROM Delivery__c
        WHERE Date__c= :dateParam AND Route_Group_Del__c = :route_id
        ORDER BY Sequence__c];
        return deliveries;
    }
    private static List<Contact> getDrivers() {
        List<Contact> drivers = [SELECT Id, FirstName, LastName FROM Contact WHERE MOWSF_Staff__c = true ORDER BY LastName];
        return drivers;
    }
    private static String getObjectType(Object o) {
        String oType = 'Unknown';
        if (o instanceof String) {
            oType = 'String';
        }
        return oType;
    }
}