/**
 * Created by banghart on 8/1/18.
 */

@isTest(SeeAllData=false)
private class updateClientStatusTrigger_Test {

    static testMethod void testUpdateStatusTrigger(){
        createTestData();
        Date startDate = Date.today().addDays(-5);
        List<Contact> contacts = [SELECT Id, LastName FROM Contact];
        Meal_Suspension__c ms = New Meal_Suspension__c(Client__c = contacts[0].Id, Start_Date__c = startDate, Reason__c = 'Appt', Requestor__c = 'RB');
        insert ms;
        contacts = [SELECT Id, LastName, Status__c FROM Contact];
        System.assert(contacts[0].Status__c == 'Suspended no resume date');
        ms.End_Date__c = Date.today().addDays(1);
        update ms;
        contacts = [SELECT Id, LastName, Status__c FROM Contact];
        System.assert(contacts[0].Status__c == 'Suspended with resume date');
        ms.End_Date__c = Date.today();
        update ms;
        contacts = [SELECT Id, LastName, Status__c FROM Contact];
        System.assert(contacts[0].Status__c == 'Active');
        ms = New Meal_Suspension__c(Client__c = contacts[0].Id, Start_Date__c = startDate, Reason__c = 'Appt', Requestor__c = 'RB');
        Date testEndDate = Date.today().addDays(8);
        ms.End_Date__c = testEndDate;
        insert ms;
        contacts = [SELECT Id, LastName, Status__c FROM Contact];
        System.assert(contacts[0].Status__c == 'Suspended with resume date');
        List<Meal_Suspension__c> toDelete = [SELECT Id FROM Meal_Suspension__c WHERE End_Date__c = :testEndDate];
        delete toDelete;
        contacts = [SELECT Id, LastName, Status__c FROM Contact];
        System.assert(contacts[0].Status__c == 'Active');


    }
    public static Void createTestData() {
        Control_Settings__c cs = New Control_Settings__c(Name = 'CS-0001', Assessment_Cycle_Enable__c = true);
        insert cs;
        Contact c = New Contact(FirstName = 'First1', LastName = 'Last1');
        insert c;
    }
}