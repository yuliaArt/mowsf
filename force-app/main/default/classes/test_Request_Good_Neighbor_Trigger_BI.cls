@isTest
private class test_Request_Good_Neighbor_Trigger_BI {

	static testMethod void validate_Request_Good_Neighbor_Trigger_BI(){

		Contact c = new Contact();
		c.FirstName = 'John';
		c.LastName = 'Doe';
		RecordType rt = [Select ID, Name from RecordType where Name = 'Client Record' and SObjectType = 'Contact' limit 1];
		c.RecordTypeID = rt.ID;
		Insert c;

		//GW_Volunteers__Volunteer_Job__c vj = [Select ID, Name, API_Name__c from GW_Volunteers__Volunteer_Job__c where API_Name__c = 'GN_Friendly_Visitor'];
		
		Request_Good_Neighbor__c rgn = new Request_Good_Neighbor__c();
		rgn.Client__c = c.ID;
		rgn.Program__c = 'GN - Friendly Visitor';
		Insert rgn;

		}
}