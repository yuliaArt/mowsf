/*
    @Author - Sunil/Girikon
    @Date -  7/11/2017
    Test class to provide code coverage to "Save_Quarterly_Assessment" Trigger
*/
@isTest
public class Save_Quarterly_Assessment_Test {
    public static testMethod void testRunAs() {

        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User usr = createTestUser(pf.Id, 'Test User', 'Test User');
        Control_Settings__c cs = new Control_Settings__c(QA_Trigger_Enable__c = true);
        insert cs;
        System.runAs(usr)
        {
            Test.startTest();

            Route__c r = new Route__c(Generate_Route_Sheets__c=true, Name = 'test route');

            insert r;

            Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact;
            Map<String,Schema.RecordTypeInfo> ContactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
            Id rtId = ContactRecordTypeInfo.get('Client Record').getRecordTypeId();

            contact c = new contact(LastName='Test LastName',FirstName='Test FirstName',Temperature__c='Hot',Diet__c='Regular',Status__c = 'Active',RecordTypeId =rtId);
            insert c;

            Quarterly_Assessment_Period__c qp = new Quarterly_Assessment_Period__c(Name='QA-201701',End_Date__c=date.today(),Start_Date__c=date.today());
            insert qp;

            Quarterly_Assessment__c qa = new Quarterly_Assessment__c(
                    client__c = c.ID,
                    Follow_up__c = false,
                    Date__c= date.today(),
                    Period__c = qp.Id,
                    Conducted_by__c='Phone',
                    Eligible_to_Continue__c='Yes',
                    Reviewed_By__c=usr.Id,
                    comments__c='Test Comment',
                    Changes_in_home_situation__c='Declined',
                    Change_in_Physical_Condition__c='Declined',
                    Abuse_or_neglect__c='Declined'
            );
            insert qa;

            update qa;

            qa.Skip_Validation__c=true;
            update qa;

            qa.Skip_Validation__c=false;
            qa.Suspended__c=true;
            update qa;

            Quarterly_Assessment__c q = [Select Id,Name from Quarterly_Assessment__c where Id=:qa.ID];
            System.assertEquals(q.Id,qa.Id);
            Test.stopTest();
        }

    }
    public static User createTestUser(Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User
                (
                        firstname = fName,
                        lastName = lName,
                        email = uniqueName + '@test' + orgId + '.org',
                        Username = uniqueName + '@test' + orgId + '.org',
                        EmailEncodingKey = 'ISO-8859-1',
                        Alias = uniqueName.substring(18, 23),
                        TimeZoneSidKey = 'America/Los_Angeles',
                        LocaleSidKey = 'en_US',
                        LanguageLocaleKey = 'en_US',
                        ProfileId = profId
                );
        return tuser;
    }
}