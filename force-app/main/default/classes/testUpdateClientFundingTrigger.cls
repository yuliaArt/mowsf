/**
 * Created by banghart on 7/10/18.
 */
@isTest
public with sharing class testUpdateClientFundingTrigger {
    static testMethod void testUpdateClientFundingTrigger(){
        System.debug('Test for update client funding');
        Contact client = new Contact(FirstName = 'testclient', LastName = 'testclientlast', Funding_Source__c = 'Elderly Nutrition Program');
        insert client;
        Date startDate = Date.today();
        List<Contact> clients = [SELECT Id FROM Contact];
        Funding_Source_Information__c fs = new Funding_Source_Information__c(Contact__c = clients[0].Id,
                Start_Date__c = startDate, Funding_Source__c = 'SFTCP', MOW_Program__c = 'Senior Meal');
        insert fs;
        clients = [SELECT Funding_Source__c FROM Contact];
        system.assert(clients[0].Funding_Source__c == 'SFTCP');
    }
}