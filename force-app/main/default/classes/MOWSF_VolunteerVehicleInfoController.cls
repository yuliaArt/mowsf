public class MOWSF_VolunteerVehicleInfoController {
    
    public Contact contact {get; set;}
    public VolunteerVehicleInfo__c VolVehicleInfo {get; set;}
    public String saveMessage {get; set;}
    public String errorMessage {get; set;}
    public boolean hideShowFlag {get; set;}
    
    
    //Contructor
 	public MOWSF_VolunteerVehicleInfoController(ApexPages.StandardController stdcontroller) {
        try{
         contact = (Contact) stdcontroller.getRecord();
        VolVehicleInfo=[SELECT Insurance_Company__c,Insurance_Policy_Nbr__c,Insurance_Expiration_Date__c,
                        Vehicle_Plate_Number__c,Drivers_License_Number__c,Driver_s_License_Expiration_Date__c, Vehicle_Available__c, 
                        Name,Contact__c
                        FROM VolunteerVehicleInfo__c WHERE Contact__c =:contact.Id];
        
        }catch(Exception e){
		
			VolVehicleInfo=new VolunteerVehicleInfo__c();           
        }
      	saveMessage = '';
        errorMessage = '';
        hideShowFlag = true;
    }
    
    //Updates Vounteer Vehicle information	
     public void saveVolunteerVehicleInfo(){       
      // try {
       		VolVehicleInfo.Vehicle_Available__c=true;
         	VolVehicleInfo.Contact__c=contact.Id;
         
            upsert VolVehicleInfo;
            hideShowFlag = false;
            saveMessage = 'Thank you for updating your vehicle information.';
       //}catch(Exception e){
		//	errorMessage = 'Could not save Volunteer Vehicle info.';
       //}
	
    }
    
    //Test method for coverage.
    public void test_Only(){
                 Integer i = 0;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;

	}
    
}