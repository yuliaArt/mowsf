/**
 * Created by banghart on 4/9/18.
 */
@isTest
public with sharing class mowsfws_test {
    static testMethod void  testPostRestService() {
        createTestData();
        List<Delivery__c> testDelivery = [SELECT Id FROM Delivery__c];
        String deliveryIDString = String.valueOf(testDelivery[0].Id);
        RestRequest req = new RestRequest();
        RestResponse resp = new RestResponse();

        req.requestURI = '/services/apexrest/ws_route';
        Map<String,String> rParams = new Map<String,String>();
        req.params.put('action','getDeliveries');
        req.params.put('year','2018');
        req.params.put('month','05');
        req.params.put('day','01');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = resp;
        mowsfws.doGet();
        req.params.put('action','getroutes');
        req.httpMethod = 'GET';
        mowsfws.doGet();
        req.httpMethod = 'POST';
        //Blob body = JSON.deserialize('{"action":"updatedeliverystatus","deliveryid":"a1529000000e0HsAAI","status":"delivered","affect":"smile"}');
        Blob body = Blob.valueOf('{"action":"updatedeliverystatus","deliveryid":"' + deliveryIDString + '","status":"delivered","affect":"smile"}');
        req.requestBody = body;
        mowsfws.doPost();
        system.debug('did first update delivery');
        body = Blob.valueOf('{"action":"updatedeliverystatus","deliveryid":"' + deliveryIDString + '","status":"delivered","affect":"10"}');
        req.requestBody = body;
        req.httpMethod = 'POST';
        mowsfws.doPost();
        system.debug('did second update delivery');
        body = Blob.valueOf('{"action":"resequence","deliveryid":"' + deliveryIDString + '","status":"delivered","sequence":"1"}');
        req.requestBody = body;
        req.httpMethod = 'POST';
        mowsfws.doPost();
        system.debug('did resequence');
        req.httpMethod = 'DELETE';
        mowsfws.doDelete();
    }
    public static Void createTestData() {
        Date genDate = Date.today();
        String dateString = genDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        if (dayOfWeek == 'Sun' || dayOfWeek == 'Sat') {
            genDate = genDate.addDays(3);
        }
        List<Route__c> routes = new List<Route__c>();
        routes.add(new Route__c(Name = 'test', Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'test2',Route_Type__c = 'Frozen', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        insert routes;
        List<Contact> c = new List<Contact>();
        c.add(new Contact(FirstName = 'A', LastName = 'B', Diet__c = 'Regular', Temperature__c = 'Chilled', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'AA', LastName = 'BB', Diet__c = 'Regular', Temperature__c = 'Frozen' , Status__c = 'Active'));
        c.add(new Contact(FirstName = 'CC', LastName = 'CC', Diet__c = 'Regular', Temperature__c = 'Hot' , Status__c = 'Active'));
        insert c;
        c = [SELECT Id FROM Contact];
        List<Meal_Scheduler__c> mealSchedulers = new List<Meal_Scheduler__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        for (Contact client : c) {
            mealSchedulers.add(new Meal_Scheduler__c(Client__c = client.Id, Mon__c = true, Tue__c = true, Wed__c = true,
                    Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                    Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
            system.debug(client.Id + 'is a client');
        }
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[1].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[2].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[1].Id, Route__c = routes[1].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[2].Id, Route__c = routes[1].Id));
        List<Delivery__c> specialDeliveriesList = new List<Delivery__c>();
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[0].Id, Quantity__c=0, Special__c = true, Sequence__c = 1, Date__c = genDate, Client__c = c[1].Id));
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[1].Id, Quantity__c=0, Special__c = true, Sequence__c = 2, Date__c = genDate, Client__c = c[0].Id));
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[1].Id, Quantity__c=0, Special__c = true, Sequence__c = 3, Date__c = genDate.addDays(1), Client__c = c[0].Id));
        insert specialDeliveriesList;
        insert mealSchedulers;
        insert routeGroupMembers;
        routes = [SELECT Id FROM Route__c];
        // need a driver
        c = new List<Contact>();
        c.add(new Contact(FirstName = 'Dan', LastName = 'Driver', npe01__WorkEmail__c = 'myemail@me.com', MOWSF_Staff__c = true));
        insert c;
        System.debug('number of routes created was ' + routes.size());
    }

}