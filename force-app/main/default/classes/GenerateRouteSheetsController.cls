/**
 * Created by dsmith on 4/24/2017.
 */

public with sharing class GenerateRouteSheetsController {

    public Date delivery_date {get; set;}
    public String deliveryDateString {get;set;}
    public Date holiday_date {get;set;}
    public Date todayDate = Date.today();
    public String page_message {get; set;}
    public List<Route_Group_Deliveries> deliveries {get; set;}
    public List<String> OffRouteSpecial {get;set;}
    public PageReference pageRef = new PageReference('/apex/escapePrint');
    public String subsequentPageString {get;set;}
    public List<Route__c> Routes {get;set;}
    public String SelectedRoute {get;set;}
    public Boolean refreshdeliveries {get;set;}
    public Boolean combine_holiday {get;set;}
    public GenerateRouteSheetsController()
    {
        refreshdeliveries = true;
        deliveryDateString = todayDate.format();
        system.debug('the delivery date string is ' + deliveryDateString);
        page_message='';
    }
    public List<SelectOption> getRouteOptions() {
        List<SelectOption> RouteOptions = new List<SelectOption>();
        Routes = [SELECT Name, Id FROM Route__c ORDER BY Name];
        RouteOptions.add(new SelectOption('all','All Routes'));
        RouteOptions.add(new SelectOption('hdg', 'All HDG'));
        RouteOptions.add(new SelectOption('daily', 'All HDM (Hot, Chill)'));
        RouteOptions.add(new SelectOption('frozen', 'All HDM (Frozen)'));
        for (Route__c r :Routes) {
            RouteOptions.add(new SelectOption(r.Id, r.Name));
        }
        system.debug('number of options is: ' + RouteOptions.size());
        return RouteOptions;
    }
    public PageReference generateRouteSheets() {
        system.debug('route selected is: ' + SelectedRoute);
        delivery_date = Date.parse(deliveryDateString);
        //delivery_date = Date.newInstance(deliveryDateString);
        subsequentPageString = '<div style="clear:both;">&nbsp;</div>';
        PageReference displayReference = new PageReference('/apex/Generate_RouteSheets_Display');
        Generate_RouteSheets gr = new Generate_RouteSheets(Date.parse(deliveryDateString));   //constructor
        deliveries = gr.do_generation(SelectedRoute);
        if (deliveries.size() == 0) {
            page_message = 'No Route Sheets were generated. Check that you have generated deliveries for the date.';
            PageReference tryAgain = new PageReference('/apex/GenerateRouteSheets');
            return tryAgain;
        }
        system.debug('generate route sheets returned: ' + deliveries.size() + ' deliveries to controller');
        return displayReference;
    }
}