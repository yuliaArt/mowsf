/**
 * Created by banghart on 11/1/17.
 */

public with sharing class Route_Sheet_Delivery_Row {
    public String ClientString {get;set;}
    public String DateString {get;set;}
    public String AddressString {get;set;}
    public String SequenceString {get;set;}
    public String PhoneString {get;set;}
    public String CrossStreetString {get;set;}
    public String DietString {get;set;}
    public String MealTypeString {get;set;}
    public String ClientInfoString {get;set;}
    public String DriverInstructionString {get;set;}
    public String GenderString {get;set;}
    public String LanguageString {get;set;}
    public String RouteString {get;set;}
    public String DeliveryNotesString {get;set;}
    public String FundingSourceString {get;set;}
    public String Quantity {get;set;}
    public String Grid {get;set;}
    public String SpecialDeliveryString {get;set;}
    public Route_Sheet_Delivery_Row() {
        // RouteString = 'dummy value';
    }

}