public with sharing class GenerateDeliveriesController {

    //public ApexPages.StandardController standardController;

    public Date delivery_date {get;set;}
    public Integer numDays{get;set;}
    public Integer delaySeconds{get;set;}
    public String deliveryDateString {get;set;}
    public Integer numDelivered {get; set;}
    public String progressMessage {get;set;}
    public Integer percentComplete{get;set;}
    public Boolean combineFrozen {get;set;}
    public Boolean combineChill {get;set;}
    public Boolean combineHot {get;set;}
    public Boolean thanksgiving {get;set;}
    public Date combineDate {get;set;}
    public Id batchId{get;set;}
    //public BatchGenerateDeliveries gd{get;set;}

//    public GenerateDeliveriesController(ApexPages.StandardController standardController)
//    {
//        this.standardController = standardController;
//    }

    public GenerateDeliveriesController() {
        numDays = 1;
        system.debug('in the generate deliveries controller');
        percentComplete = 0;
        delaySeconds = 0;
    }
    public void checkTotal() {
        List<AsyncApexJob> a = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id,
                CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name
        From AsyncApexJob WHERE Id=:batchId];
        Double itemsProcessed;
        Double totalItems;
        String batchStatus;
        if (a.size() > 0) {
            batchStatus = a[0].Status;
            progressMessage = a[0].Status;
            itemsProcessed = a[0].JobItemsProcessed;
            totalItems = a[0].TotalJobItems;
            itemsProcessed = a[0].JobItemsProcessed;
            totalItems = a[0].TotalJobItems;
        } else {
            batchStatus = 'Nothing';
            itemsProcessed = 0;
            totalItems = 0;
            progressMessage = ' No batch found ';
        }
        system.debug('items processed is ' + itemsProcessed);
        system.debug('total items is ' + totalItems);
        if(batchStatus == 'Completed') {
            percentComplete = 100;
            //Determine the pecent complete based on the number of batches complete
        } else if (totalItems == 0) {
            //A little check here as we don't want to divide by 0.
            percentComplete = 0;
        } else {
            percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            system.debug('fraction is ' + (itemsProcessed/totalItems));
        }
        system.debug('percentage is ' + percentComplete);
        if (batchStatus != 'Completed') {
            delaySeconds = delaySeconds + 5;
        }

        //numDelivered = gd.totalUpserted;
    }
    public PageReference generateDeliveries() {
        system.debug('Calling class generator');
        system.debug(delivery_date);
        deliveryDateString = delivery_date.format();
        Datetime genDate = Datetime.newInstance(delivery_date.year(), delivery_date.month(), delivery_date.day());
        //Generate_Deliveries gd = new Generate_Deliveries();
        if (thanksgiving == false) {
            BatchGenerateDeliveries gd = new BatchGenerateDeliveries(delivery_date);
            gd.combineDate = combineDate;
            gd.combineChill = combineChill;
            gd.combineFrozen = combineFrozen;
            if (combineDate != null) {
                gd.combine_holiday = true;
            }
            List<Date> datesToGenerate = new List<Date>();
            datesToGenerate = gd.prepare_date_list(delivery_date, numDays);
            gd.gendate = datesToGenerate[0];
            batchId = Database.executeBatch(gd);
        } else {
            BatchThanksgiving gd = new BatchThanksgiving(delivery_date);
            batchId = Database.executeBatch(gd);
        }
        //numDelivered = gd.do_generation(datesToGenerate);
        return Page.GenerateDeliveriesPageConfirm;
    }
}