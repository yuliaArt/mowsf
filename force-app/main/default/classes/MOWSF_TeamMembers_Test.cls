@isTest
public class MOWSF_TeamMembers_Test {
  
    static testMethod void testGetTeamMembers() {
        //Account Add
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
        //Contact Add	 	
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
        //Campaign Add
        Campaign camp = new Campaign(Name = 'Test',IsActive = TRUE);            
    	insert camp;
        //Job Add
        GW_Volunteers__Volunteer_Job__c vJob=new GW_Volunteers__Volunteer_Job__c(Name='Test Job',GW_Volunteers__Campaign__c=camp.Id);
        insert vJob;
        //Shift Add
        DateTime dt = DateTime.now();
        GW_Volunteers__Volunteer_Shift__c vShift=new GW_Volunteers__Volunteer_Shift__c(GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Start_Date_Time__c=dt,GW_Volunteers__Duration__c=2);
        insert vShift;
        //Hours Add
        GW_Volunteers__Volunteer_Hours__c vHrs=new GW_Volunteers__Volunteer_Hours__c(GW_Volunteers__Contact__c=contactObj.Id,GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Status__c='Confirmed',GW_Volunteers__Number_of_Volunteers__c=1,GW_Volunteers__Start_Date__c=dt.date());
        insert vHrs;
	
        PageReference pageRef = Page.MOWSF_VolunteerVehicleInfo;
    	Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('contactId',contactObj.Id);
        ApexPages.currentPage().getParameters().put('hoursid',vHrs.Id);
	
        
        MOWSF_TeamMembersController mowsfTmc = new MOWSF_TeamMembersController(new ApexPages.StandardController(contactObj));

        List<TeamMembers__c> teamList = mowsfTmc.getTeamMembers(String.valueOf(contactObj.Id),String.valueOf(vHrs.Id));
        
        List<TeamMembers__c> listOfTM= [SELECT Contact__c,First_Name__c,Last_Name__c,Minor__c,Phone__c,Email__c,HoursId__c FROM
                TeamMembers__c WHERE  Contact__c=:contactObj.Id and HoursId__c=:vHrs.Id];
        
        System.assertEquals(listOfTM.size(), teamList.size());
    }

    static testMethod void testAddNewRow(){
        //Account Add
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
        //Contact Add	 	
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
        
        PageReference pageRef = Page.MOWSF_TeamMembers;
        String VolHoursID ='';
 		pageRef.getParameters().put('contactId', contactObj.Id);
        pageRef.getParameters().put('hoursid', VolHoursID);
        Test.setCurrentPage(pageRef);
        MOWSF_TeamMembersController mowsfTmc = new MOWSF_TeamMembersController(new ApexPages.StandardController(contactObj));
        mowsfTmc.addNewRow();
	
        //Test if Querystring matches
        System.assertEquals(mowsfTmc.hoursParamId, VolHoursID, 'ID\'s should match');

   	}
  	 static testMethod void testSaveAll(){
        //Account Add
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
        //Contact Add	 	
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
        //Campaign Add
        Campaign camp = new Campaign(Name = 'Test',IsActive = TRUE);            
    	insert camp;
        //Job Add
        GW_Volunteers__Volunteer_Job__c vJob=new GW_Volunteers__Volunteer_Job__c(Name='Test Job',GW_Volunteers__Campaign__c=camp.Id);
        insert vJob;
        //Shift Add
        DateTime dt = DateTime.now();
        GW_Volunteers__Volunteer_Shift__c vShift=new GW_Volunteers__Volunteer_Shift__c(GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Start_Date_Time__c=dt,GW_Volunteers__Duration__c=2);
        insert vShift;
        //Hours Add
        GW_Volunteers__Volunteer_Hours__c vHrs=new GW_Volunteers__Volunteer_Hours__c(GW_Volunteers__Contact__c=contactObj.Id,GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Status__c='Confirmed',GW_Volunteers__Number_of_Volunteers__c=1,GW_Volunteers__Start_Date__c=dt.date());
        insert vHrs;
 
        
        List<TeamMembers__c> listOfTM=new  List<TeamMembers__c>();
        TeamMembers__c tc=new TeamMembers__c(First_Name__c='Dev1',Last_Name__c='Test1',Minor__c=true,Phone__c='3216549877',Email__c='devtest@gmail.com',Contact__c=String.valueOf(contactObj.Id),HoursId__c=String.valueOf(vHrs.Id));
        TeamMembers__c tc1=new TeamMembers__c(First_Name__c='Dev2',Last_Name__c='Test2',Minor__c=false,Phone__c='3216549871',Email__c='devtest1@gmail.com',Contact__c=String.valueOf(contactObj.Id),HoursId__c=String.valueOf(vHrs.Id));
        listOfTM.add(tc);
        listOfTM.add(tc1);
        
        MOWSF_TeamMembersController mowsfTmc = new MOWSF_TeamMembersController(new ApexPages.StandardController(contactObj));
        mowsfTmc.listTeamMembers=listOfTM;
        mowsfTmc.saveAll();
        
        //Test that Records are Inserted
       
        system.assertequals('Team Member information saved successfully.',mowsfTmc.saveMessage);	
   	}
    static testMethod void testDelete(){
        //Account Add
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
        //Contact Add	 	
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
        //Campaign Add
        Campaign camp = new Campaign(Name = 'Test',IsActive = TRUE);            
    	insert camp;
        //Job Add
        GW_Volunteers__Volunteer_Job__c vJob=new GW_Volunteers__Volunteer_Job__c(Name='Test Job',GW_Volunteers__Campaign__c=camp.Id);
        insert vJob;
        //Shift Add
        DateTime dt = DateTime.now();
        GW_Volunteers__Volunteer_Shift__c vShift=new GW_Volunteers__Volunteer_Shift__c(GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Start_Date_Time__c=dt,GW_Volunteers__Duration__c=2);
        insert vShift;
        //Hours Add
        GW_Volunteers__Volunteer_Hours__c vHrs=new GW_Volunteers__Volunteer_Hours__c(GW_Volunteers__Contact__c=contactObj.Id,GW_Volunteers__Volunteer_Job__c=vJob.Id,GW_Volunteers__Status__c='Confirmed',GW_Volunteers__Number_of_Volunteers__c=1,GW_Volunteers__Start_Date__c=dt.date());
        insert vHrs;
  
        
        List<TeamMembers__c> listOfTM=new  List<TeamMembers__c>();
        TeamMembers__c tc=new TeamMembers__c(First_Name__c='Dev1',Last_Name__c='Test1',Minor__c=true,Phone__c='3216549877',Email__c='devtest@gmail.com',Contact__c=String.valueOf(contactObj.Id),HoursId__c=String.valueOf(vHrs.Id));
        TeamMembers__c tc1=new TeamMembers__c(First_Name__c='Dev2',Last_Name__c='Test2',Minor__c=false,Phone__c='3216549871',Email__c='devtest1@gmail.com',Contact__c=String.valueOf(contactObj.Id),HoursId__c=String.valueOf(vHrs.Id));
        listOfTM.add(tc);
        listOfTM.add(tc1);
        insert listOfTM; 
        
        MOWSF_TeamMembersController mowsfTmc = new MOWSF_TeamMembersController(new ApexPages.StandardController(contactObj));  
        mowsfTmc.listTeamMembers=listOfTM;
 		Integer index = 0;
        
        for(TeamMembers__c tcObj:mowsfTmc.listTeamMembers)
        {
            ApexPages.currentPage().getParameters().put('rowNumId',String.valueOf(index));
            index++;
        }
        mowsfTmc.rowId=0;
        mowsfTmc.deleteRow();
       	
        //Test that Records are Deleted
         list<TeamMembers__c> obj2 = mowsfTmc.getTeamMembers(String.valueOf(contactObj.Id),String.valueOf(vHrs.Id));
         system.assertEquals(obj2.size(),1);
   	}
    
    static testMethod void runTest() {
        Account testAccount = new Account();
		testAccount.Name='Test Account' ;
		insert testAccount;
		
		Contact contactObj = new Contact();
		contactObj.FirstName='Test';
		contactObj.LastName='Test';
		contactObj.Accountid= testAccount.id;
		insert contactObj;
        
        MOWSF_TeamMembersController c = new MOWSF_TeamMembersController(new ApexPages.StandardController(contactObj));
        c.test_Only();
      }
}