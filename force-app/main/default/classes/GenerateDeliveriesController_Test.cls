/**
 * Created by banghart on 3/2/18.
 */

@IsTest(SeeAllData=false)
public with sharing class GenerateDeliveriesController_Test {
    public static testMethod void testRunAs() {
        // Instantiate a new controller with all parameters in the page
        GenerateDeliveriesController DelController = new GenerateDeliveriesController();
        DelController.numDays = 2;
        DelController.delivery_date = date.today();
        DelController.combineChill = false;
        DelController.combineFrozen = false;
        DelController.thanksgiving = false;
        test.startTest();
        DelController.generateDeliveries();
        test.stopTest();
        DelController.checkTotal();
        DelController.thanksgiving = true;
        DelController.generateDeliveries();
        system.debug('in the DelController test ');
        //System.assertEquals(controller.login(),null);
    }
}