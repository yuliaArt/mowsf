public class VolunteerCheckInStatusCont {

    public List<GW_Volunteers__Volunteer_Hours__c> displayVolunteers {get;set;}
    public List<Account> displayCheckinS {get;set;}
    public GW_Volunteers__Volunteer_Job__c filterProgram { get; set; }    
    public GW_Volunteers__Volunteer_Hours__c filterVolunteer {get;set;}

    public VolunteerCheckInStatusCont () {

         displayVolunteers = new List<GW_Volunteers__Volunteer_Hours__c>();
         displayCheckinS = new List<Account>();
         filterProgram = new GW_Volunteers__Volunteer_Job__c();
         filterVolunteer = new GW_Volunteers__Volunteer_Hours__c();

    }

    public void filter(){
        
        String vProg = filterProgram.GW_Volunteers__Campaign__c;
        String cProg = vProg.left(15);
        //system.debug('campaign='+vProg+', cProg='+cProg);
        String vJob = '';
        String vShift = '';
        
        Account selectCheckinS = new Account();
                        
        displayVolunteers = [Select GW_Volunteers__Volunteer_Job__r.Name, 
                             FORMAT(GW_Volunteers__Shift_Start_Date_Time__c),
                             Name, GW_Volunteers__Full_Name__c, 
                             GW_Volunteers__Contact__r.Email, GW_Volunteers__Contact__r.Primary_Phone__c,
                             GW_Volunteers__Status__c
            From GW_Volunteers__Volunteer_Hours__c 
            Where GW_Volunteers__Volunteer_Campaign__c = :cProg
              And GW_Volunteers__Volunteer_Job__c = :filterVolunteer.GW_Volunteers__Volunteer_Job__c
              And GW_Volunteers__Status__c In ('Confirmed', 'Completed')
              And GW_Volunteers__Start_Date__c >= :filterVolunteer.GW_Volunteers__Start_Date__c               
              And GW_Volunteers__Start_Date__c <= :filterVolunteer.GW_Volunteers__End_Date__c 
         Order By GW_Volunteers__Volunteer_Job__r.Name, GW_Volunteers__Shift_Start_Date_Time__c,
                  GW_Volunteers__Full_Name__c, GW_Volunteers__Status__c ];
         
         displayCheckinS.clear();  
         
         for(GW_Volunteers__Volunteer_Hours__c cs : displayVolunteers){
           if(cs.GW_Volunteers__Shift_Start_Date_Time__c == null){
           } else {
           if(cs.GW_Volunteers__Volunteer_Job__r.Name != vJob || 
              cs.GW_Volunteers__Shift_Start_Date_Time__c.format('M/dd/yyyy h:mm a') != vShift){
                 if(vJob != ''){    
                    displayCheckinS.add (selectCheckinS);
                 }
                 
                 selectCheckinS = new Account(); 
                 vJob = cs.GW_Volunteers__Volunteer_Job__r.Name; 
                 vShift = cs.GW_Volunteers__Shift_Start_Date_Time__c.format('M/dd/yyyy h:mm a');
    
                 selectCheckinS.Name = cs.GW_Volunteers__Shift_Start_Date_Time__c.format('M/dd/yyyy h:mm a');
                 selectCheckinS.npo02__LastMembershipLevel__c  = cs.GW_Volunteers__Volunteer_Job__r.Name;
                 selectCheckinS.npo02__LastMembershipOrigin__c = cs.GW_Volunteers__Shift_Start_Date_Time__c.format('M/dd/yyyy h:mm a');
                 selectCheckinS.npo02__OppsClosedLastNDays__c  = 0;
                 selectCheckinS.npo02__OppsClosedLastYear__c   = 0;     
                 selectCheckinS.npo02__OppsClosedThisYear__c   = 0;         
                 selectCheckinS.npsp__Matching_Gift_Percent__c = 0;
              }
           if(cs.GW_Volunteers__Status__c == 'Confirmed'){
                selectCheckinS.npo02__OppsClosedLastNDays__c = selectCheckinS.npo02__OppsClosedLastNDays__c+1;    
                } else { // Completed
                selectCheckinS.npo02__OppsClosedLastYear__c = selectCheckinS.npo02__OppsClosedLastYear__c+1;              
                }              
           selectCheckinS.npo02__OppsClosedThisYear__c = 
               selectCheckinS.npo02__OppsClosedLastNDays__c;
           selectCheckinS.npsp__Matching_Gift_Percent__c = 
               100 * selectCheckinS.npo02__OppsClosedLastNDays__c /
               (selectCheckinS.npo02__OppsClosedLastNDays__c + 
               selectCheckinS.npo02__OppsClosedLastYear__c);
           }    
         } //for loop on displayVolunteers
         
         if(selectCheckinS.npo02__LastMembershipLevel__c != ''){    
           displayCheckinS.add (selectCheckinS);
         } //add the last row  
        
    } // filter
    }