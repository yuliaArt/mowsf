/**
 * Created by banghart on 11/5/17.
 */

public with sharing class Route_Group_Deliveries {
    public List<Route_Sheet_Delivery_Row> deliveryList {get;
        set{
            deliveryList = value;
            //generateFooter();
            System.debug('in the setter for Route_Group_Deliveries');
        }
    }
    public Boolean isNotFirst {get;set;}
    public Decimal SumHotRg {get;set;}
    public Decimal SumHotLs {get;set;}
    public Decimal SumHotDb {get;set;}
    public Decimal SumHotMs {get;set;}
    public Decimal SumChillRg {get;set;}
    public Decimal SumChillLs {get;set;}
    public Decimal SumChillDb {get;set;}
    public Decimal SumChillMs {get;set;}
    public Decimal SumChillRg_alt {get;set;}
    public Decimal SumChillLs_alt {get;set;}
    public Decimal SumChillDb_alt {get;set;}
    public Decimal SumChillMs_alt {get;set;}
    public Decimal SumFrozenRg {get;set;}
    public Decimal SumFrozenLs {get;set;}
    public Decimal SumFrozenDb {get;set;}
    public Decimal SumFrozenMs {get;set;}
    public Decimal SumFrozenRg_alt {get;set;}
    public Decimal SumFrozenLs_alt {get;set;}
    public Decimal SumFrozenDb_alt {get;set;}
    public Decimal SumFrozenMs_alt {get;set;}
    public Decimal HotToChillClients {get;set;}
    public Integer ClientCount {get;set;}
    public Map<String, Map<String,Integer>> summaryTable {get;set;}
    public List<Meal_Suspension__c> suspensionList {get;set;}
    public String RouteGroupString {get;set;}
    public String DateString {get;set;}
    public String LoadTimeString {get;set;}
    public String SummaryString {get;set;}
    public Boolean combineHoliday {get;set;}
    public Date holiday_date {get;set;}
    public Boolean displayAltSummary {get;set;}
    public List<String> OffRouteSpecial {get;set;}
    public String AltDateString {get;set;}
    public Boolean specialDeliveryFooter {get;set;}
    public String dayOfWeek{get;set;}

    public Route_Group_Deliveries() {
        // RouteString = 'dummy value';
        combineHoliday = false; // default to false
        suspensionList = new List<Meal_Suspension__c>();
        OffRouteSpecial = new List<String>();
        specialDeliveryFooter = false;
        HotToChillClients = 0;
    }
    public Void generateOffRouteSpecialDelivery(Delivery__c d){
        String specialDeliveryString = '';
        specialDeliveryString += '<div style="font-family: sans-serif; text-align: center">';
        specialDeliveryString += '<h2 style="font-family: sans-serif; text-align: center">Meals on Wheels Delivery Reciept</h2>';
        specialDeliveryString += '<h3>Route: ' + d.Route_Group_Del__r.Name + '</h3>';
        specialDeliveryString += '    I acknolwedge that I have received:<br/>';
        specialDeliveryString += d.List_Items__c + '<br />';
        specialDeliveryString += '    from my delivery driver.';
        specialDeliveryString += '    </div>';
        specialDeliveryString += ' <div style="text-align: left">';
        specialDeliveryString += '<br/><br/><br/><br/><br/>_________________________________________<br/>';
        specialDeliveryString += 'Signature';
        specialDeliveryString += '</div>';
        specialDeliveryString += d.Client__r.LastName + ', ' + d.Client__r.FirstName + '<br />';
        specialDeliveryString += d.Client__r.MailingStreet + 'Apt. ' + d.Client__r.Apartment__c + '<br/>';
        specialDeliveryString += '<br/>';
        specialDeliveryString += '<br /><br />_________________________________________<br/>Date' ;
        OffRouteSpecial.add(specialDeliveryString);
        specialDeliveryFooter = true;
    }
    public Void GenerateSummary() {
        // Column headers to be Diet, Total, Pack
        SummaryString = '';
        Decimal altChill = 0;
        System.debug('generating summary with: ' + deliveryList.size() + ' deliveries');
        system.debug('chill rg alt entering at Summary ' + SumChillRg_alt);

        Decimal totalHot = SumHotRg + SumHotLs + SumHotDb + SumHotMs;
        Decimal totalChill = SumChillRg + SumChillMs + SumChillDb + SumChillLs + SumChillRg_alt + SumChillMs_alt + SumChillDb_alt + SumChillLs_alt;
        Decimal totalFrozen = SumFrozenRg + SumFrozenMs + SumFrozenDb + SumFrozenLs + SumFrozenRg_alt + SumFrozenMs_alt + SumFrozenDb_alt + SumFrozenLs_alt;
        SummaryString += '<table border="1">';
        SummaryString += '<tr><th>Diet</th><th>' + DateString + ' Pack</th></tr>';
        SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumHotRg.toPlainString() + '</tr>';
        SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumHotLs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td>Diabetic</td><td style="text-align:center">' + SumHotDb.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumHotMs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td><strong>Total Meals (Hot)</strong></td><td style="text-align:center">' + totalHot.toPlainString() + '</td></tr>';
        SummaryString += '</table>';
        SummaryString += '<table border="1">';
        SummaryString += '<tr><th>Diet</th><th>' + DateString + ' Pack</th>';
        System.debug('Day of week is ' + dayOfWeek);
        if (dayOfWeek == 'Wed' || dayOfWeek == 'Fri' || combineHoliday == true) {
            if (dayOfWeek == 'Fri') {
                AltDateString = 'Weekend ';
            }
            displayAltSummary = true;
            System.debug('Adding to summary string ' + AltDateString );
            SummaryString += '<th>' + AltDateString + ' Pack</th>';
        }
        SummaryString += '</tr>';
        SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumChillRg.toPlainString() + '</td>';
        if (displayAltSummary == true) {
            SummaryString += '<td style="text-align:center">' + SumChillRg_alt + '</td>';
        }
        SummaryString += '</tr>';
        SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumChillLs.toPlainString() + '</td>';
        if (displayAltSummary == true) {
            SummaryString += '<td style="text-align:center">' + SumChillLs_alt + '</td>';
        }
        SummaryString += '</tr>';
        SummaryString += '<tr><td>Diabetic</td><td style="text-align:center">' + SumChillDb.toPlainString() + '</td>';
        if (displayAltSummary == true) {
            SummaryString += '<td style="text-align:center">' + SumChillDb_alt + '</td>';
        }
        SummaryString += '</tr>';
        SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumChillMs.toPlainString() + '</td>';
        if (displayAltSummary == true) {
            SummaryString += '<td style="text-align:center">' + SumChillMs_alt + '</td>';
        }
        SummaryString += '</tr>';
        SummaryString += '<tr><td><strong>Total Meals (Chill)</strong></td><td style="text-align:center">' + totalChill.toPlainString() + '</td>';
        if (displayAltSummary == true) {
            SummaryString += '<td style="text-align:center">' + (SumChillRg_alt + SumChillLs_alt + SumChillDb_alt + SumChillMs_alt) + '</td>';
        }
        SummaryString += '</tr>';
        SummaryString += '</table><br />';
        SummaryString += '<b>Hot Clients Receiving Chill: ' + HotToChillClients.toPlainString() + '</b><br /><br />';
        if (SumFrozenRg != 0 || SumFrozenLs != 0 || SumFrozenDb != 0 || SumFrozenMs != 0) {


            SummaryString += '<table border="1">';
            SummaryString += '<tr><th>Diet</th><th>Pack</th></tr>';
            SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumFrozenRg.toPlainString() + '</td></tr>';
            SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumFrozenLs.toPlainString() + '</td></tr>';
            SummaryString += '<tr><td>Diabetic</td><td style="text-align:center">' + SumFrozenDb.toPlainString() + '</td></tr>';
            SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumFrozenMs.toPlainString() + '</td></tr>';
            SummaryString += '<tr><td><strong>Total Meals (Frozen)</strong></td><td style="text-align:center">' + totalFrozen.toPlainString() + '</td></tr>';
            SummaryString += '</table><br />';
        }
        SummaryString += '<div >Total Clients: ' + ClientCount + '</div>';

        SummaryString += '<div style="text-align:center"><strong>Suspended Clients</strong></div><br />';
        SummaryString += '<table border="1"><tr><th>Name</th><th>Start Hold</th><th>Reason</th></tr>';

        For (Meal_Suspension__c ms : suspensionList) {
            SummaryString += '<tr><td>' + ms.Client__r.LastName + ', ' + ms.Client__r.FirstName + '</td>';
            SummaryString += '<td>' + ms.Start_Date__c.month() + '/' + ms.Start_Date__c.day() + '/' + ms.Start_Date__c.year() + '</td>' + '<td>' + ms.Reason__c + '</td></tr>';
        }
        SummaryString += '</table>';
    }
    public Void oldGenerateSummary() {
        SummaryString = '';
        for (Route_Sheet_Delivery_Row dr : deliveryList){

        }
        Decimal totalHot = SumHotRg + SumHotLs + SumHotDb + SumHotMs;
        Decimal totalChill = SumChillRg + SumChillMs + SumChillDb + SumChillLs;
        Decimal totalFrozen = SumFrozenRg + SumFrozenMs + SumFrozenDb + SumFrozenLs;
        SummaryString += '<table border-style="solid" border-width="1px" border-color="black">';
        SummaryString += '<tr><th>Diet</th><th>Pack</th></tr>';
        SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumHotRg.toPlainString() + '</tr>';
        SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumHotLs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumHotMs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td><strong>Total Meals (Hot)</strong></td><td style="text-align:center">' + totalHot.toPlainString() + '</td></tr>';
        SummaryString += '</table>';
        SummaryString += '<table>';
        SummaryString += '<tr><th>Diet</th><th>Pack</th></tr>';
        SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumChillRg.toPlainString() + '</tr>';
        SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumChillLs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumChillMs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td><strong>Total Meals (Chill)</strong></td><td style="text-align:center">' + totalChill.toPlainString() + '</td></tr>';
        SummaryString += '</table><br />';
        SummaryString += '<table>';
        SummaryString += '<b>Hot Clients Receiving Chill: ' + HotToChillClients.toPlainString() + '</b><br>';
        SummaryString += '<tr><th>Diet</th><th>Pack</th></tr>';
        SummaryString += '<tr><td>Regular</td><td style="text-align:center">' + SumFrozenRg.toPlainString() + '</tr>';
        SummaryString += '<tr><td>Low Sodium</td><td style="text-align:center">' + SumFrozenLs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td>Mech/Soft</td><td style="text-align:center">' + SumFrozenMs.toPlainString() + '</td></tr>';
        SummaryString += '<tr><td><strong>Total Meals (Frozen)</strong></td><td style="text-align:center">' + totalFrozen.toPlainString() + '</td></tr>';
        SummaryString += '</table><br />';
        SummaryString += '<div >Total Clients: ' + ClientCount + '</div>';
        SummaryString += '<div style="text-align:center"><strong>Suspended Clients</strong></div><br />';
        SummaryString += '<table><tr><th>Name</th><th>Start Hold</th><th>Reason</th></tr>';

        For (Meal_Suspension__c ms : suspensionList) {
            SummaryString += '<tr><td>' + ms.Client__r.LastName + ', ' + ms.Client__r.FirstName + '</td>';
            SummaryString += '<td>' + ms.Start_Date__c.month() + '/' + ms.Start_Date__c.day() + '/' + ms.Start_Date__c.year() + '</td>' + '<td>' + ms.Reason__c + '</td></tr>';
        }
        SummaryString += '</table>';
    }
}