/**
 * Created by banghart on 11/6/17.
 */

@IsTest(SeeAllData=false)
private class test_quarterly_assessment_controller {
    static testMethod void testBehavior() {
        Route__c route = new Route__c(Name = 'CENT 1');
        Contact testContact = new Contact(Firstname='r',Lastname = 'b',Primary_Route__c = route.Id);
        insert testContact;
        system.debug('in the test method');
        Date startDate = date.newInstance(2017,3,12);
        Date endDate = date.newInstance(2020,3,12);
        Quarterly_Assessment_Period__c qap = new Quarterly_Assessment_Period__c(Start_Date__c = startDate,End_Date__c=endDate, Active__c = true);
        insert qap;
        system.debug('*****   ***** ***** assigning client id of ' + testContact.Id + ' to quarterly assessment');
        Quarterly_Assessment__c qa = new Quarterly_Assessment__c(Period__c = qap.Id,Client__c = testContact.Id);

        // Contact cId = [SELECT Id FROM Contact WHERE RecordType.Name = 'Client Record' AND Primary_Route__r.Name = 'CENT 1' LIMIT 1];
        //List<Route__c> qas = [SELECT Id FROM Route__c LIMIT 1];
        //qa.Primary_Route__c = qas[0].Id;
        qa.Client__c = testContact.Id;
        //call the controller
        ApexPages.StandardController stdQa = new ApexPages.StandardController(qa);
        QuarterlyAssessmentNewExtn controller = new QuarterlyAssessmentNewExtn(stdQa);
        ApexPages.currentPage().getParameters().put('ID', qa.ID);
        system.debug(qa.Client__c);
        system.debug('above is client id');
        system.debug(controller.mymessage);
        //Id route_id = controller.qa.Primary_Route__c;
        //List<Route__c> route = [SELECT Name FROM Route__c WHERE Id = :route_id LIMIT 1];
        //system.debug(controller.qa.Primary_Route__c);
        //System.AssertEquals(route[0].Name,'CENT 1');

    }
}