public class MOWSF_TeamMembersController {

    public Contact contact {get; set;}
    public GW_Volunteers__Volunteer_Hours__c vHours {get; set;}
    public List<GW_Volunteers__Volunteer_Hours__c> lVhs {get; set;}
    public List<TeamMembers__c> listTeamMembers {get; set;}
    public String saveMessage {get; set;}
    public Integer rowId {get; set;}
    public String strProgramName {get; set;}
    public String strJobName {get; set;}
    public String strShift {get; set;}
    public String strDateTime {get; set;}
    public TeamMembers__c tm {get; set;}
    public String contactParamId{get; set;}
    public String hoursParamId{get; set;}
	public boolean hideShowFlag {get; set;}
    
	//Constructor   
 	public MOWSF_TeamMembersController(ApexPages.StandardController stdcontroller) {
        hideShowFlag=false;
        contactParamId = apexpages.currentpage().getparameters().get('contactId');
        hoursParamId = apexpages.currentpage().getparameters().get('hoursid');
        if(contactParamId!=null && hoursParamId!=null)
        	getProgramDetails(hoursParamId);
        saveMessage = '';
		
    }
    
    // Display Voluteer Job & Shift Details based on the hrs id
    public void getProgramDetails(String hrsId)
    {
       List<GW_Volunteers__Volunteer_Hours__c> vHourslist=[SELECT Id,GW_Volunteers__Volunteer_Shift__r.Name,GW_Volunteers__Volunteer_Job__r.Name,GW_Volunteers__Volunteer_Shift__r.GW_Volunteers__Start_Date_Time__c 
  				FROM GW_Volunteers__Volunteer_Hours__c WHERE Name=:hrsId];
        if(vHourslist.size()==0)
        {	 vHours=null;}
        else
        {    vHours=vHourslist[0];}
            
		if (vHours != null) 
        {	listTeamMembers=getTeamMembers(contactParamId,vHours.Id);
        	hideShowFlag=true;
        }	
     }
    
    
    //To Fetch member information using the contactid
    public List<TeamMembers__c> getTeamMembers(String contactId,String hrsId){

        	List<TeamMembers__c> listTM=[SELECT Contact__c,First_Name__c,Last_Name__c,Minor__c,Phone__c,Email__c,HoursId__c FROM
                TeamMembers__c WHERE Contact__c=:contactId and HoursId__c=:hrsId];
               return listTM;    
               
    }
    
    //Add new Team Member Record 
    public void addNewRow(){
		saveMessage = '';
        lVhs=[SELECT Id FROM GW_Volunteers__Volunteer_Hours__c WHERE Name=:hoursParamId];
        if(lVhs.size()>0){
            TeamMembers__c tm=new TeamMembers__c();
            tm.Contact__c=contactParamId;
            tm.HoursId__c=lVhs[0].Id; //Here cannot use hoursParamId as it is the Name Field and Here we need to pass the Id Field
            listTeamMembers.add(tm);
        }
    }
    
    //Save updated List of Team Members 
    public void saveAll(){	
        try {
            
            upsert listTeamMembers;
            If (!listTeamMembers.isEmpty())
			saveMessage = 'Team Member information saved successfully.';
       	}catch(Exception e){
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,'Failed to save Team Member information.' );
        	apexpages.addmessage(msg);
        }
    }
    //Delete Team Member record
    public void deleteRow(){
		try {
			saveMessage = '';
            rowId=Integer.valueOf(apexpages.currentPage().getparameters().get('rowNumId'));
        	TeamMembers__c ObjTM=listTeamMembers.remove(rowId);
        	if(String.isNotBlank(ObjTM.Id)) delete objTM;
         }catch(Exception e){
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,'Failed to delete Team Member information.' );
        	apexpages.addmessage(msg);
         }
    }
    
     //Test method for coverage.
    public void test_Only(){
                 Integer i = 0;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;

	}
    
}