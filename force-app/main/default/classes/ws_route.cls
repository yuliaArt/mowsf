/**
 * Created by banghart on 2/8/18.
 */


@RestResource(urlMapping='/ws_route/*')
global with sharing class ws_route {
    @HttpDelete

    global static void doDelete() {
        system.debug('handle delete here');
    }

    @HttpGet
    global static List<Route__c> doGet() {
        system.debug('handle GET here');
        RestRequest req = RestContext.request;
        system.debug('**********');
        Map<String,String> reqParams = req.params;
        String action = reqParams.get('action');
        if (action == 'getroutes') {
            List<Route__c> routes = getRoutes();
            return routes;
        }

        system.debug(req.params);
        system.debug(reqParams.get('q'));
        List<Route__c> dummy = new List<Route__c>();
        return dummy;
    }

    @HttpPost
    global static String doPost() {
        return 'done';
    }
    private static List<Route__c> getRoutes() {
        List<Route__c> routes =[SELECT Id, Name
        FROM Route__c
        ORDER BY Name];
        return routes;
    }
}