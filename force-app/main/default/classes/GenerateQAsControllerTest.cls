/**
 * Created by banghart on 11/7/17.
 */
/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class GenerateQAsControllerTest {
    public static testMethod void testRunAs() {
        Quarterly_Assessment_Period__c QAP = new Quarterly_Assessment_Period__c();
        // Instantiate a new controller with all parameters in the page
        GenerateQuarterlyAssessmentsController QAcontroller = new GenerateQuarterlyAssessmentsController(new ApexPages.StandardController(QAP));
        QAcontroller.generateAssessments();
        QAcontroller.deleteAssessments();
        system.debug('in the gen qa test ');
        //System.assertEquals(controller.login(),null);
    }
}