/*
	@Author - Sunil/Girikon
	@Date -  6/16/2017
	Test class to provide code coverage to "AssessmentControllerExtn" controller
*/
@isTest
public class AssessmentControllerExtn_Test {
    public static testMethod void testRunAs() {
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User usr = createTestUser(pf.Id, 'Test User', 'Test User');
        Assessment__c newAssessment =new Assessment__c();
        Assessment__c newAssessment2 =new Assessment__c();
        System.runAs(usr)
        {
            //Route__c newRoute = new Route__c();
            //newRoute.Status__c = 'Active';
            //newRoute.Generate_Assessments__c=true;
            //newRoute.Generate_Route_Sheets__c=true;
            //newRoute.Social_Worker__c=usr.id;
            //insert newRoute;
        
            Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact; 
            Map<String,Schema.RecordTypeInfo> ContactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        	Id rtId = ContactRecordTypeInfo.get('Client Record').getRecordTypeId();
            Control_Settings__c cs = new Control_Settings__c();
            insert cs;
            contact c = new contact(LastName='Test LastName',FirstName='Test FirstName',Temperature__c='Hot',Diet__c='Regular',Status__c = 'Active',RecordTypeId =rtId);
            insert c;
        
        	//Delivery_Days__c newDay = new Delivery_Days__c();
            //newDay.Day_Number__c=1;
            //newDay.Day_of_Week__c='Monday';
            //newDay.Frequency__c='Every';
            //newDay.Name='Mon-Every';
            //insert newDay;
        
        	//Delivery_Rule__c newRule = new Delivery_Rule__c();
            //newRule.Client__c=c.Id;
            //newRule.Day__c=newDay.Id;
            //newRule.Diet__c='Regular';
            //newRule.Route__c=newRoute.Id;
        	//newRule.Temperature__c='Hot';
        	//newRule.Special__c=true;
        	//newRule.Start_Date__c=date.today();
        	//newRule.Quantity__c=2;
            //insert newRule;
        
            newAssessment.Client__c=c.Id;
            insert  newAssessment;
            
            newAssessment2.Client__c=c.Id;
            newAssessment2.Complete__c = true;
        }
         
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', newAssessment.Id);
        AssessmentControllerExtn objAssessmentCtrlExtn = new AssessmentControllerExtn(new ApexPages.StandardController(newAssessment));
        objAssessmentCtrlExtn.populateUserDtls();
        objAssessmentCtrlExtn.save();
        objAssessmentCtrlExtn.customDelete();
		
        AssessmentControllerExtn objAssessmentCtrlExtn2 = new AssessmentControllerExtn(new ApexPages.StandardController(newAssessment2));
        objAssessmentCtrlExtn2.save();

        Test.stopTest();
    }
    
    
    public static User createTestUser(Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User
                (
                        firstname = fName,
                        lastname = lName,
                        Username = uniqueName + '@test' + orgId + '.org',
                        email = uniqueName + '@test' + orgId + '.org',
                        EmailEncodingKey = 'ISO-8859-1',
                        Alias = uniqueName.substring(18,23),
                        TimeZoneSidKey = 'America/Los_Angeles',
                        LocaleSidKey = 'en_US',
                        LanguageLocaleKey = 'en_US',
                        ProfileId = profID
                );
        return tuser;
    }

}