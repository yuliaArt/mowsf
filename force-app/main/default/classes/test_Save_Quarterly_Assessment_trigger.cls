@isTest(SeeAllData=true)
private class test_Save_Quarterly_Assessment_trigger {

	static testMethod void validate_Save_Quarterly_Assessment_trigger(){

	System.debug('Test for Save Quarterly Assessment');
	Quarterly_Assessment__c qa = new Quarterly_Assessment__c();
	Contact c = [Select ID, Name from Contact limit 1];
	Quarterly_Assessment_Period__c qp = [Select ID, Name from Quarterly_Assessment_Period__c limit 1];
	qa.client__c = c.ID;
	qa.Follow_up__c = false;
	qa.Date__c= date.today();
	qa.Period__c = qp.Id;
	qa.Comments__c='Comments';
	Insert qa;	//trigger is only called on update so insert first, then update
	try{
		//qa.Primary_Route__c = r.Id;
		Update qa;
		}
	catch (Exception  e){
			System.debug(e.getmessage());
			System.Assert(e.getMessage().contains('Required data'));
	}
		qa.Skip_Validation__c = true;
		qa.Date__c = Null;
		qa.Suspended__c = false;
		qa.Reviewed_By__c = Null;
		try {
			Update qa;
		}
		catch (Exception e) {
			//System.Assert(e.getMessage().contains('Required data'));
		}
		qa.Suspended__c = true;
		try {
			update qa;
		}
		catch (Exception e) {
			system.debug('exception here');
		}
		qa.Date__c = date.today();
		try {
			Update qa;
		}
		catch (Exception e) {
			//System.Assert(e.getMessage().contains('Required data'));
		}
		qa.Skip_Validation__c = false;
		try {
			Update qa;
		}
		catch (Exception e){
			//System.Assert(e.getMessage().contains('Required data'));

		}


	}
}