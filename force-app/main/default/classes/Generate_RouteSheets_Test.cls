/**
 * Created by banghart on 11/12/17.
 */
@IsTest(SeeAllData=false)
public with sharing class Generate_RouteSheets_Test {
    public static testMethod void testRunAs() {

        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User usr = createTestUser(pf.Id, 'Test User', 'Test User');
        createTestData();
        System.runAs(usr) {
            Date genDate = Date.today();
            String dateString = genDate.format();
            DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
            String dayOfWeek = deliveryDate.format('EEE');
            if (dayOfWeek == 'Sun' || dayOfWeek == 'Sat') {
                genDate = genDate.addDays(3);
            }
            Generate_Deliveries gd = new Generate_Deliveries();
            List<Date> returnedDates = gd.prepare_date_list(genDate, 7);
            // gd.combine_holiday = true;
            gd.holiday_date = genDate.addDays(7);
            System.debug('+++++ +++++ About to generate Deliveries ');
            List<Date> gdateList = new List<Date>();
            gdateList.add(genDate);
            gd.do_generation(gdateList);
            List<Delivery__c> checkDeliveries = [SELECT Client__r.Lastname, Route_Group_Del__r.Name, Date__c, Special__c FROM Delivery__c];
            System.debug('Follows is deliveries created');
            for (Delivery__c d :checkDeliveries) {
                System.debug(d.Client__r.Lastname + ' - ' + d.Route_Group_Del__r.Name + ' - ' + d.Date__c + ' - ' + d.Special__c);
            }
            gd.do_generation(returnedDates);
            Generate_RouteSheets gr = new Generate_RouteSheets(genDate);
            System.debug('+++++ +++++ About to generate route sheet all, no holiday ');

            gr.do_generation('all');
            gr.holiday_date = genDate.addDays(7);
            gr.combine_holiday = true;
            System.debug('+++++ +++++ About to generate route sheet all, with holiday ');
            gr.do_generation('all');
            List<Route__c> routes = [SELECT Id FROM Route__c];
            System.debug('+++++ +++++ About to generate route sheet one route ');
            gr.do_generation(routes[0].Id);
            gr.combine_holiday = false;
            gr.do_generation(routes[1].Id);

        }
    }

    public static User createTestUser(Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User testUser = new User
                (
                        firstname = fName,
                        lastName = lName,
                        email = uniqueName + '@test' + orgId + '.org',
                        Username = uniqueName + '@test' + orgId + '.org',
                        EmailEncodingKey = 'ISO-8859-1',
                        Alias = uniqueName.substring(18, 23),
                        TimeZoneSidKey = 'America/Los_Angeles',
                        LocaleSidKey = 'en_US',
                        LanguageLocaleKey = 'en_US',
                        ProfileId = profId
                );
        return testUser;
    }
    public static Void createTestData() {
        Date genDate = Date.today();
        String dateString = genDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        if (dayOfWeek == 'Sun' || dayOfWeek == 'Sat') {
            genDate = genDate.addDays(3);
        }
        List<Control_Settings__c> cs = new List<Control_Settings__c>();
        cs.add(new Control_Settings__c());
        insert cs;
        List<Route__c> routes = new List<Route__c>();
        routes.add(new Route__c(Name = 'test', Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'test2',Route_Type__c = 'Frozen', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'test3',Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        insert routes;
        List<Contact> c = new List<Contact>();
        c.add(new Contact(FirstName = 'A', LastName = 'B', Diet__c = 'Regular', Temperature__c = 'Chilled', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'AA', LastName = 'BB', Diet__c = 'Regular', Temperature__c = 'Frozen', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'CC', LastName = 'CC', Diet__c = 'Regular', Temperature__c = 'Hot', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'DD', LastName = 'DD', Diet__c = 'Low Sodium', Temperature__c = 'Chilled', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'EE', LastName = 'EE', Diet__c = 'Low Sodium', Temperature__c = 'Chilled', Status__c = 'Active'));
        insert c;
        c = [SELECT Id FROM Contact];
        List<Meal_Scheduler__c> mealSchedulers = new List<Meal_Scheduler__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        for (Contact client : c) {
            mealSchedulers.add(new Meal_Scheduler__c(Client__c = client.Id, Mon__c = true, Tue__c = true, Wed__c = true,
                    Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                    Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
            system.debug(client.Id + 'is a client');
        }
        System.debug('route 0 is ' + routes[0].Name + 'with ID: ' + routes[0].Id);
        System.debug('route 2 is ' + routes[2].Name + 'with ID: ' + routes[2].Id);
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[1].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[2].Id, Route__c = routes[2].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[3].Id, Route__c = routes[2].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[4].Id, Route__c = routes[2].Id));

        List<Delivery__c> specialDeliveriesList = new List<Delivery__c>();
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[0].Id, Quantity__c=0, Special__c = true, Date__c = genDate, Client__c = c[0].Id));
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[0].Id, Quantity__c=0, Special__c = true, Date__c = genDate, Client__c = c[1].Id));
        specialDeliveriesList.add(new Delivery__c(Route_Group_Del__c = routes[2].Id, Quantity__c=0, Special__c = true, Date__c = genDate, Client__c = c[2].Id));
        // insert specialDeliveriesList;

        insert mealSchedulers;
        insert routeGroupMembers;
        routes = [SELECT Id FROM Route__c];
        System.debug('number of routes created was ' + routes.size());
    }

}