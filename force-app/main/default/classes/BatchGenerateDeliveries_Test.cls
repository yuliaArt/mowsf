/**
 * Created by banghart on 6/18/18.
 */
@isTest
public with sharing class BatchGenerateDeliveries_Test {
    public static testMethod void testRunAs() {
        deleteTestData();
        Date genDate = adjustGenDate();
        system.debug('running the testRunAs ');
        List<Delivery__c> existing = [SELECT Id FROM Delivery__c];
        system.debug('existing delivery count: ' + existing.size());
        createTestData(genDate);
        List<Contact> c = [SELECT FirstName, LastName, Status__c FROM Contact ORDER BY FirstName];
        BatchGenerateDeliveries batch = new BatchGenerateDeliveries(genDate);
        List<Date> dateList = batch.prepare_date_list(genDate,7);
        //
        Test.startTest();
        for (Date gdate : dateList) {
            batch = new BatchGenerateDeliveries(gdate);
            system.debug('about to execute');
            List<Meal_Suspension__c> mealSuspensions = [SELECT Id, Start_Date__c, End_Date__c, Client__r.FirstName FROM Meal_Suspension__c];
            system.debug(mealSuspensions[0].Client__r.FirstName + ' meal suspension found');
            system.debug ('number meals suspensions found: ' + mealSuspensions.size());
            system.debug('start date: ' + mealSuspensions[0].Start_Date__c + ', end date: ' + mealSuspensions[0].End_Date__c);
            system.debug('about to run batch for ' + gdate);
            Id batchId = Database.executeBatch(batch);
        }
        Test.stopTest();
        for (Date gdate : dateList) {
            // This run should not create a delivery for Client1first
            List<Delivery__c> createdDeliveries = [SELECT Id, Date__c, Client__r.FirstName, Route_Group_Del__c, Route_Group_Del__r.Name FROM Delivery__c WHERE Client__r.FirstName = 'Client1first' AND Special__c = false AND Date__c = :gdate];
            if (gdate == genDate) {
                system.debug('Size is ' + createdDeliveries.size());
                for (Delivery__c d : createdDeliveries) {
                    system.debug('delivery ' + d.Date__c + ' ' + ' ' + d.Route_Group_Del__c + ' ' + d.Route_Group_Del__r.Name + ' ' + d.Client__r.FirstName);
                }
                system.assert(createdDeliveries.size() == 0);
            }
        }
        for (Date gDate : dateList) {
            List<Delivery__c> deliveries = [SELECT Id, Quantity__c, Chill__c, Chill_Alt__c FROM Delivery__c WHERE Date__c = :gDate AND Special__c = false];
            system.debug('number generated is: ' + deliveries.size());
        }
        // need to confirm deliveries were generated
        for (Date gdate : dateList) {
            batch = new BatchGenerateDeliveries(gdate);
            batch.combine_holiday = true;
            batch.combineDate = gdate.addDays(7);
            batch.combineChill = true;
            batch.combineFrozen = true;
            system.debug('about to execute');
            Id batchId = Database.executeBatch(batch);
        }
        //system.assert(c[0].Status__c == 'Suspended no resume date');
    }
    public static testMethod void testThree() {
        system.debug('running testThree');
        Date genDate = adjustGenDate();
        deleteTestData();
        createTestData(genDate);
        List<Meal_Suspension__c> msToUpdate = [SELECT Id, Start_Date__c, End_Date__c FROM Meal_Suspension__c];
        for (Meal_Suspension__c ms : msToUpdate) {
            ms.Start_Date__c = genDate.addDays(-3);
            ms.End_Date__c = genDate;
        }
        update msToUpdate;
        Test.startTest();
        BatchGenerateDeliveries batch = new BatchGenerateDeliveries(genDate);
        List<Date> dateList = batch.prepare_date_list(genDate,6);
        for (Date oneDate : dateList) {
            batch = new BatchGenerateDeliveries(oneDate);
            batch.combineFrozen = true;
            batch.combine_holiday = true;
            batch.combineChill = true;
            batch.combineDate = oneDate.addDays(7);
            Id batchId = Database.executeBatch(batch);
        }
        Test.stopTest();
        List<Delivery__c> createdDeliveries = [SELECT Id, Date__c, Client__r.FirstName, Route_Group_Del__c, Route_Group_Del__r.Name FROM Delivery__c WHERE Client__r.FirstName = 'Client1first' AND Special__c = false AND Date__c = :genDate];
        System.assert(createdDeliveries.size() > 0);
    }
    public static testMethod void aSecondTest() {
        system.debug('running the second test');
        Date genDate = adjustGenDate();
        deleteTestData();
        createTestData(genDate);
        List<Meal_Suspension__c> msToDelete = [SELECT Id FROM Meal_Suspension__c];
        delete msToDelete;
        system.debug('the gen date is ' + genDate);
        Test.startTest();
        BatchGenerateDeliveries batch = new BatchGenerateDeliveries(genDate);
        batch.combine_holiday = false;
        batch.combineChill = false;
        batch.combineFrozen = false;
        Id batchId = Database.executeBatch(batch);
        Test.stopTest();
        List<Delivery__c> createdDeliveries = [SELECT Id, Date__c, Client__r.FirstName, Route_Group_Del__c, Route_Group_Del__r.Name FROM Delivery__c WHERE Client__r.FirstName = 'Client1first' AND Special__c = false AND Date__c = :genDate];
        System.assert(createdDeliveries.size() > 0);
    }
    public static Date adjustGenDate() {
        Date genDate = Date.today();
        String dateString = genDate.format();
        DateTime deliveryDate = DateTime.parse(dateString + ' 11:12 AM');
        String dayOfWeek = deliveryDate.format('EEE');
        // we want to generate deliveries for mon - sat, so will adjust date based on day of week
        if (dayOfWeek == 'Sun') {
            genDate = genDate.addDays(1);
        } else if (dayOfWeek == 'Tue') {
            genDate = genDate.addDays(6);
        } else if (dayOfWeek == 'Wed') {
            genDate = genDate.addDays(5);
        } else if (dayOfWeek == 'Thu') {
            genDate = genDate.addDays(4);
        } else if (dayOfWeek == 'Fri') {
            genDate = genDate.addDays(3);
        } else if (dayOfWeek == 'Sat') {
            genDate = genDate.addDays(2);
        }
        return genDate;
    }
    public static Void deleteTestData() {
        List<Contact> c = [SELECT Id FROM Contact];
        List<Meal_Scheduler__c> ms = [SELECT Id FROM Meal_Scheduler__c];
        List<Meal_Suspension__c> msus = [SELECT Id FROM Meal_Suspension__c];
        List<Route__c> routes = [SELECT Id FROM Route__c];
        List<Route_Group_Member__c> rgm = [SELECT Id FROM Route_Group_Member__c];
        List<Delivery__c> d = [SELECT Id FROM Delivery__c];
        system.debug('about to delete data');
        delete d;
        delete rgm;
        delete routes;
        delete msus;
        delete ms;
        delete c;
    }
    public static Void createTestData(Date genDate) {
        List<Contact> c = new List<Contact>();
        Date startDate = genDate.addDays(-1);
        c.add(new Contact(FirstName = 'Client1first', LastName = 'Client1last', Start_Date__c = startDate, Diet__c = 'Regular', Temperature__c = 'Chilled',  Status__c = 'Active'));
        c.add(new Contact(FirstName = 'Client2first', LastName = 'Client2last', Start_Date__c = startDate, Diet__c = 'Regular', Temperature__c = 'Frozen', Status__c = 'Active'));
        c.add(new Contact(FirstName = 'Client3first', LastName = 'Client3last', Start_Date__c = startDate, Diet__c = 'Regular', Temperature__c = 'Hot', Status__c = 'Active', MailingCity = 'San Francisco', MailingStreet = '123 Oak'));
        c.add(new Contact(FirstName = 'Driver1first', LastName = 'Driver1last', MOWSF_Staff__c = true));
        insert c;
        List<Route__c> routes = new List<Route__c>();
        routes.add(new Route__c(Name = 'Daily', Route_Type__c = 'Daily', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        routes.add(new Route__c(Name = 'Frozen',Route_Type__c = 'Frozen', Mon__c = true, Tue__c = true, Wed__c = true, Thu__c = true, Fri__c = true, Sat__c = true));
        insert routes;
        c = [SELECT Id FROM Contact WHERE FirstName = 'Client1first'];
        List <Meal_Suspension__c> mealSuspensionsList = new List<Meal_Suspension__c>();
        mealSuspensionsList.add(new Meal_Suspension__c(
                Client__c = c[0].Id,
                Start_Date__c = genDate,
                Reason__c = 'Appt',
                Requestor__c = 'Self'
            )
        );
        insert mealSuspensionsList;
        List<Meal_Scheduler__c> mealSchedulers = new List<Meal_Scheduler__c>();
        List<Route_Group_Member__c> routeGroupMembers = new List<Route_Group_Member__c>();
        c = [SELECT Id FROM Contact WHERE FirstName = 'Client1first'];
        mealSchedulers.add(new Meal_Scheduler__c(Client__c = c[0].Id, Mon__c = true, Tue__c = true, Wed__c = true,
                Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[1].Id));
        c = [SELECT Id FROM Contact WHERE FirstName = 'Client2first'];
        mealSchedulers.add(new Meal_Scheduler__c(Client__c = c[0].Id, Mon__c = true, Tue__c = true, Wed__c = true,
                Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[1].Id));
        c = [SELECT Id FROM Contact WHERE FirstName = 'Client3first'];
        mealSchedulers.add(new Meal_Scheduler__c(Client__c = c[0].Id, Mon__c = true, Tue__c = true, Wed__c = true,
                Thu__c = true, Fri__c = true, Sat__c = true, Mon_Quantity__c = 2, Tue_Quantity__c = 2, Wed_Quantity__c = 2,
                Thu_Quantity__c = 2, Fri_Quantity__c = 2, Sat_Quantity__c = 2, Sun_Quantity__c = 2));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[0].Id));
        routeGroupMembers.add(new Route_Group_Member__c(RG_Member_Contact__c = c[0].Id, Route__c = routes[1].Id));

        c = [SELECT Id FROM Contact WHERE MOWSF_Staff__c = true];
        routes = [SELECT Id FROM Route__c];
        routes[0].Driver_1__c = c[0].Id;
        routes[1].Driver_1__c = c[0].Id;
        insert mealSchedulers;
        insert routeGroupMembers;
        List<Delivery__c> specialDeliveries = new List<Delivery__c>();
        c = [SELECT Id FROM Contact WHERE MOWSF_Staff__c = false];

        specialDeliveries.add(new Delivery__c(Quantity__c = 1, Date__c = genDate, Special__c = true, Client__c = c[0].Id, Route_Group_Del__c = routes[0].Id));
        specialDeliveries.add(new Delivery__c(Quantity__c = 2, Date__c = genDate, Special__c = false, Client__c = c[0].Id, Route_Group_Del__c = routes[0].Id));
        for (Integer offset = 0; offset < 8; offset ++) {
            Date delDate = genDate.addDays(offset);
            specialDeliveries.add(new Delivery__c(Quantity__c = 2, Date__c = delDate, Special__c = false, Client__c = c[0].Id, Route_Group_Del__c = routes[0].Id));
            specialDeliveries.add(new Delivery__c(Quantity__c = 2, Date__c = delDate, Special__c = false, Client__c = c[1].Id, Route_Group_Del__c = routes[0].Id));

        }
        insert specialDeliveries;
        System.debug('number of routes created was ' + routes.size());
    }
}