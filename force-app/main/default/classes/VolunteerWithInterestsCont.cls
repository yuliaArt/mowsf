public class VolunteerWithInterestsCont {

    public List<Contact> displayVolunteers {get;set;}
    public GW_Volunteers__Volunteer_Job__c filterProgram { get; set; }    
    public GW_Volunteers__Volunteer_Hours__c filterVolunteer {get;set;}

    public VolunteerWithInterestsCont () {
         filterProgram = new GW_Volunteers__Volunteer_Job__c();
         filterVolunteer = new GW_Volunteers__Volunteer_Hours__c();
         displayVolunteers = new List<Contact>();

    }

    public void filter(){
        
        String vProg = filterProgram.GW_Volunteers__Campaign__c;
        String cProg = vProg.left(15);
       
        displayVolunteers = [Select Id, Firstname, Lastname, 
              Primary_Email__c, Primary_Phone__c, GW_Volunteers__Volunteer_Status__c,  
        (select Volunteer__c, program__r.Name from Volunteer_Interests__r) 
         From Contact Where Id in (         
            select  GW_Volunteers__Contact__c 
            From GW_Volunteers__Volunteer_Hours__c 
            Where GW_Volunteers__Volunteer_Campaign__c = :cProg
              And GW_Volunteers__Volunteer_Job__c = :filterVolunteer.GW_Volunteers__Volunteer_Job__c
              And GW_Volunteers__Status__c = :filterVolunteer.GW_Volunteers__Status__c
              And GW_Volunteers__Start_Date__c = :filterVolunteer.GW_Volunteers__Start_Date__c 
           )
         Order By Firstname, Lastname ];

    }

}