/**
 * Created by banghart on 10/23/17.
 */
/*
    @Author - Sunil/Girikon
    modified by Rick
    @Date -  6/16/2017
    Controller to save, edit and delete Assessment and fetch user primary information .
*/
public class AssessmentControllerExtn {

    public Set<string> diets {get; set;}
    public Set<string> days{get; set;}
    public Set<string> temps{get; set;}
    public Integer lonelyScore{get;set;}
    public Integer adder;
    public string route {get; set;}
    public string t {get;set;}
    public string d {get;set;}
    public string age {get;set;}
    public Boolean renderSection {get;set;}
    public List<Contact> drList;
    public Assessment__c  assessment { set; get; }
    public Id SocialWorkerID {get;set;}
    public string SW_degree {get;set;}
    public Contact Client {get;set;}
    public AssessmentControllerExtn(ApexPages.StandardController stdCtrl)
    {
        assessment = (Assessment__c)stdCtrl.getRecord();
        diets = new Set<string>();
        days = new Set<string>();
        temps = new Set<string>();
        renderSection = true;
        system.debug(assessment.Income_Level__c + ' is the income level');
        //system.debug(assessment.Valid_ADL__c + ' is the ADL constructor');
        system.debug('Should be about to fetch user details');
        fetchUserDtls();
        getSocialWorkDegree();
    }
    public void getSocialWorkDegree() {
        Id userID = UserInfo.getUserId();
        SocialWorkerID = userID;
        List<User> degrees;
        degrees = [SELECT Social_Work_Degree__c FROM User WHERE Id = :userID];
        SW_degree = degrees[0].Social_Work_Degree__c;
    }
    public void populateUserDtls()
    {
        fetchUserDtls();
    }
    //Fetch user details like meal plan, diet, primary route form delivery rules.
    // Better to call this fetchClientDtls
    public void fetchUserDtls() {
        Id cid;
        if(assessment.Id != null){
            cid = [select Client__c from Assessment__c where Id = :assessment.Id limit 1].Client__c;
        } else {
            cid = assessment.Client__c;
        }
        drList = [select Diet__c,Temperature__c,Primary_Route__r.Name, Age__c, Birthdate, OwnerId
            FROM Contact WHERE Id = :cid];
        List<User> ownerList;
        Client = drList[0];
        //ownerList = [SELECT IsActive FROM User WHERE Id = :Client.OwnerId];
        //if (ownerList[0].IsActive == False) {
        //    Client.OwnerId = UserInfo.getUserId();
        //}
        system.debug('owner id is ' + Client.OwnerId);
        system.debug('what follows is meal type');
        system.debug(drList[0].Primary_Route__r.Name);
        route = drList[0].Primary_Route__r.Name;
        t = drList[0].Temperature__c;
        d = drList[0].Diet__c;
        age = drList[0].Age__c.toPlainString();
        system.debug('***********');
        system.debug(d);
        //if(drList!=null && drList.size()>0)
        //{
        //    diets.clear();days.clear();temps.clear();route ='';
        //    route = drList[0].Client__r.Primary_Route__r.Name;
        //    assessment.Primary_Route__c = drList[0].Client__r.Primary_Route__c;
        //    for(Delivery_Rule__c dr : drList)
        //    {
        //        diets.add(dr.Diet__c.replaceAll('-Every',''));
        //        days.add(dr.Day__r.Name.replaceAll('-Every',''));
        //        temps.add(dr.Temperature__c.replaceAll('-Every',''));
        //    }
        //}
    }
    public PageReference save() {
        system.debug('Client Residence_Type__c stuff ');
        system.debug(assessment.Complete__c);
        system.debug(assessment.Residence_Type__c);

        string errormsg='';

        if ((assessment.Complete__c == true) &&
                (assessment.ADL_Transfer_Mobility__c == '--Select--' ||
                assessment.ADL_Bathing__c == '--Select--' ||
                        assessment.ADL_Dressing__c == '--Select--' ||
                assessment.ADL_Toileting__c == '--Select--' ||
                        assessment.ADL_Eating__c == '--Select--' ||
                assessment.ADL_Ambulating__c == '--Select--' ||
                        assessment.ADL_Grooming__c == '--Select--')){
            errormsg += 'ADL section is incomplete<br />';
        } //ADL_is_Complete
        if (assessment.Complete__c == true &&
                (assessment.Assessment_Type__c == '--Select--' ||
                assessment.Primary_Language__c == '--Select--' ||
                assessment.Neighborhood__c == '--Select--'))
        {
            errormsg += 'Client Info section is incomplete<br />';
        }//Client_Info_Valid
        if ((assessment.Complete__c) &&
                (assessment.Lives_Alone__c == '--Select--' ||
                assessment.Personal_Appearance__c == '--Select--'
                )) {
            errormsg += 'Client Situation section is incomplete<br />';} //Client_Situation_is_Complete
        if ((assessment.Complete__c) && (assessment.Food_Left_List__c == '--Select--' )){
            errormsg += 'Delivery section is incomplete<br />';
        } //Delivery_is_Complete
        if ((assessment.Complete__c) && (
                assessment.Race__c == '--Select--' ||
                assessment.Ethnicity__c == '--Select--' ||
                assessment.Sexual_Orientation__c == '--Select--' ||
                assessment.Medi_Cal__c == '--Select--' ||
                assessment.Medicare__c == '--Select--' ||
                assessment.Veteran__c == '--Select--' ||
                assessment.Income_Level__c == '--Select--')) {
            errormsg += 'Demographics section is incomplete<br />';
        }  //Demographics_is_Complete
        if ((assessment.Complete__c == true) && (assessment.Residence_Type__c == '--Select--')){
            errormsg += 'Physical Environment section is incomplete<br />';
        }//Environment_is_Complete
        if ((assessment.Complete__c) &&
                (assessment.IADL_Managing_Medicines__c == '--Select--' ||
                assessment.IADL_Shopping__c == '--Select--' ||
                assessment.IADL_Meal_Preparation__c == '--Select--' ||
                assessment.IADL_Telephone__c == '--Select--' ||
                assessment.IADL_Transportation__c == '--Select--' ||
                assessment.IADL_Light_Housework__c == '--Select--' ||
                assessment.IADL_Heavy_Housework__c == '--Select--' ||
                assessment.IADL_Managing_Money__c == '--Select--'
            )){
            errormsg +='IADL section is incomplete<br />';
        }//IADL_is_Complete
        if ((assessment.Complete__c) &&
                (assessment.Attitude__c == '--Select--' ||
                        assessment.Alertness__c == '--Select--' ||
                        assessment.Communications_Receptive__c == '--Select--' ||
                        assessment.Expressive__c == '--Select--'
                )){
            errormsg += 'Mental Health section is incomplete<br />';
        } //Mental_Health_is_Complete
        if ((assessment.Complete__c) &&
                (assessment.Mobility__c == '--Select--' ||
                        assessment.Recent_Falls__c == '--Select--')){
            errormsg += 'Mobility section is incomplete<br />';
        } //Mobility_is_Complete
        if ((assessment.Complete__c) &&
                (assessment.Food_Storage__c == '--Select--' ||
                        assessment.Food_Reheated__c == '--Select--' ||
                        assessment.Diet__c == '' )){
            errormsg += 'Nutrition section is incomplete<br />';
        }//Nutrition_is_Complete
        if ((assessment.Complete__c) &&
                (assessment.OOA_Consent_Signed2__c == '--Select--' ||
                        assessment.Follow_in_CM__c == '--Select--' ||
                        assessment.Client_Eligible__c == '--Select--' )){

            errormsg += 'Other section is incomplete<br />';} //Other_is_Complete
        //
        if ((assessment.Complete__c) &&
                (assessment.Health_Number_of_Meds__c == NULL ||
                        assessment.Vision__c == '--Select--' ||
                        assessment.Hearing__c == '--Select--' ||
                        assessment.Vision_Assistive_Devices__c == '--Select--' ||
                        assessment.Hearing_Assistive_Devices__c == '--Select--' )){

            errormsg += 'Physical Health section is incomplete<br />';
        }  //Physical_Health_is_Complete
        if(errormsg != '') {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errormsg));
            return null;
        }
        if(assessment.Id!=null)
        {
            update assessment;
        }
        else{
            insert assessment;
        }
        update Client;
        PageReference pr = new PageReference('/'+assessment.Id);
        pr.setRedirect(true);
        return pr;
    }
    public Integer getLonelyScore() {
        adder = 0;
        lonelyScore = adder;
        return lonelyScore;
    }
    public PageReference customClone() {
        //assessment = (Assessment__c)stdCtrl.getRecord();
        Assessment__c newAssessment = assessment.clone();
        newAssessment.Assessment_Type__c = '--Select--';
        newAssessment.Date__c = null;
        newAssessment.Complete__c = false;
        newAssessment.Social_Worker_E_Signature__c = null;
        insert newAssessment;
        PageReference pr = new PageReference('/lightning/r/Assessment__c/' + newAssessment.Id + '/edit');
        pr.setRedirect(true);
        return pr;
    }
    public PageReference customDelete()
    {
        if(assessment.Id!=null)
        {
            delete [select Id from Assessment__c where Id=:assessment.Id];
        }
        PageReference pr = new PageReference('/'+assessment.Client__c);
        pr.setRedirect(true);
        return pr;
    }
}