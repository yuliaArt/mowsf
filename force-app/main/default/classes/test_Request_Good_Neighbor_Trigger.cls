@isTest
private class test_Request_Good_Neighbor_Trigger {
	static testMethod void validate_Request_Good_Neighbor_Trigger(){

		Contact c = new Contact();
		c.FirstName = 'John';
		c.LastName = 'Doe';
		RecordType rt = [Select ID, Name from RecordType where Name = 'Client Record' and SObjectType = 'Contact' limit 1];
		c.RecordTypeID = rt.ID;
		Insert c;
		
		Request_Good_Neighbor__c rgn = new Request_Good_Neighbor__c();
		rgn.Client__c = c.ID;
		rgn.Program__c = 'GN - Friendly Visitor';
		Insert rgn;

        String rgnID;
		String subject = 'New Good Neighbor Request';
		String message = 'A new Good Neighbor Request has been created for client '+c.Name+'. You can access the request here -> '+'Link: '+ URL.getSalesforceBaseUrl().toExternalForm()+ '/'+rgnID;
		String[] recipients = new String[] {'it@mowsf.org'};
		rgnID = rgn.ID;

		Messaging.SingleEmailMessage msg = MailerUtils.constructEmail(subject,message,recipients);

		System.assertEquals(msg.Subject,subject);
		System.assertEquals(msg.HTMLBody,message);
		System.assertEquals(msg.ToAddresses, recipients);
		}

}