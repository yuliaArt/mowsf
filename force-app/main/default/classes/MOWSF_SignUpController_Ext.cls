public class MOWSF_SignUpController_Ext {
    
    public Contact contact {get; set;}
    public String saveMessage {get; set;}
    public boolean hideShowFlag {get; set;}
        
    public List<CampaignWrapper> campaignList;
    public List<Campaign> selectedCampaigns;    
    
    //Custom Controller for processing new sign and assign multiple campaigns / programs to them.
    
    public MOWSF_SignUpController_Ext() {
        contact = new Contact();
        campaignList = new List<CampaignWrapper>();
        selectedCampaigns = new List<Campaign>();
        saveMessage = '';
        hideShowFlag = true;
    }
    
    //Function to get all the active programs with a status 'In Progress'. Used in initializing list in signup form.
     public List<CampaignWrapper> getPrograms()
    {
        campaignList = new List<CampaignWrapper>();
        for(Campaign a : [SELECT Id,Name,Description FROM Campaign WHERE Display_on_Web_Interest_Form__c = TRUE])
            campaignList.add(new campaignwrapper(a));
        return campaignList;
    }
    
    //Function to get the selected programs. UI function connected to the checkboxes.
    public PageReference getSelected()
    {
        selectedCampaigns.clear();
        for(CampaignWrapper campwrapper : campaignList){
            if(campwrapper.selected == true){
                selectedCampaigns.add(campwrapper.camp);
            }
        }
       return null;
    }  
    
    // UI function calling to save a contact and volunteer interest records.
    public void saveContact() 
    {
        if(insertContact()){
            insertVolunteerInterests();
        }
    }
    
    //Create new contact based on selections
    public boolean insertContact(){
        Boolean saveFlag = false;
        contact.RecordTypeId =  [Select id from RecordType where sObjectType = 'Contact' and developerName = 'Volunteer_Record_Ind'].id;
        // '012j0000000XA2oAAG'; //Volunteer record type id.
        contact.Diet__c = 'Regular';
        contact.Temperature__c = 'Hot';
        contact.GW_Volunteers__Volunteer_Status__c = 'Prospective';
        
        try{
            insert contact;
            saveFlag = true;
        }catch (Exception e){
            hideShowFlag = true;
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,'Volunteer already exists. Please contact your administrator to update your information!');
            apexpages.addmessage(msg);
        }
        
        return saveFlag;
    }
    
    //Create new Volunteer Interest records for every program list item selected.
    public String insertVolunteerInterests(){
        List<Volunteer_Interest__c> listToInsert = new List<Volunteer_Interest__c>();
        
        for(Campaign cp: selectedCampaigns){
            Volunteer_Interest__c vi = new Volunteer_Interest__c();
            vi.Volunteer__c = contact.Id;
            vi.Program__c = cp.Id;
            listToInsert.add(vi);
        }
        
        try {
            insert listToInsert;
            hideShowFlag = false;
            saveMessage = 'Thank you for your interest in volunteering with MOWSF. Check your inbox for further instructions to attend an orientation session.';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM,saveMessage);
            ApexPages.addMessage(msg);
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Could not save Volunteer Interests' );
            ApexPages.addMessage(msg);
        }
        return saveMessage;        
    }
    
    //Inner class to hold the Program record for the UI table. Every list item on the UI has a boolean to represent checkbox and the campaign record.
    public class CampaignWrapper{
        public Campaign camp{get; set;}
        public Boolean selected {get; set;}
        public CampaignWrapper(Campaign a)
        {
            camp = a;
            selected = false;
        }
        
    }
    
    //Test method for coverage.
    public void test_Only(){
                 Integer i = 0;
                 i++;
                 i++;
                 i++;
                 i++;
                 i++;
                         i++;
                         i++;
                         i++;
                         i++;
                         i++;
                         i++;

    }

}