/**
 * Created by banghart on 6/15/18.
 */

global class BatchThanksgiving implements Database.Batchable<sObject>, Database.Stateful {
    public Delivery__c tempDelivery;
    public Date gendate { get; set; }
    public Integer totalUpserted { get; set; }
    global Boolean foundMeal = false;
    global Boolean goodGenDate = false;
    global List<Delivery__c> existingDeliveries;
    global Set<Delivery__c> deliveriesToUpsert;
    global List<Delivery__c> deliveriesToUpsertList;
    global List<Delivery__c> deliveriesToDelete;
    global Set<Delivery__c> deliveriesToDeleteSet;
    global Map<Id, String> clientsMap;
    global Map<String, List<Delivery__c>> existingDeliveryMap;
    global Map<Id, String> clientFoodDel = new Map<Id, String>();
    global List<Meal_Suspension__c> suspensions;
    global Map<Id, List<Meal_Suspension__c> > suspensionMap = new Map<Id, List<Meal_Suspension__c>>();
    global BatchThanksgiving(Date genDateArg) {
        clientsMap = new Map<Id, String>();
        List<Contact> contactsList = [SELECT Id FROM Contact
            WHERE (Thanksgiving_Meal__c = 'Yes' OR Thanksgiving_Meal__c = 'Yes with a guest')
            AND Status__c != 'Inactive'];
        for (Contact c : contactsList) {
            clientsMap.put(c.Id,'present');
        }
        this.clientsMap = clientsMap;
        String yearString;
        String monthString;
        String dayString;
        String formattedDateString;
        gendate = genDateArg;
        Date todayDate = Date.today();
        String existDelSOQLCondition = ' WHERE ';
        if (todayDate.daysBetween(gendate) < 0) {
            // do nothing if gendate is in the past
            goodGenDate = false;
        } else {
            goodGenDate = true;
        }
        yearString = String.valueOf(gendate.year());
        monthString = '0' + String.valueOf(gendate.month());
        monthString = monthString.right(2);
        dayString = '0' + String.valueOf(gendate.day());
        dayString = dayString.right(2);
        formattedDateString = yearString + '-' + monthString + '-' + dayString;
        existDelSOQLCondition += 'Date__c = ' + formattedDateString;
        List<Route__c> rgList = [SELECT Id, Mon__c, Tue__c, Wed__c, Thu__c, Fri__c, Sat__c, Driver_1__c FROM Route__c WHERE Name = 'Thanksgiving'];
        Date updateDate = Date.today();
        suspensions = [
                SELECT Id, Client__c, Start_Date__c, End_Date__c
                FROM Meal_Suspension__c
                WHERE Start_Date__c <= :gendate
                AND (End_Date__c = null OR End_Date__c > :gendate)
        ];
        for (Meal_Suspension__c msusp : suspensions) {
            if (suspensionMap.containsKey(msusp.Client__c)) {
                suspensionMap.get(msusp.Client__c).add(msusp);
            } else {
                List<Meal_Suspension__c> suspList = new List<Meal_Suspension__c>();
                suspList.add(msusp);
                suspensionMap.put(msusp.Client__c, suspList);
            }
        }
        this.suspensionMap = suspensionMap;
        String existDelQry = 'SELECT Id, Client__c, Date__c, Client__r.Status__c, ' +
                'Address__c, Zip__c, Diet__c, Temperature__c, Quantity__c, Sequence__c,' +
                'Route_Group_Del__c, Side_Bag__c, Special__c, Driver_1__c, Funding_Source__c FROM Delivery__c ' + existDelSOQLCondition;
        List<Delivery__c> existingDeliveries = Database.query(existDelQry);
        System.debug('existing delivery count: ' + existingDeliveries.size());
        Map<String, List<Delivery__c>> existingDeliveryMap = new Map<String, List<Delivery__c>>();

        Set<Delivery__c> deliveriesToDeleteSet = new Set<Delivery__c>();
        for (Delivery__c d : existingDeliveries) {

            List<Delivery__c> newList = new List<Delivery__c>();
            newList.add(d);
            existingDeliveryMap.put(d.Client__c + d.Date__c.format(), newList);


            if (checkSuspension(suspensionMap.get(d.Client__c), gendate) == true || d.Client__r.Status__c == 'Inactive' || clientsMap.containsKey(d.Client__c) == false) {
                system.debug(' *#*#* found delivery that should be suspended ');
                deliveriesToDeleteSet.add(d);
            }
        }
        Set<Delivery__c> deliveriesToUpsert = new Set<Delivery__c>();
        List<Delivery__c> deliveriesToUpsertList = new List<Delivery__c>(deliveriesToUpsert);
        this.deliveriesToUpsertList = deliveriesToUpsertList;
        this.deliveriesToUpsert = deliveriesToUpsert;
        this.existingDeliveries = existingDeliveries;
        this.deliveriesToDelete = deliveriesToDelete;
        this.deliveriesToDeleteSet = deliveriesToDeleteSet;
        this.existingDeliveryMap = existingDeliveryMap;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT MailingAddress,
                       FirstName,
                       Id,
                        Thanksgiving_Meal__c,
                        LastName,
                        Diet__c,
                        Temperature__c
                FROM Contact
                WHERE (Thanksgiving_Meal__c = 'Yes' OR Thanksgiving_Meal__c = 'Yes with a guest')
                AND Status__c != 'Inactive'
                ORDER BY LastName, FirstName
        ]);
    }
    global void execute(Database.BatchableContext BC, List<Contact> clients) {
        system.debug('begin execute portion of batch generate delivery');
        for (Contact c : clients) {
            // decide if this Client gets a delivery on this date
            if (checkSuspension(suspensionMap.get(c.Id), gendate) == false) {
                Integer qInteger = 0;
                if (c.Thanksgiving_Meal__c == 'Yes') {
                    qInteger = 1;
                } else if (c.Thanksgiving_Meal__c == 'Yes with a guest') {
                    qInteger = 2;
                }
                // create new if no existing delivery
                system.debug('suspension test was false with ' + c.FirstName);
                system.debug('suspension for client was ' + suspensionMap.get(c.Id));
                if (existingDeliveryMap.containsKey(c.Id + gendate.format())) {
                    foundMeal = false;
                    for (Delivery__c tempDelivery : existingDeliveryMap.get(c.Id + gendate.format())) {
                        if (tempDelivery.Special__c == true) {
                            System.debug('***** Found special in Map **** ');
                            System.debug('found meal is ' + foundMeal);
                        } else {
                            foundMeal = true;
                        }
                        Delivery__c preDelivery = tempDelivery.clone(true, true, true, true);
                        refreshDelivery(c, genDate, preDelivery);
                        if (tempDelivery == preDelivery) {
                        } else {
                            //system.debug('== says they are not equal');
                            deliveriesToUpsert.add(preDelivery);
                        }
                    }
                } else {
                    system.debug('delivery does not exist, create');
                    Delivery__c del = createDelivery(c, genDate, existingDeliveryMap);
                    del.Quantity__c = qInteger;
                    del.Chill__c = qInteger;
                    deliveriesToUpsert.add(del);
                }
            } else {
                //deliveriesToDelete.add(tempDelivery);
                // system.debug('no delivery for this rgm');
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        system.debug('******  batch update of generate delivery is finished ****** ');
        system.debug('number of deliveries to upsert: ' + deliveriesToUpsertList.size());
        system.debug('number of deliveries to delete: ' + deliveriesToDeleteSet.size());
        List<Delivery__c> deliveriesToDelete = new List<Delivery__c>(deliveriesToDeleteSet);
        List<Delivery__c> deliveriesToUpsertList = new List<Delivery__c>(deliveriesToUpsert);

        if (goodGenDate == true) {
            upsert deliveriesToUpsertList;
            this.totalUpserted = deliveriesToUpsertList.size();
            try {
                delete deliveriesToDelete;
            } catch (DmlException e) {
                system.debug(e);
            }
        }
    }
    public List<Date> prepare_date_list(Date startDate, Integer numToMake) {
        List<Date> returnDateList = new List<Date>();
        for (Integer i = 0; i < numToMake; i++) {
            returnDateList.add(startDate);
            startDate = startDate.addDays(1);
        }
        return returnDateList;
    }
    public Integer refreshDelivery(Contact m, Date genDate, Delivery__c delivery) {
        system.debug('refreshing a delivery here');
        Integer dummy = 1;
        String address = '';
        String postalCode = '';
        Integer sideBagInteger = 0;
        if (m.MailingAddress == NULL) {
            address = '';
            postalCode = '';
        } else {
            address = m.MailingAddress.getStreet();
            postalCode = m.MailingAddress.getPostalCode();
        }
        delivery.Client__c = m.Id;
        delivery.Date__c = genDate;
        delivery.Address__c = address;
        delivery.Zip__c = postalCode;
        delivery.Diet__c = m.Diet__c;
        delivery.Temperature__c = m.Temperature__c;
        delivery.Funding_Source__c = 'MOWSF';
        //system.debug(delivery.Side_Bag__c + ' is sidebag');
        return dummy;
    }
    public Delivery__c createDelivery(Contact c, Date genDate, Map<String, List<Delivery__c>> exDels) {
        // exDels Map string key is Client_id + Date__c.format()
        // **** check if there is already a delivery for the client and date
        String address = '';
        String postalCode = '';
        if (c.MailingAddress == NULL) {
            address = '';
            postalCode = '';
        } else {
            address = c.MailingAddress.getStreet();
            postalCode = c.MailingAddress.getPostalCode();
        }
        Delivery__c del = new Delivery__c(
                Client__c = c.Id,
                Date__c = genDate,
                Delivery_Status__c = 'Pending',
                Address__c = address,
                Funding_Source__c = 'MOWSF',
                Zip__c = postalCode,
                Diet__c = c.Diet__c,
                Temperature__c = c.Temperature__c,
                Special__c = false
        );
        return del;
    }
    public Boolean checkSuspension(List<Meal_Suspension__c> suspList, Date checkDate) {
        Boolean checkResult = false;
        if (suspList != null) {
            for (Meal_Suspension__c susp : suspList) {
                system.debug('checking ' + susp.End_Date__c + ' against ' + checkDate);
                if (checkDate >= susp.Start_Date__c && ((susp.End_Date__c == null) || susp.End_Date__c >= checkDate)) {
                    checkResult = true;
                }
                system.debug('result is ' + checkResult);
            }
        }
        return checkResult;
    }
}